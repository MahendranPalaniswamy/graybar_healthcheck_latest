﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;
using System.Threading.Tasks;

namespace SyncWindowsService
{
    [RunInstaller(true)]
    public partial class SyncServiceProjectInstaller : System.Configuration.Install.Installer
    {
        public SyncServiceProjectInstaller()
        {
            InitializeComponent();
        }
    }
}
