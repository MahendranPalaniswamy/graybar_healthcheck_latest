
namespace GB.intrnl
{
    public class PackagePropertiesMetaData
        : Sybase.Reflection.EntityMetaData
    {
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public PackagePropertiesMetaData()
        {
            _init();
        }
        
        protected void _init()
        {
            SetId(5);
            SetAttributes(new Sybase.Reflection.AttributeMetaDataList());
            SetOperations(new Sybase.Reflection.OperationMetaDataList());
            SetAttributeMap(new Sybase.Reflection.AttributeMap());
            SetOperationMap(new Sybase.Reflection.OperationMap());
            Sybase.Reflection.AttributeMetaData value_attribute = AddAttributeWithParams(0, "value", "string", 300, false, false, false, false, false, false, "\"b\"");
            Sybase.Reflection.AttributeMetaData pending_attribute = AddAttributeWithParams(20001, "pending", "boolean", -1, false, false, true, false, false, false, "\"pending\"");
            Sybase.Reflection.AttributeMetaData pendingChange_attribute = AddAttributeWithParams(20002, "pendingChange", "char", 1, false, false, true, false, false, false, "\"_pc\"");
            Sybase.Reflection.AttributeMetaData replayPending_attribute = AddAttributeWithParams(20005, "replayPending", "long", -1, false, false, true, false, false, false, "\"_rp\"");
            Sybase.Reflection.AttributeMetaData replayFailure_attribute = AddAttributeWithParams(20006, "replayFailure", "long", -1, false, false, true, false, false, false, "\"_rf\"");
            Sybase.Reflection.AttributeMetaData key_attribute = AddAttributeWithParams(1, "key", "string", 300, false, true, false, false, false, false, "\"a\"");
            Sybase.Reflection.AttributeMetaData replayCounter_attribute = AddAttributeWithParams(20004, "replayCounter", "long", -1, false, false, true, false, false, false, "\"_rc\"");
            Sybase.Reflection.AttributeMetaData disableSubmit_attribute = AddAttributeWithParams(20003, "disableSubmit", "boolean", -1, false, false, true, false, false, false, "\"_ds\"");
            InitAttributeMapFromAttributes();
            AddOperationWithParams(500, "getPendingObjects", "PackageProperties*", true);
            Sybase.Reflection.OperationMetaData GetPendingObjects1_operation = AddOperationWithParams(502, "getPendingObjects", "PackageProperties*", true);
            AddParameterForOperation(GetPendingObjects1_operation, "pendingChange", "char");
            AddOperationWithParams(505, "getReplayPendingObjects", "PackageProperties*", true);
            AddOperationWithParams(690, "isNew", "boolean", false);
            AddOperationWithParams(691, "isDirty", "boolean", false);
            AddOperationWithParams(692, "isDeleted", "boolean", false);
            AddOperationWithParams(693, "refresh", "void", false);
            AddOperationWithParams(694, "_pk", "string?", false);
            AddOperationWithParams(695, "isPending", "boolean", false);
            AddOperationWithParams(696, "isCreated", "boolean", false);
            AddOperationWithParams(697, "isUpdated", "boolean", false);
            AddOperationWithParams(698, "submitPending", "void", false);
            AddOperationWithParams(699, "cancelPending", "void", false);
            AddOperationWithParams(700, "submitPendingOperations", "void", true);
            AddOperationWithParams(701, "cancelPendingOperations", "void", true);
            AddOperationWithParams(702, "save", "void", false);
            Sybase.Reflection.OperationMetaData FindWithQuery16_operation = AddOperationWithParams(703, "findWithQuery", "PackageProperties*", true);
            AddParameterForOperation(FindWithQuery16_operation, "query", "Sybase.Persistence.Query");
            Sybase.Reflection.OperationMetaData GetSize17_operation = AddOperationWithParams(705, "getSize", "int", true);
            AddParameterForOperation(GetSize17_operation, "query", "Sybase.Persistence.Query");
            InitOperationMapFromOperations();
            SetName("PackageProperties");
            SetTable("\"graybarmobilityanonymous_1_0_packageproperties\"");
            SetSynchronizationGroup("system");
        }
    }
}