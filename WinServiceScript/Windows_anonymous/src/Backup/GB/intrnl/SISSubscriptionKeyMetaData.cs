
namespace GB.intrnl
{
    public class SISSubscriptionKeyMetaData
        : Sybase.Reflection.ClassMetaData
    {
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public SISSubscriptionKeyMetaData()
        {
            _init();
        }
        
        protected void _init()
        {
            SetId(11);
            SetAttributes(new Sybase.Reflection.AttributeMetaDataList());
            SetOperations(new Sybase.Reflection.OperationMetaDataList());
            SetAttributeMap(new Sybase.Reflection.AttributeMap());
            SetOperationMap(new Sybase.Reflection.OperationMap());
            Sybase.Reflection.AttributeMetaData domain_attribute = AddAttributeWithParams(0, "domain", "string", -1, false, false, false, false, false, false);
            Sybase.Reflection.AttributeMetaData package_attribute = AddAttributeWithParams(1, "package", "string", -1, false, false, false, false, false, false);
            Sybase.Reflection.AttributeMetaData syncGroup_attribute = AddAttributeWithParams(2, "syncGroup", "string", -1, false, false, false, false, false, false);
            Sybase.Reflection.AttributeMetaData clientId_attribute = AddAttributeWithParams(3, "clientId", "string", -1, false, false, false, false, false, false);
            InitAttributeMapFromAttributes();
            SetName("SISSubscriptionKey");
            
        }
        
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public  override bool IsEntity()
        {
            return false;
        }
        
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public  override bool IsService()
        {
            return false;
        }
    }
}