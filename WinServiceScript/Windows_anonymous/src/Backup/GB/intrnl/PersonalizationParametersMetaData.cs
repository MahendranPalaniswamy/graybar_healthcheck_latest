
namespace GB.intrnl
{
    public class PersonalizationParametersMetaData
        : Sybase.Reflection.ClassMetaData
    {
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public PersonalizationParametersMetaData()
        {
            _init();
        }
        
        protected void _init()
        {
            SetId(16);
            SetAttributes(new Sybase.Reflection.AttributeMetaDataList());
            SetOperations(new Sybase.Reflection.OperationMetaDataList());
            SetAttributeMap(new Sybase.Reflection.AttributeMap());
            SetOperationMap(new Sybase.Reflection.OperationMap());
            Sybase.Reflection.AttributeMetaData listSessionPK_attribute = AddAttributeWithParams(0, "listSessionPK", "GB.SessionPersonalization*", -1, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData username_attribute = AddAttributeWithParams(1, "username", "string", -1, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData usernameUserDefined_attribute = AddAttributeWithParams(2, "usernameUserDefined", "boolean", -1, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData password_attribute = AddAttributeWithParams(3, "password", "string", -1, false, false, true, false, false, true);
            Sybase.Reflection.AttributeMetaData passwordUserDefined_attribute = AddAttributeWithParams(4, "passwordUserDefined", "boolean", -1, false, false, true, false, false, false);
            InitAttributeMapFromAttributes();
            AddOperationWithParams(352, "load", "void", false);
            AddOperationWithParams(356, "save", "void", false);
            AddOperationWithParams(360, "saveUserNamePassword", "void", false);
            InitOperationMapFromOperations();
            SetName("PersonalizationParameters");
            
        }
        
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public  override bool IsEntity()
        {
            return false;
        }
        
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public  override bool IsService()
        {
            return false;
        }
    }
}