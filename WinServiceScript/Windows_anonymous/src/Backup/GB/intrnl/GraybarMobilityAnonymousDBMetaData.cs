namespace GB.intrnl
{
    public class GraybarMobilityAnonymousDBMetaData
        : Sybase.Reflection.DatabaseMetaData
    {
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public GraybarMobilityAnonymousDBMetaData()
        {
            _init();
        }
        
        protected void _init()
        {
            SetId(10);
            SetAttributes(new Sybase.Reflection.AttributeMetaDataList());
            SetOperations(new Sybase.Reflection.OperationMetaDataList());
            SetAttributeMap(new Sybase.Reflection.AttributeMap());
            SetOperationMap(new Sybase.Reflection.OperationMap());
            InitAttributeMapFromAttributes();
            AddOperationWithParams(8, "createDatabase", "void", true);
            Sybase.Reflection.OperationMetaData CleanAllData1_operation = AddOperationWithParams(9, "cleanAllData", "void", true);
            AddParameterForOperation(CleanAllData1_operation, "p0", "boolean");
            AddOperationWithParams(11, "cleanAllData", "void", true);
            AddOperationWithParams(12, "getSyncUsername", "string", true);
            Sybase.Reflection.OperationMetaData LoginToSync4_operation = AddOperationWithParams(15, "loginToSync", "void", true);
            AddParameterForOperation(LoginToSync4_operation, "p0", "string");
            AddParameterForOperation(LoginToSync4_operation, "p1", "string");
            Sybase.Reflection.OperationMetaData OnlineLogin5_operation = AddOperationWithParams(18, "onlineLogin", "void", true);
            AddParameterForOperation(OnlineLogin5_operation, "p0", "string");
            AddParameterForOperation(OnlineLogin5_operation, "p1", "string");
            Sybase.Reflection.OperationMetaData OfflineLogin6_operation = AddOperationWithParams(21, "offlineLogin", "boolean", true);
            AddParameterForOperation(OfflineLogin6_operation, "p0", "string");
            AddParameterForOperation(OfflineLogin6_operation, "p1", "string");
            AddOperationWithParams(24, "initialSync", "void", true);
            Sybase.Reflection.OperationMetaData Synchronize8_operation = AddOperationWithParams(25, "synchronize", "void", true);
            AddParameterForOperation(Synchronize8_operation, "p0", "string");
            Sybase.Reflection.OperationMetaData Synchronize9_operation = AddOperationWithParams(27, "synchronize", "void", true);
            AddParameterForOperation(Synchronize9_operation, "p0", "Sybase.Persistence.SyncStatusListener");
            AddOperationWithParams(29, "getSynchronizationProfile", "Sybase.Persistence.ConnectionProfile", true);
            Sybase.Reflection.OperationMetaData Synchronize11_operation = AddOperationWithParams(30, "synchronize", "void", true);
            AddParameterForOperation(Synchronize11_operation, "p0", "string");
            AddParameterForOperation(Synchronize11_operation, "p1", "Sybase.Persistence.SyncStatusListener");
            Sybase.Reflection.OperationMetaData GetSynchronizationGroup12_operation = AddOperationWithParams(33, "getSynchronizationGroup", "Sybase.Persistence.ISynchronizationGroup", true);
            AddParameterForOperation(GetSynchronizationGroup12_operation, "p0", "string");
            Sybase.Reflection.OperationMetaData RegisterCallbackHandler13_operation = AddOperationWithParams(35, "registerCallbackHandler", "void", true);
            AddParameterForOperation(RegisterCallbackHandler13_operation, "p0", "Sybase.Persistence.ICallbackHandler");
            AddOperationWithParams(37, "getAllSynchronizationGroups", "Sybase.Collections.GenericList<Sybase.Persistence.ISynchronizationGroup>", true);
            Sybase.Reflection.OperationMetaData BeginSynchronize15_operation = AddOperationWithParams(38, "beginSynchronize", "void", true);
            AddParameterForOperation(BeginSynchronize15_operation, "p0", "Sybase.Collections.GenericList<Sybase.Persistence.ISynchronizationGroup>");
            AddParameterForOperation(BeginSynchronize15_operation, "p1", "Object");
            AddOperationWithParams(41, "submitPendingOperations", "void", true);
            AddOperationWithParams(42, "synchronize", "void", true);
            Sybase.Reflection.OperationMetaData SubmitPendingOperations18_operation = AddOperationWithParams(43, "submitPendingOperations", "void", true);
            AddParameterForOperation(SubmitPendingOperations18_operation, "p0", "string");
            AddOperationWithParams(45, "cancelPendingOperations", "void", true);
            Sybase.Reflection.OperationMetaData GetLogRecords20_operation = AddOperationWithParams(46, "getLogRecords", "object", true);
            AddParameterForOperation(GetLogRecords20_operation, "p0", "Sybase.Persistence.Query");
            AddOperationWithParams(48, "getLogger", "Sybase.Persistence.ILogger", true);
            AddOperationWithParams(49, "submitLogRecords", "void", true);
            AddOperationWithParams(50, "deleteDatabase", "void", true);
            Sybase.Reflection.OperationMetaData IsSynchronized24_operation = AddOperationWithParams(51, "isSynchronized", "boolean", true);
            AddParameterForOperation(IsSynchronized24_operation, "p0", "string");
            Sybase.Reflection.OperationMetaData GetLastSynchronizationTime25_operation = AddOperationWithParams(53, "getLastSynchronizationTime", "dateTime", true);
            AddParameterForOperation(GetLastSynchronizationTime25_operation, "p0", "string");
            AddOperationWithParams(295, "getPersonalizationParameters", "PersonalizationParameters", true);
            InitOperationMapFromOperations();
            SetName("GraybarMobilityAnonymousDB");
            Sybase.Reflection.ClassMetaDataList _classList = new Sybase.Reflection.ClassMetaDataList(20);
            Sybase.Reflection.ClassMap _classMap = new Sybase.Reflection.ClassMap();
            SetClassList(_classList);
            SetClassMap(_classMap);
            GB.intrnl.BranchesMetaData _BranchesMetaData = new GB.intrnl.BranchesMetaData();
            _classList.Add(_BranchesMetaData);
            _classMap.Add("Branches", _BranchesMetaData);
            GB.intrnl.LogRecordImplMetaData _LogRecordImplMetaData = new GB.intrnl.LogRecordImplMetaData();
            _classList.Add(_LogRecordImplMetaData);
            _classMap.Add("LogRecordImpl", _LogRecordImplMetaData);
            GB.intrnl.OperationReplayMetaData _OperationReplayMetaData = new GB.intrnl.OperationReplayMetaData();
            _classList.Add(_OperationReplayMetaData);
            _classMap.Add("OperationReplay", _OperationReplayMetaData);
            GB.intrnl.SISSubscriptionKeyMetaData _SISSubscriptionKeyMetaData = new GB.intrnl.SISSubscriptionKeyMetaData();
            _classList.Add(_SISSubscriptionKeyMetaData);
            _classMap.Add("SISSubscriptionKey", _SISSubscriptionKeyMetaData);
            GB.intrnl.SISSubscriptionMetaData _SISSubscriptionMetaData = new GB.intrnl.SISSubscriptionMetaData();
            _classList.Add(_SISSubscriptionMetaData);
            _classMap.Add("SISSubscription", _SISSubscriptionMetaData);
            GB.intrnl.PackagePropertiesMetaData _PackagePropertiesMetaData = new GB.intrnl.PackagePropertiesMetaData();
            _classList.Add(_PackagePropertiesMetaData);
            _classMap.Add("PackageProperties", _PackagePropertiesMetaData);
            GB.intrnl.ChangeLogKeyMetaData _ChangeLogKeyMetaData = new GB.intrnl.ChangeLogKeyMetaData();
            _classList.Add(_ChangeLogKeyMetaData);
            _classMap.Add("ChangeLogKey", _ChangeLogKeyMetaData);
            GB.intrnl.ChangeLogImplMetaData _ChangeLogImplMetaData = new GB.intrnl.ChangeLogImplMetaData();
            _classList.Add(_ChangeLogImplMetaData);
            _classMap.Add("ChangeLogImpl", _ChangeLogImplMetaData);
            GB.intrnl.OfflineAuthenticationMetaData _OfflineAuthenticationMetaData = new GB.intrnl.OfflineAuthenticationMetaData();
            _classList.Add(_OfflineAuthenticationMetaData);
            _classMap.Add("OfflineAuthentication", _OfflineAuthenticationMetaData);
            GB.intrnl.KeyPackageNameMetaData _KeyPackageNameMetaData = new GB.intrnl.KeyPackageNameMetaData();
            _classList.Add(_KeyPackageNameMetaData);
            _classMap.Add("KeyPackageName", _KeyPackageNameMetaData);
            GB.intrnl.PersonalizationParametersMetaData _PersonalizationParametersMetaData = new GB.intrnl.PersonalizationParametersMetaData();
            _classList.Add(_PersonalizationParametersMetaData);
            _classMap.Add("PersonalizationParameters", _PersonalizationParametersMetaData);
            GB.intrnl.SessionPersonalizationMetaData _SessionPersonalizationMetaData = new GB.intrnl.SessionPersonalizationMetaData();
            _classList.Add(_SessionPersonalizationMetaData);
            _classMap.Add("SessionPersonalization", _SessionPersonalizationMetaData);
            GB.intrnl.KeyGeneratorMetaData _KeyGeneratorMetaData = new GB.intrnl.KeyGeneratorMetaData();
            _classList.Add(_KeyGeneratorMetaData);
            _classMap.Add("KeyGenerator", _KeyGeneratorMetaData);
            GB.intrnl.KeyGeneratorPKMetaData _KeyGeneratorPKMetaData = new GB.intrnl.KeyGeneratorPKMetaData();
            _classList.Add(_KeyGeneratorPKMetaData);
            _classMap.Add("KeyGeneratorPK", _KeyGeneratorPKMetaData);
            GB.intrnl.LocalKeyGeneratorMetaData _LocalKeyGeneratorMetaData = new GB.intrnl.LocalKeyGeneratorMetaData();
            _classList.Add(_LocalKeyGeneratorMetaData);
            _classMap.Add("LocalKeyGenerator", _LocalKeyGeneratorMetaData);
            GB.intrnl.LocalKeyGeneratorPKMetaData _LocalKeyGeneratorPKMetaData = new GB.intrnl.LocalKeyGeneratorPKMetaData();
            _classList.Add(_LocalKeyGeneratorPKMetaData);
            _classMap.Add("LocalKeyGeneratorPK", _LocalKeyGeneratorPKMetaData);
            GB.intrnl.LocalStorageMetaData _LocalStorageMetaData = new GB.intrnl.LocalStorageMetaData();
            _classList.Add(_LocalStorageMetaData);
            _classMap.Add("LocalStorage", _LocalStorageMetaData);
            Sybase.Reflection.EntityMetaDataList _entityList = new Sybase.Reflection.EntityMetaDataList(20);
            Sybase.Reflection.EntityMap _entityMap = new Sybase.Reflection.EntityMap();
            SetEntityList(_entityList);
            SetEntityMap(_entityMap);
            _entityList.Add(_BranchesMetaData);
            _entityMap.Add("Branches", _BranchesMetaData);
            _entityList.Add(_LogRecordImplMetaData);
            _entityMap.Add("LogRecordImpl", _LogRecordImplMetaData);
            _entityList.Add(_OperationReplayMetaData);
            _entityMap.Add("OperationReplay", _OperationReplayMetaData);
            _entityList.Add(_SISSubscriptionMetaData);
            _entityMap.Add("SISSubscription", _SISSubscriptionMetaData);
            _entityList.Add(_PackagePropertiesMetaData);
            _entityMap.Add("PackageProperties", _PackagePropertiesMetaData);
            _entityList.Add(_ChangeLogImplMetaData);
            _entityMap.Add("ChangeLogImpl", _ChangeLogImplMetaData);
            _entityList.Add(_OfflineAuthenticationMetaData);
            _entityMap.Add("OfflineAuthentication", _OfflineAuthenticationMetaData);
            _entityList.Add(_KeyGeneratorMetaData);
            _entityMap.Add("KeyGenerator", _KeyGeneratorMetaData);
            _entityList.Add(_LocalKeyGeneratorMetaData);
            _entityMap.Add("LocalKeyGenerator", _LocalKeyGeneratorMetaData);
            _entityList.Add(_LocalStorageMetaData);
            _entityMap.Add("LocalStorage", _LocalStorageMetaData);
            Sybase.Collections.StringList _publications = new Sybase.Collections.StringList(20);
            Sybase.Reflection.EntityListMap _publicationsToEntities = new Sybase.Reflection.EntityListMap();
            _publications.Add("default");
            Sybase.Reflection.EntityMetaDataList defaultEntities = new Sybase.Reflection.EntityMetaDataList(20);
            defaultEntities.Add(_BranchesMetaData);
            defaultEntities.Add(_LogRecordImplMetaData);
            defaultEntities.Add(_OperationReplayMetaData);
            defaultEntities.Add(_SISSubscriptionMetaData);
            defaultEntities.Add(_PackagePropertiesMetaData);
            defaultEntities.Add(_ChangeLogImplMetaData);
            defaultEntities.Add(_KeyGeneratorMetaData);
            _publicationsToEntities.Add("default", defaultEntities);
            Sybase.Reflection.EntityMetaDataList unsubscribeEntities = new Sybase.Reflection.EntityMetaDataList(20);
            unsubscribeEntities.Add(_KeyGeneratorMetaData);
            _publicationsToEntities.Add("unsubscribe", unsubscribeEntities);
            _publications.Add("system");
            Sybase.Reflection.EntityMetaDataList systemEntities = new Sybase.Reflection.EntityMetaDataList(20);
            systemEntities.Add(_LogRecordImplMetaData);
            systemEntities.Add(_OperationReplayMetaData);
            systemEntities.Add(_SISSubscriptionMetaData);
            systemEntities.Add(_PackagePropertiesMetaData);
            systemEntities.Add(_KeyGeneratorMetaData);
            _publicationsToEntities.Add("system", systemEntities);
            Sybase.Reflection.EntityMetaDataList operationReplayEntities = new Sybase.Reflection.EntityMetaDataList(20);
            operationReplayEntities.Add(_OperationReplayMetaData);
            _publicationsToEntities.Add("operationReplay", operationReplayEntities);
            _publications.Add("initialSync");
            Sybase.Reflection.EntityMetaDataList initialSyncEntities = new Sybase.Reflection.EntityMetaDataList(20);
            initialSyncEntities.Add(_PackagePropertiesMetaData);
            initialSyncEntities.Add(_KeyGeneratorMetaData);
            _publicationsToEntities.Add("initialSync", initialSyncEntities);
            SetDatabaseFile("graybarMobilityAnonymous1_0.udb");
            SetDatabaseName("graybarMobilityAnonymous1_0");
            InitEntityListMap(_publicationsToEntities);
            SetSynchronizationGroups(_publications);
        }
        
        
        
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public  override bool IsEntity()
        {
            return false;
        }
        
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public  override bool IsService()
        {
            return false;
        }
    }
}