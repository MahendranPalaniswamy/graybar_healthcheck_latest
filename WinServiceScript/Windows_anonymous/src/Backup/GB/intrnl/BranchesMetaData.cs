
namespace GB.intrnl
{
    public class BranchesMetaData
        : Sybase.Reflection.EntityMetaData
    {
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public BranchesMetaData()
        {
            _init();
        }
        
        protected void _init()
        {
            SetId(0);
            SetAttributes(new Sybase.Reflection.AttributeMetaDataList());
            SetOperations(new Sybase.Reflection.OperationMetaDataList());
            SetAttributeMap(new Sybase.Reflection.AttributeMap());
            SetOperationMap(new Sybase.Reflection.OperationMap());
            Sybase.Reflection.AttributeMetaData locationId_attribute = AddAttributeWithParams(0, "locationId", "int", -1, false, false, false, false, false, false, "\"a\"");
            Sybase.Reflection.AttributeMetaData plantCode_attribute = AddAttributeWithParams(1, "plantCode", "string?", 4, false, false, false, false, false, false, "\"b\"");
            Sybase.Reflection.AttributeMetaData branchName_attribute = AddAttributeWithParams(2, "branchName", "string?", 50, false, false, false, false, false, false, "\"c\"");
            Sybase.Reflection.AttributeMetaData streetAddress_attribute = AddAttributeWithParams(3, "streetAddress", "string?", 100, false, false, false, false, false, false, "\"d\"");
            Sybase.Reflection.AttributeMetaData streetAddress2_attribute = AddAttributeWithParams(4, "streetAddress2", "string?", 100, false, false, false, false, false, false, "\"e\"");
            Sybase.Reflection.AttributeMetaData city_attribute = AddAttributeWithParams(5, "city", "string?", 30, false, false, false, false, false, false, "\"f\"");
            Sybase.Reflection.AttributeMetaData state_attribute = AddAttributeWithParams(6, "state", "string?", 2, false, false, false, false, false, false, "\"g\"");
            Sybase.Reflection.AttributeMetaData zip_attribute = AddAttributeWithParams(7, "zip", "string?", 10, false, false, false, false, false, false, "\"h\"");
            Sybase.Reflection.AttributeMetaData phone_attribute = AddAttributeWithParams(8, "phone", "string?", 20, false, false, false, false, false, false, "\"i\"");
            Sybase.Reflection.AttributeMetaData latitude_attribute = AddAttributeWithParams(9, "latitude", "string?", 25, false, false, false, false, false, false, "\"j\"");
            Sybase.Reflection.AttributeMetaData longitude_attribute = AddAttributeWithParams(10, "longitude", "string?", 25, false, false, false, false, false, false, "\"l\"");
            Sybase.Reflection.AttributeMetaData fax_attribute = AddAttributeWithParams(11, "fax", "string?", 20, false, false, false, false, false, false, "\"m\"");
            Sybase.Reflection.AttributeMetaData locationType_attribute = AddAttributeWithParams(12, "locationType", "string?", 20, false, false, false, false, false, false, "\"n\"");
            Sybase.Reflection.AttributeMetaData upsShippingNumber_attribute = AddAttributeWithParams(13, "upsShippingNumber", "string?", 50, false, false, false, false, false, false, "\"o\"");
            Sybase.Reflection.AttributeMetaData counterHours_attribute = AddAttributeWithParams(14, "counterHours", "string?", 50, false, false, false, false, false, false, "\"p\"");
            Sybase.Reflection.AttributeMetaData mobileShippingPlant_attribute = AddAttributeWithParams(15, "mobileShippingPlant", "string?", 4, false, false, false, false, false, false, "\"q\"");
            Sybase.Reflection.AttributeMetaData mobileRoute_attribute = AddAttributeWithParams(16, "mobileRoute", "string?", 10, false, false, false, false, false, false, "\"r\"");
            Sybase.Reflection.AttributeMetaData timeZone_attribute = AddAttributeWithParams(17, "timeZone", "string?", 50, false, false, false, false, false, false, "\"s\"");
            Sybase.Reflection.AttributeMetaData mobileShippingCapable_attribute = AddAttributeWithParams(18, "mobileShippingCapable", "string?", 1, false, false, false, false, false, false, "\"t\"");
            Sybase.Reflection.AttributeMetaData surrogateKey_attribute = AddAttributeWithParams(19, "surrogateKey", "long", -1, false, true, false, false, false, false, "\"u\"");
            InitAttributeMapFromAttributes();
            AddOperationWithParams(78, "findAll", "Branches*", true);
            Sybase.Reflection.OperationMetaData FindByPrimaryKey1_operation = AddOperationWithParams(80, "findByPrimaryKey", "Branches", true);
            AddParameterForOperation(FindByPrimaryKey1_operation, "locationId", "int");
            AddOperationWithParams(83, "getAllBranches", "Sybase.Persistence.QueryResultSet", true);
            Sybase.Reflection.OperationMetaData GetBranchByMobileShippingPlant3_operation = AddOperationWithParams(85, "getBranchByMobileShippingPlant", "Sybase.Persistence.QueryResultSet", true);
            AddParameterForOperation(GetBranchByMobileShippingPlant3_operation, "mob_shipping_plant", "string?");
            Sybase.Reflection.OperationMetaData GetBranchByPlantCode4_operation = AddOperationWithParams(88, "getBranchByPlantCode", "Sybase.Persistence.QueryResultSet", true);
            AddParameterForOperation(GetBranchByPlantCode4_operation, "plantCode", "string?");
            AddOperationWithParams(644, "isNew", "boolean", false);
            AddOperationWithParams(645, "isDirty", "boolean", false);
            AddOperationWithParams(646, "isDeleted", "boolean", false);
            AddOperationWithParams(647, "refresh", "void", false);
            AddOperationWithParams(648, "_pk", "long?", false);
            Sybase.Reflection.OperationMetaData FindWithQuery10_operation = AddOperationWithParams(649, "findWithQuery", "Branches*", true);
            AddParameterForOperation(FindWithQuery10_operation, "query", "Sybase.Persistence.Query");
            Sybase.Reflection.OperationMetaData GetSize11_operation = AddOperationWithParams(651, "getSize", "int", true);
            AddParameterForOperation(GetSize11_operation, "query", "Sybase.Persistence.Query");
            InitOperationMapFromOperations();
            SetName("Branches");
            SetTable("\"graybarmobilityanonymous_1_0_branches\"");
            SetSynchronizationGroup("default");
        }
    }
}