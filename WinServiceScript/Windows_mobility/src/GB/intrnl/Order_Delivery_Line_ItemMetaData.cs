
namespace GB.intrnl
{
    public class Order_Delivery_Line_ItemMetaData
        : Sybase.Reflection.EntityMetaData
    {
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public Order_Delivery_Line_ItemMetaData()
        {
            _init();
        }
        
        protected void _init()
        {
            SetId(20);
            SetAttributes(new Sybase.Reflection.AttributeMetaDataList());
            SetOperations(new Sybase.Reflection.OperationMetaDataList());
            SetAttributeMap(new Sybase.Reflection.AttributeMap());
            SetOperationMap(new Sybase.Reflection.OperationMap());
            Sybase.Reflection.AttributeMetaData SOLD_TO_attribute = AddAttributeWithParams(0, "SOLD_TO", "string", 10, false, false, false, false, false, false, "\"a\"");
            Sybase.Reflection.AttributeMetaData ORDER_NUMBER_attribute = AddAttributeWithParams(1, "ORDER_NUMBER", "string", 10, false, false, false, false, false, false, "\"b\"");
            Sybase.Reflection.AttributeMetaData MOBILE_ORDER_NUMBER_attribute = AddAttributeWithParams(2, "MOBILE_ORDER_NUMBER", "string", 20, false, false, false, false, false, false, "\"c\"");
            Sybase.Reflection.AttributeMetaData LINE_ITEM_ID_attribute = AddAttributeWithParams(3, "LINE_ITEM_ID", "string", 6, false, false, false, false, false, false, "\"d\"");
            Sybase.Reflection.AttributeMetaData DELIVERY_NUMBER_attribute = AddAttributeWithParams(4, "DELIVERY_NUMBER", "string", 10, false, false, false, false, false, false, "\"e\"");
            Sybase.Reflection.AttributeMetaData DELIVERY_ITEM_ID_attribute = AddAttributeWithParams(5, "DELIVERY_ITEM_ID", "string", 6, false, false, false, false, false, false, "\"f\"");
            Sybase.Reflection.AttributeMetaData ITEM_STATUS_attribute = AddAttributeWithParams(6, "ITEM_STATUS", "string", 1, false, false, false, false, false, false, "\"g\"");
            Sybase.Reflection.AttributeMetaData ITEM_STATUS_TEXT_attribute = AddAttributeWithParams(7, "ITEM_STATUS_TEXT", "string", 25, false, false, false, false, false, false, "\"h\"");
            Sybase.Reflection.AttributeMetaData SHIPPING_METHOD_attribute = AddAttributeWithParams(8, "SHIPPING_METHOD", "string", 1024, false, false, false, false, false, false, "\"i\"");
            Sybase.Reflection.AttributeMetaData QUANTITY_attribute = AddAttributeWithParams(9, "QUANTITY", "decimal", -1, false, false, false, false, false, false, "\"j\"");
            Sybase.Reflection.AttributeMetaData UNIT_attribute = AddAttributeWithParams(10, "UNIT", "string", 3, false, false, false, false, false, false, "\"l\"");
            Sybase.Reflection.AttributeMetaData orderDeliveryHeader_attribute = AddAttributeWithParams(11, "orderDeliveryHeader", "GB.Order_Delivery_Header", -1, false, false, false, true, false, false);
            Sybase.Reflection.AttributeMetaData pending_attribute = AddAttributeWithParams(20001, "pending", "boolean", -1, false, false, true, false, false, false, "\"pending\"");
            Sybase.Reflection.AttributeMetaData pendingChange_attribute = AddAttributeWithParams(20002, "pendingChange", "char", 1, false, false, true, false, false, false, "\"_pc\"");
            Sybase.Reflection.AttributeMetaData replayPending_attribute = AddAttributeWithParams(20005, "replayPending", "long", -1, false, false, true, false, false, false, "\"_rp\"");
            Sybase.Reflection.AttributeMetaData replayFailure_attribute = AddAttributeWithParams(20006, "replayFailure", "long", -1, false, false, true, false, false, false, "\"_rf\"");
            Sybase.Reflection.AttributeMetaData cvpOperation_attribute = AddAttributeWithParams(12, "cvpOperation", "bigString?", -1, false, false, true, false, false, false, "\"cvp_operation_header\"");
            Sybase.Reflection.AttributeMetaData cvpOperationLobs_attribute = AddAttributeWithParams(13, "cvpOperationLobs", "bigString?", -1, false, false, true, false, false, false, "\"cvp_operation_lobs\"");
            Sybase.Reflection.AttributeMetaData orderDeliveryHeaderFK_attribute = AddAttributeWithParams(14, "orderDeliveryHeaderFK", "long?", -1, false, false, true, false, false, false, "\"orderDeliveryHeaderFK\"");
            Sybase.Reflection.AttributeMetaData surrogateKey_attribute = AddAttributeWithParams(15, "surrogateKey", "long", -1, false, true, false, false, false, false, "\"m\"");
            Sybase.Reflection.AttributeMetaData replayCounter_attribute = AddAttributeWithParams(20004, "replayCounter", "long", -1, false, false, true, false, false, false, "\"_rc\"");
            Sybase.Reflection.AttributeMetaData disableSubmit_attribute = AddAttributeWithParams(20003, "disableSubmit", "boolean", -1, false, false, true, false, false, false, "\"_ds\"");
            Sybase.Reflection.AttributeMetaData cvpOperationLength_attribute = AddAttributeWithParams(16, "cvpOperationLength", "long", -1, false, false, true, false, false, false, "\"cvpOperation_length\"");
            Sybase.Reflection.AttributeMetaData cvpOperationLobsLength_attribute = AddAttributeWithParams(17, "cvpOperationLobsLength", "long", -1, false, false, true, false, false, false, "\"cvpOperationLobs_length\"");
            InitAttributeMapFromAttributes();
            AddOperationWithParams(1464, "findAll", "Order_Delivery_Line_Item*", true);
            Sybase.Reflection.OperationMetaData FindByORDER_NUMBER1_operation = AddOperationWithParams(1466, "findByORDER_NUMBER", "Order_Delivery_Line_Item*", true);
            AddParameterForOperation(FindByORDER_NUMBER1_operation, "ORDER_NUMBER", "string");
            Sybase.Reflection.OperationMetaData FindByMOBILE_ORDER_NUMBER2_operation = AddOperationWithParams(1469, "findByMOBILE_ORDER_NUMBER", "Order_Delivery_Line_Item*", true);
            AddParameterForOperation(FindByMOBILE_ORDER_NUMBER2_operation, "MOBILE_ORDER_NUMBER", "string");
            Sybase.Reflection.OperationMetaData FindByLINE_ITEM_ID3_operation = AddOperationWithParams(1472, "findByLINE_ITEM_ID", "Order_Delivery_Line_Item*", true);
            AddParameterForOperation(FindByLINE_ITEM_ID3_operation, "LINE_ITEM_ID", "string");
            Sybase.Reflection.OperationMetaData FindByDELIVERY_NUMBER4_operation = AddOperationWithParams(1475, "findByDELIVERY_NUMBER", "Order_Delivery_Line_Item*", true);
            AddParameterForOperation(FindByDELIVERY_NUMBER4_operation, "DELIVERY_NUMBER", "string");
            Sybase.Reflection.OperationMetaData FindByDELIVERY_ITEM_ID5_operation = AddOperationWithParams(1478, "findByDELIVERY_ITEM_ID", "Order_Delivery_Line_Item*", true);
            AddParameterForOperation(FindByDELIVERY_ITEM_ID5_operation, "DELIVERY_ITEM_ID", "string");
            Sybase.Reflection.OperationMetaData FindByPrimaryKey6_operation = AddOperationWithParams(1481, "findByPrimaryKey", "Order_Delivery_Line_Item", true);
            AddParameterForOperation(FindByPrimaryKey6_operation, "ORDER_NUMBER", "string");
            AddParameterForOperation(FindByPrimaryKey6_operation, "MOBILE_ORDER_NUMBER", "string");
            AddParameterForOperation(FindByPrimaryKey6_operation, "LINE_ITEM_ID", "string");
            AddParameterForOperation(FindByPrimaryKey6_operation, "DELIVERY_NUMBER", "string");
            AddParameterForOperation(FindByPrimaryKey6_operation, "DELIVERY_ITEM_ID", "string");
            Sybase.Reflection.OperationMetaData GetDeliveryLineItems7_operation = AddOperationWithParams(1488, "getDeliveryLineItems", "Sybase.Persistence.QueryResultSet", true);
            AddParameterForOperation(GetDeliveryLineItems7_operation, "order_number", "string");
            AddParameterForOperation(GetDeliveryLineItems7_operation, "line_item_id", "string");
            AddParameterForOperation(GetDeliveryLineItems7_operation, "mobile_order_number", "string");
            Sybase.Reflection.OperationMetaData GetOrderDeliveryLineItems_for_Order_Delivery_Header8_operation = AddOperationWithParams(3213, "getOrderDeliveryLineItems_for_Order_Delivery_Header", "Order_Delivery_Line_Item*", true);
            AddParameterForOperation(GetOrderDeliveryLineItems_for_Order_Delivery_Header8_operation, "surrogateKey", "long?");
            Sybase.Reflection.OperationMetaData DeleteOSOrderDeliveryLineItems_for_Order_Delivery_Header9_operation = AddOperationWithParams(3216, "deleteOSOrderDeliveryLineItems_for_Order_Delivery_Header", "void", true);
            AddParameterForOperation(DeleteOSOrderDeliveryLineItems_for_Order_Delivery_Header9_operation, "surrogateKey", "long?");
            Sybase.Reflection.OperationMetaData DeleteOrderDeliveryLineItems_for_Order_Delivery_Header10_operation = AddOperationWithParams(3218, "deleteOrderDeliveryLineItems_for_Order_Delivery_Header", "void", true);
            AddParameterForOperation(DeleteOrderDeliveryLineItems_for_Order_Delivery_Header10_operation, "surrogateKey", "long?");
            AddOperationWithParams(3238, "getPendingObjects", "Order_Delivery_Line_Item*", true);
            Sybase.Reflection.OperationMetaData GetPendingObjects12_operation = AddOperationWithParams(3240, "getPendingObjects", "Order_Delivery_Line_Item*", true);
            AddParameterForOperation(GetPendingObjects12_operation, "pendingChange", "char");
            AddOperationWithParams(3243, "getReplayPendingObjects", "Order_Delivery_Line_Item*", true);
            AddOperationWithParams(4383, "isNew", "boolean", false);
            AddOperationWithParams(4384, "isDirty", "boolean", false);
            AddOperationWithParams(4385, "isDeleted", "boolean", false);
            AddOperationWithParams(4386, "refresh", "void", false);
            AddOperationWithParams(4387, "_pk", "long?", false);
            AddOperationWithParams(4388, "isPending", "boolean", false);
            AddOperationWithParams(4389, "isCreated", "boolean", false);
            AddOperationWithParams(4390, "isUpdated", "boolean", false);
            AddOperationWithParams(4391, "submitPending", "void", false);
            AddOperationWithParams(4392, "cancelPending", "void", false);
            AddOperationWithParams(4393, "submitPendingOperations", "void", true);
            AddOperationWithParams(4394, "cancelPendingOperations", "void", true);
            AddOperationWithParams(4395, "save", "void", false);
            Sybase.Reflection.OperationMetaData FindWithQuery27_operation = AddOperationWithParams(4396, "findWithQuery", "Order_Delivery_Line_Item*", true);
            AddParameterForOperation(FindWithQuery27_operation, "query", "Sybase.Persistence.Query");
            Sybase.Reflection.OperationMetaData GetSize28_operation = AddOperationWithParams(4398, "getSize", "int", true);
            AddParameterForOperation(GetSize28_operation, "query", "Sybase.Persistence.Query");
            InitOperationMapFromOperations();
            SetName("Order_Delivery_Line_Item");
            SetTable("\"graybarmobility_1_0_order_delivery_line_item\"");
            SetSynchronizationGroup("OrderRelatedSyncGroup");
        }
    }
}