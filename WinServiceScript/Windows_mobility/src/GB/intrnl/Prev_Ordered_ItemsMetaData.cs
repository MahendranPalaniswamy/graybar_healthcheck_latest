
namespace GB.intrnl
{
    public class Prev_Ordered_ItemsMetaData
        : Sybase.Reflection.EntityMetaData
    {
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public Prev_Ordered_ItemsMetaData()
        {
            _init();
        }
        
        protected void _init()
        {
            SetId(27);
            SetAttributes(new Sybase.Reflection.AttributeMetaDataList());
            SetOperations(new Sybase.Reflection.OperationMetaDataList());
            SetAttributeMap(new Sybase.Reflection.AttributeMap());
            SetOperationMap(new Sybase.Reflection.OperationMap());
            Sybase.Reflection.AttributeMetaData SOLD_TO_attribute = AddAttributeWithParams(0, "SOLD_TO", "string", 10, false, false, false, false, false, false, "\"a\"");
            Sybase.Reflection.AttributeMetaData PRODUCT_ID_attribute = AddAttributeWithParams(1, "PRODUCT_ID", "string", 18, false, false, false, false, false, false, "\"b\"");
            Sybase.Reflection.AttributeMetaData CATALOG_NUMBER_attribute = AddAttributeWithParams(2, "CATALOG_NUMBER", "string", 40, false, false, false, false, false, false, "\"c\"");
            Sybase.Reflection.AttributeMetaData MANUFACTURER_NAME_attribute = AddAttributeWithParams(3, "MANUFACTURER_NAME", "string", 35, false, false, false, false, false, false, "\"d\"");
            Sybase.Reflection.AttributeMetaData QUANTITY_attribute = AddAttributeWithParams(4, "QUANTITY", "string", 6, false, false, false, false, false, false, "\"e\"");
            Sybase.Reflection.AttributeMetaData LAST_PURCHASE_DATE_attribute = AddAttributeWithParams(5, "LAST_PURCHASE_DATE", "date?", -1, false, false, false, false, false, false, "\"f\"");
            Sybase.Reflection.AttributeMetaData DESCRIPTION_attribute = AddAttributeWithParams(6, "DESCRIPTION", "string", 1024, false, false, false, false, false, false, "\"g\"");
            Sybase.Reflection.AttributeMetaData IMAGE_URL_attribute = AddAttributeWithParams(7, "IMAGE_URL", "string", 1024, false, false, false, false, false, false, "\"h\"");
            Sybase.Reflection.AttributeMetaData pending_attribute = AddAttributeWithParams(20001, "pending", "boolean", -1, false, false, true, false, false, false, "\"pending\"");
            Sybase.Reflection.AttributeMetaData pendingChange_attribute = AddAttributeWithParams(20002, "pendingChange", "char", 1, false, false, true, false, false, false, "\"_pc\"");
            Sybase.Reflection.AttributeMetaData replayPending_attribute = AddAttributeWithParams(20005, "replayPending", "long", -1, false, false, true, false, false, false, "\"_rp\"");
            Sybase.Reflection.AttributeMetaData replayFailure_attribute = AddAttributeWithParams(20006, "replayFailure", "long", -1, false, false, true, false, false, false, "\"_rf\"");
            Sybase.Reflection.AttributeMetaData cvpOperation_attribute = AddAttributeWithParams(8, "cvpOperation", "bigString?", -1, false, false, true, false, false, false, "\"cvp_operation_header\"");
            Sybase.Reflection.AttributeMetaData cvpOperationLobs_attribute = AddAttributeWithParams(9, "cvpOperationLobs", "bigString?", -1, false, false, true, false, false, false, "\"cvp_operation_lobs\"");
            Sybase.Reflection.AttributeMetaData surrogateKey_attribute = AddAttributeWithParams(10, "surrogateKey", "long", -1, false, true, false, false, false, false, "\"i\"");
            Sybase.Reflection.AttributeMetaData replayCounter_attribute = AddAttributeWithParams(20004, "replayCounter", "long", -1, false, false, true, false, false, false, "\"_rc\"");
            Sybase.Reflection.AttributeMetaData disableSubmit_attribute = AddAttributeWithParams(20003, "disableSubmit", "boolean", -1, false, false, true, false, false, false, "\"_ds\"");
            Sybase.Reflection.AttributeMetaData cvpOperationLength_attribute = AddAttributeWithParams(11, "cvpOperationLength", "long", -1, false, false, true, false, false, false, "\"cvpOperation_length\"");
            Sybase.Reflection.AttributeMetaData cvpOperationLobsLength_attribute = AddAttributeWithParams(12, "cvpOperationLobsLength", "long", -1, false, false, true, false, false, false, "\"cvpOperationLobs_length\"");
            InitAttributeMapFromAttributes();
            AddOperationWithParams(1047, "findAll", "Prev_Ordered_Items*", true);
            Sybase.Reflection.OperationMetaData FindBySOLD_TO1_operation = AddOperationWithParams(1049, "findBySOLD_TO", "Prev_Ordered_Items*", true);
            AddParameterForOperation(FindBySOLD_TO1_operation, "SOLD_TO", "string");
            Sybase.Reflection.OperationMetaData FindByPRODUCT_ID2_operation = AddOperationWithParams(1052, "findByPRODUCT_ID", "Prev_Ordered_Items*", true);
            AddParameterForOperation(FindByPRODUCT_ID2_operation, "PRODUCT_ID", "string");
            Sybase.Reflection.OperationMetaData FindByPrimaryKey3_operation = AddOperationWithParams(1055, "findByPrimaryKey", "Prev_Ordered_Items", true);
            AddParameterForOperation(FindByPrimaryKey3_operation, "SOLD_TO", "string");
            AddParameterForOperation(FindByPrimaryKey3_operation, "PRODUCT_ID", "string");
            AddOperationWithParams(1059, "getAllPrevOrderedItems", "Sybase.Persistence.QueryResultSet", true);
            Sybase.Reflection.OperationMetaData FilterByDate5_operation = AddOperationWithParams(1061, "filterByDate", "Sybase.Persistence.QueryResultSet", true);
            AddParameterForOperation(FilterByDate5_operation, "date", "date?");
            Sybase.Reflection.OperationMetaData FilterByManufacturer6_operation = AddOperationWithParams(1064, "filterByManufacturer", "Sybase.Persistence.QueryResultSet", true);
            AddParameterForOperation(FilterByManufacturer6_operation, "manufacturer_name", "string");
            Sybase.Reflection.OperationMetaData FilterByDateAndManufacturer7_operation = AddOperationWithParams(1067, "filterByDateAndManufacturer", "Sybase.Persistence.QueryResultSet", true);
            AddParameterForOperation(FilterByDateAndManufacturer7_operation, "date", "date?");
            AddParameterForOperation(FilterByDateAndManufacturer7_operation, "manufacturer_name", "string");
            AddOperationWithParams(1075, "CreateDataWithDCN", "void", false, 'C');
            AddOperationWithParams(2916, "getPendingObjects", "Prev_Ordered_Items*", true);
            Sybase.Reflection.OperationMetaData GetPendingObjects10_operation = AddOperationWithParams(2918, "getPendingObjects", "Prev_Ordered_Items*", true);
            AddParameterForOperation(GetPendingObjects10_operation, "pendingChange", "char");
            AddOperationWithParams(2921, "getReplayPendingObjects", "Prev_Ordered_Items*", true);
            AddOperationWithParams(4240, "isNew", "boolean", false);
            AddOperationWithParams(4241, "isDirty", "boolean", false);
            AddOperationWithParams(4242, "isDeleted", "boolean", false);
            AddOperationWithParams(4243, "refresh", "void", false);
            AddOperationWithParams(4244, "_pk", "long?", false);
            AddOperationWithParams(4245, "isPending", "boolean", false);
            AddOperationWithParams(4246, "isCreated", "boolean", false);
            AddOperationWithParams(4247, "isUpdated", "boolean", false);
            AddOperationWithParams(4248, "submitPending", "void", false);
            AddOperationWithParams(4249, "cancelPending", "void", false);
            AddOperationWithParams(4250, "submitPendingOperations", "void", true);
            AddOperationWithParams(4251, "cancelPendingOperations", "void", true);
            AddOperationWithParams(4252, "save", "void", false);
            Sybase.Reflection.OperationMetaData FindWithQuery25_operation = AddOperationWithParams(4253, "findWithQuery", "Prev_Ordered_Items*", true);
            AddParameterForOperation(FindWithQuery25_operation, "query", "Sybase.Persistence.Query");
            Sybase.Reflection.OperationMetaData GetSize26_operation = AddOperationWithParams(4255, "getSize", "int", true);
            AddParameterForOperation(GetSize26_operation, "query", "Sybase.Persistence.Query");
            InitOperationMapFromOperations();
            SetName("Prev_Ordered_Items");
            SetTable("\"graybarmobility_1_0_prev_ordered_items\"");
            SetSynchronizationGroup("OrderRelatedSyncGroup");
        }
    }
}