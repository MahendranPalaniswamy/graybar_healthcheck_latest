
namespace GB.intrnl
{
    public class Order_OrderHeader_ShippingAddressMetaData
        : Sybase.Reflection.ClassMetaData
    {
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public Order_OrderHeader_ShippingAddressMetaData()
        {
            _init();
        }
        
        protected void _init()
        {
            SetId(60);
            SetAttributes(new Sybase.Reflection.AttributeMetaDataList());
            SetOperations(new Sybase.Reflection.OperationMetaDataList());
            SetAttributeMap(new Sybase.Reflection.AttributeMap());
            SetOperationMap(new Sybase.Reflection.OperationMap());
            Sybase.Reflection.AttributeMetaData Name_attribute = AddAttributeWithParams(0, "Name", "string", 300, false, false, false, false, false, false);
            Sybase.Reflection.AttributeMetaData Name1_attribute = AddAttributeWithParams(1, "Name1", "string", 300, false, false, false, false, false, false);
            Sybase.Reflection.AttributeMetaData CareOf_attribute = AddAttributeWithParams(2, "CareOf", "string", 300, false, false, false, false, false, false);
            Sybase.Reflection.AttributeMetaData Street1_attribute = AddAttributeWithParams(3, "Street1", "string", 300, false, false, false, false, false, false);
            Sybase.Reflection.AttributeMetaData Street2_attribute = AddAttributeWithParams(4, "Street2", "string", 300, false, false, false, false, false, false);
            Sybase.Reflection.AttributeMetaData City_attribute = AddAttributeWithParams(5, "City", "string", 300, false, false, false, false, false, false);
            Sybase.Reflection.AttributeMetaData State_attribute = AddAttributeWithParams(6, "State", "string", 300, false, false, false, false, false, false);
            Sybase.Reflection.AttributeMetaData ZipCode_attribute = AddAttributeWithParams(7, "ZipCode", "string", 300, false, false, false, false, false, false);
            Sybase.Reflection.AttributeMetaData Country_attribute = AddAttributeWithParams(8, "Country", "string", 300, false, false, false, false, false, false);
            Sybase.Reflection.AttributeMetaData Phone_attribute = AddAttributeWithParams(9, "Phone", "string", 300, false, false, false, false, false, false);
            InitAttributeMapFromAttributes();
            SetName("Order_OrderHeader_ShippingAddress");
            
        }
        
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public  override bool IsEntity()
        {
            return false;
        }
        
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public  override bool IsService()
        {
            return false;
        }
    }
}