
namespace GB.intrnl
{
    public class CategoryExtractMetaData
        : Sybase.Reflection.EntityMetaData
    {
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public CategoryExtractMetaData()
        {
            _init();
        }
        
        protected void _init()
        {
            SetId(0);
            SetAttributes(new Sybase.Reflection.AttributeMetaDataList());
            SetOperations(new Sybase.Reflection.OperationMetaDataList());
            SetAttributeMap(new Sybase.Reflection.AttributeMap());
            SetOperationMap(new Sybase.Reflection.OperationMap());
            Sybase.Reflection.AttributeMetaData name_attribute = AddAttributeWithParams(0, "name", "string", 301, false, false, false, false, false, false, "\"a\"");
            Sybase.Reflection.AttributeMetaData parentId_attribute = AddAttributeWithParams(1, "parentId", "string", 10, false, false, false, false, false, false, "\"b\"");
            Sybase.Reflection.AttributeMetaData categoryId_attribute = AddAttributeWithParams(2, "categoryId", "string", 10, false, false, false, false, false, false, "\"c\"");
            Sybase.Reflection.AttributeMetaData count_attribute = AddAttributeWithParams(3, "count", "string", 10, false, false, false, false, false, false, "\"d\"");
            Sybase.Reflection.AttributeMetaData categoryLevel_attribute = AddAttributeWithParams(4, "categoryLevel", "string", 10, false, false, false, false, false, false, "\"e\"");
            Sybase.Reflection.AttributeMetaData surrogateKey_attribute = AddAttributeWithParams(5, "surrogateKey", "long", -1, false, true, false, false, false, false, "\"f\"");
            InitAttributeMapFromAttributes();
            AddOperationWithParams(280, "findAll", "CategoryExtract*", true);
            Sybase.Reflection.OperationMetaData FindByPrimaryKey1_operation = AddOperationWithParams(282, "findByPrimaryKey", "CategoryExtract", true);
            AddParameterForOperation(FindByPrimaryKey1_operation, "categoryId", "string");
            Sybase.Reflection.OperationMetaData GetCategoriesByParent2_operation = AddOperationWithParams(285, "getCategoriesByParent", "Sybase.Persistence.QueryResultSet", true);
            AddParameterForOperation(GetCategoriesByParent2_operation, "parent_id", "string");
            Sybase.Reflection.OperationMetaData GetCategoriesByLevel3_operation = AddOperationWithParams(288, "getCategoriesByLevel", "Sybase.Persistence.QueryResultSet", true);
            AddParameterForOperation(GetCategoriesByLevel3_operation, "categoryLevel", "string");
            AddOperationWithParams(4038, "isNew", "boolean", false);
            AddOperationWithParams(4039, "isDirty", "boolean", false);
            AddOperationWithParams(4040, "isDeleted", "boolean", false);
            AddOperationWithParams(4041, "refresh", "void", false);
            AddOperationWithParams(4042, "_pk", "long?", false);
            Sybase.Reflection.OperationMetaData FindWithQuery9_operation = AddOperationWithParams(4043, "findWithQuery", "CategoryExtract*", true);
            AddParameterForOperation(FindWithQuery9_operation, "query", "Sybase.Persistence.Query");
            Sybase.Reflection.OperationMetaData GetSize10_operation = AddOperationWithParams(4045, "getSize", "int", true);
            AddParameterForOperation(GetSize10_operation, "query", "Sybase.Persistence.Query");
            InitOperationMapFromOperations();
            SetName("CategoryExtract");
            SetTable("\"graybarmobility_1_0_categoryextract\"");
            SetSynchronizationGroup("ReferenceSyncGroup");
        }
    }
}