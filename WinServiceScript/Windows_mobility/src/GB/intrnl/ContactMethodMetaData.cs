
namespace GB.intrnl
{
    public class ContactMethodMetaData
        : Sybase.Reflection.EntityMetaData
    {
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public ContactMethodMetaData()
        {
            _init();
        }
        
        protected void _init()
        {
            SetId(3);
            SetAttributes(new Sybase.Reflection.AttributeMetaDataList());
            SetOperations(new Sybase.Reflection.OperationMetaDataList());
            SetAttributeMap(new Sybase.Reflection.AttributeMap());
            SetOperationMap(new Sybase.Reflection.OperationMap());
            Sybase.Reflection.AttributeMetaData user_id_attribute = AddAttributeWithParams(0, "user_id", "string", 100, false, false, false, false, false, false, "\"a\"");
            Sybase.Reflection.AttributeMetaData email_attribute = AddAttributeWithParams(1, "email", "string?", 50, false, false, false, false, false, false, "\"b\"");
            Sybase.Reflection.AttributeMetaData text_phone_number_attribute = AddAttributeWithParams(2, "text_phone_number", "string?", 20, false, false, false, false, false, false, "\"c\"");
            Sybase.Reflection.AttributeMetaData voice_phone_number_attribute = AddAttributeWithParams(3, "voice_phone_number", "string?", 20, false, false, false, false, false, false, "\"d\"");
            Sybase.Reflection.AttributeMetaData pending_attribute = AddAttributeWithParams(20001, "pending", "boolean", -1, false, false, true, false, false, false, "\"pending\"");
            Sybase.Reflection.AttributeMetaData pendingChange_attribute = AddAttributeWithParams(20002, "pendingChange", "char", 1, false, false, true, false, false, false, "\"_pc\"");
            Sybase.Reflection.AttributeMetaData replayPending_attribute = AddAttributeWithParams(20005, "replayPending", "long", -1, false, false, true, false, false, false, "\"_rp\"");
            Sybase.Reflection.AttributeMetaData replayFailure_attribute = AddAttributeWithParams(20006, "replayFailure", "long", -1, false, false, true, false, false, false, "\"_rf\"");
            Sybase.Reflection.AttributeMetaData cvpOperation_attribute = AddAttributeWithParams(4, "cvpOperation", "bigString?", -1, false, false, true, false, false, false, "\"cvp_operation_header\"");
            Sybase.Reflection.AttributeMetaData cvpOperationLobs_attribute = AddAttributeWithParams(5, "cvpOperationLobs", "bigString?", -1, false, false, true, false, false, false, "\"cvp_operation_lobs\"");
            Sybase.Reflection.AttributeMetaData surrogateKey_attribute = AddAttributeWithParams(6, "surrogateKey", "long", -1, false, true, false, false, false, false, "\"e\"");
            Sybase.Reflection.AttributeMetaData replayCounter_attribute = AddAttributeWithParams(20004, "replayCounter", "long", -1, false, false, true, false, false, false, "\"_rc\"");
            Sybase.Reflection.AttributeMetaData disableSubmit_attribute = AddAttributeWithParams(20003, "disableSubmit", "boolean", -1, false, false, true, false, false, false, "\"_ds\"");
            Sybase.Reflection.AttributeMetaData cvpOperationLength_attribute = AddAttributeWithParams(7, "cvpOperationLength", "long", -1, false, false, true, false, false, false, "\"cvpOperation_length\"");
            Sybase.Reflection.AttributeMetaData cvpOperationLobsLength_attribute = AddAttributeWithParams(8, "cvpOperationLobsLength", "long", -1, false, false, true, false, false, false, "\"cvpOperationLobs_length\"");
            InitAttributeMapFromAttributes();
            AddOperationWithParams(314, "findAll", "ContactMethod*", true);
            Sybase.Reflection.OperationMetaData FindByPrimaryKey1_operation = AddOperationWithParams(316, "findByPrimaryKey", "ContactMethod", true);
            AddParameterForOperation(FindByPrimaryKey1_operation, "user_id", "string");
            AddOperationWithParams(319, "getContactMethod", "Sybase.Persistence.QueryResultSet", true);
            AddOperationWithParams(322, "create", "void", false, 'C');
            AddOperationWithParams(333, "update", "void", false, 'U');
            AddOperationWithParams(342, "delete", "void", false, 'D');
            AddOperationWithParams(2511, "getPendingObjects", "ContactMethod*", true);
            Sybase.Reflection.OperationMetaData GetPendingObjects7_operation = AddOperationWithParams(2513, "getPendingObjects", "ContactMethod*", true);
            AddParameterForOperation(GetPendingObjects7_operation, "pendingChange", "char");
            AddOperationWithParams(2516, "getReplayPendingObjects", "ContactMethod*", true);
            AddOperationWithParams(4050, "isNew", "boolean", false);
            AddOperationWithParams(4051, "isDirty", "boolean", false);
            AddOperationWithParams(4052, "isDeleted", "boolean", false);
            AddOperationWithParams(4053, "refresh", "void", false);
            AddOperationWithParams(4054, "_pk", "long?", false);
            AddOperationWithParams(4055, "isPending", "boolean", false);
            AddOperationWithParams(4056, "isCreated", "boolean", false);
            AddOperationWithParams(4057, "isUpdated", "boolean", false);
            AddOperationWithParams(4058, "submitPending", "void", false);
            AddOperationWithParams(4059, "cancelPending", "void", false);
            AddOperationWithParams(4060, "submitPendingOperations", "void", true);
            AddOperationWithParams(4061, "cancelPendingOperations", "void", true);
            AddOperationWithParams(4062, "save", "void", false);
            Sybase.Reflection.OperationMetaData FindWithQuery22_operation = AddOperationWithParams(4063, "findWithQuery", "ContactMethod*", true);
            AddParameterForOperation(FindWithQuery22_operation, "query", "Sybase.Persistence.Query");
            Sybase.Reflection.OperationMetaData GetSize23_operation = AddOperationWithParams(4065, "getSize", "int", true);
            AddParameterForOperation(GetSize23_operation, "query", "Sybase.Persistence.Query");
            InitOperationMapFromOperations();
            SetName("ContactMethod");
            SetTable("\"graybarmobility_1_0_contactmethod\"");
            SetSynchronizationGroup("ReferenceSyncGroup");
        }
    }
}