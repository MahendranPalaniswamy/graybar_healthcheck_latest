
namespace GB.intrnl
{
    public class SISSubscriptionMetaData
        : Sybase.Reflection.EntityMetaData
    {
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public SISSubscriptionMetaData()
        {
            _init();
        }
        
        protected void _init()
        {
            SetId(29);
            SetAttributes(new Sybase.Reflection.AttributeMetaDataList());
            SetOperations(new Sybase.Reflection.OperationMetaDataList());
            SetAttributeMap(new Sybase.Reflection.AttributeMap());
            SetOperationMap(new Sybase.Reflection.OperationMap());
            Sybase.Reflection.AttributeMetaData deviceId_attribute = AddAttributeWithParams(0, "deviceId", "string", 200, false, false, false, false, false, false, "\"device_id\"");
            Sybase.Reflection.AttributeMetaData username_attribute = AddAttributeWithParams(1, "username", "string", 100, false, false, false, false, false, false, "\"user_name\"");
            Sybase.Reflection.AttributeMetaData appname_attribute = AddAttributeWithParams(2, "appname", "string", 100, false, false, false, false, false, false, "\"h\"");
            Sybase.Reflection.AttributeMetaData enable_attribute = AddAttributeWithParams(3, "enable", "boolean", -1, false, false, false, false, false, false, "\"i\"");
            Sybase.Reflection.AttributeMetaData adminLock_attribute = AddAttributeWithParams(4, "adminLock", "boolean", -1, false, false, false, false, false, false, "\"admin_lock\"");
            Sybase.Reflection.AttributeMetaData interval_attribute = AddAttributeWithParams(5, "interval", "int", -1, false, false, false, false, false, false, "\"l\"");
            Sybase.Reflection.AttributeMetaData protocol_attribute = AddAttributeWithParams(6, "protocol", "string", 100, false, false, false, false, false, false, "\"m\"");
            Sybase.Reflection.AttributeMetaData address_attribute = AddAttributeWithParams(7, "address", "string?", 300, false, false, false, false, false, false, "\"n\"");
            Sybase.Reflection.AttributeMetaData domain_attribute = AddAttributeWithParams(8, "domain", "string", 100, false, true, false, false, false, false, "\"domain\"");
            Sybase.Reflection.AttributeMetaData package_attribute = AddAttributeWithParams(9, "package", "string", 100, false, true, false, false, false, false, "\"pkg\"");
            Sybase.Reflection.AttributeMetaData syncGroup_attribute = AddAttributeWithParams(10, "syncGroup", "string", 100, false, true, false, false, false, false, "\"sync_group\"");
            Sybase.Reflection.AttributeMetaData clientId_attribute = AddAttributeWithParams(11, "clientId", "string", 100, false, true, false, false, false, false, "\"cid\"");
            InitAttributeMapFromAttributes();
            AddOperationWithParams(1621, "findAll", "SISSubscription*", true);
            AddOperationWithParams(4430, "isNew", "boolean", false);
            AddOperationWithParams(4431, "isDirty", "boolean", false);
            AddOperationWithParams(4432, "isDeleted", "boolean", false);
            AddOperationWithParams(4433, "refresh", "void", false);
            AddOperationWithParams(4434, "save", "void", false);
            InitOperationMapFromOperations();
            SetName("SISSubscription");
            SetTable("\"sup_sis_subscription\"");
            SetSynchronizationGroup("system");
        }
    }
}