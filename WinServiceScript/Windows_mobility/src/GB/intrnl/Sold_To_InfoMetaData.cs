
namespace GB.intrnl
{
    public class Sold_To_InfoMetaData
        : Sybase.Reflection.EntityMetaData
    {
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public Sold_To_InfoMetaData()
        {
            _init();
        }
        
        protected void _init()
        {
            SetId(34);
            SetAttributes(new Sybase.Reflection.AttributeMetaDataList());
            SetOperations(new Sybase.Reflection.OperationMetaDataList());
            SetAttributeMap(new Sybase.Reflection.AttributeMap());
            SetOperationMap(new Sybase.Reflection.OperationMap());
            Sybase.Reflection.AttributeMetaData SOLD_TO_attribute = AddAttributeWithParams(0, "SOLD_TO", "string", 10, false, false, false, false, false, false, "\"a\"");
            Sybase.Reflection.AttributeMetaData DEFAULT_PICKUP_PLANT_attribute = AddAttributeWithParams(1, "DEFAULT_PICKUP_PLANT", "string", 4, false, false, false, false, false, false, "\"b\"");
            Sybase.Reflection.AttributeMetaData PHONE_attribute = AddAttributeWithParams(2, "PHONE", "string", 30, false, false, false, false, false, false, "\"c\"");
            Sybase.Reflection.AttributeMetaData IS_ON_ACCOUNT_attribute = AddAttributeWithParams(3, "IS_ON_ACCOUNT", "string", 1, false, false, false, false, false, false, "\"d\"");
            Sybase.Reflection.AttributeMetaData CREDIT_BLOCKED_attribute = AddAttributeWithParams(4, "CREDIT_BLOCKED", "string", 1, false, false, false, false, false, false, "\"e\"");
            Sybase.Reflection.AttributeMetaData OVERALL_BLOCKED_attribute = AddAttributeWithParams(5, "OVERALL_BLOCKED", "string", 1, false, false, false, false, false, false, "\"f\"");
            Sybase.Reflection.AttributeMetaData surrogateKey_attribute = AddAttributeWithParams(6, "surrogateKey", "long", -1, false, true, false, false, false, false, "\"g\"");
            InitAttributeMapFromAttributes();
            AddOperationWithParams(1228, "findAll", "Sold_To_Info*", true);
            Sybase.Reflection.OperationMetaData FindByPrimaryKey1_operation = AddOperationWithParams(1230, "findByPrimaryKey", "Sold_To_Info", true);
            AddParameterForOperation(FindByPrimaryKey1_operation, "SOLD_TO", "string");
            AddOperationWithParams(1233, "getSoldToInfo", "Sybase.Persistence.QueryResultSet", true);
            AddOperationWithParams(4291, "isNew", "boolean", false);
            AddOperationWithParams(4292, "isDirty", "boolean", false);
            AddOperationWithParams(4293, "isDeleted", "boolean", false);
            AddOperationWithParams(4294, "refresh", "void", false);
            AddOperationWithParams(4295, "_pk", "long?", false);
            Sybase.Reflection.OperationMetaData FindWithQuery8_operation = AddOperationWithParams(4296, "findWithQuery", "Sold_To_Info*", true);
            AddParameterForOperation(FindWithQuery8_operation, "query", "Sybase.Persistence.Query");
            Sybase.Reflection.OperationMetaData GetSize9_operation = AddOperationWithParams(4298, "getSize", "int", true);
            AddParameterForOperation(GetSize9_operation, "query", "Sybase.Persistence.Query");
            InitOperationMapFromOperations();
            SetName("Sold_To_Info");
            SetTable("\"graybarmobility_1_0_sold_to_info\"");
            SetSynchronizationGroup("SoldToInfoSyncGroup");
        }
    }
}