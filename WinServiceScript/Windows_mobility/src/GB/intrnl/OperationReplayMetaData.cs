
namespace GB.intrnl
{
    public class OperationReplayMetaData
        : Sybase.Reflection.EntityMetaData
    {
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public OperationReplayMetaData()
        {
            _init();
        }
        
        protected void _init()
        {
            SetId(18);
            SetAttributes(new Sybase.Reflection.AttributeMetaDataList());
            SetOperations(new Sybase.Reflection.OperationMetaDataList());
            SetAttributeMap(new Sybase.Reflection.AttributeMap());
            SetOperationMap(new Sybase.Reflection.OperationMap());
            Sybase.Reflection.AttributeMetaData remoteId_attribute = AddAttributeWithParams(0, "remoteId", "string", 100, false, false, false, false, false, false, "\"a\"");
            Sybase.Reflection.AttributeMetaData component_attribute = AddAttributeWithParams(1, "component", "string", 200, false, false, false, false, false, false, "\"c\"");
            Sybase.Reflection.AttributeMetaData entityKey_attribute = AddAttributeWithParams(2, "entityKey", "string", 256, false, false, false, false, false, false, "\"d\"");
            Sybase.Reflection.AttributeMetaData attributes_attribute = AddAttributeWithParams(3, "attributes", "string", -1, false, false, false, false, false, false, "\"e\"");
            Sybase.Reflection.AttributeMetaData operation_attribute = AddAttributeWithParams(4, "operation", "string", 100, false, false, false, false, false, false, "\"f\"");
            Sybase.Reflection.AttributeMetaData parameters_attribute = AddAttributeWithParams(5, "parameters", "string", -1, false, false, false, false, false, false, "\"g\"");
            Sybase.Reflection.AttributeMetaData replayLog_attribute = AddAttributeWithParams(6, "replayLog", "string?", -1, false, false, false, false, false, false, "\"h\"");
            Sybase.Reflection.AttributeMetaData exception_attribute = AddAttributeWithParams(7, "exception", "string?", -1, false, false, false, false, false, false, "\"i\"");
            Sybase.Reflection.AttributeMetaData completed_attribute = AddAttributeWithParams(8, "completed", "boolean", -1, false, false, false, false, false, false, "\"j\"");
            Sybase.Reflection.AttributeMetaData requestId_attribute = AddAttributeWithParams(9, "requestId", "long", -1, false, true, false, false, false, false, "\"b\"");
            InitAttributeMapFromAttributes();
            AddOperationWithParams(1565, "saveErrorInfo", "void", false);
            AddOperationWithParams(1573, "findReadyToFinish", "OperationReplay*", true);
            AddOperationWithParams(4423, "isNew", "boolean", false);
            AddOperationWithParams(4424, "isDirty", "boolean", false);
            AddOperationWithParams(4425, "isDeleted", "boolean", false);
            AddOperationWithParams(4426, "refresh", "void", false);
            AddOperationWithParams(4427, "_pk", "long?", false);
            AddOperationWithParams(4428, "save", "void", false);
            InitOperationMapFromOperations();
            SetName("OperationReplay");
            SetTable("\"graybarmobility_1_0_operationreplay\"");
            SetSynchronizationGroup("system");
        }
    }
}