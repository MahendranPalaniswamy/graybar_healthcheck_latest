
namespace GB.intrnl
{
    public class Order_Line_ItemsMetaData
        : Sybase.Reflection.EntityMetaData
    {
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public Order_Line_ItemsMetaData()
        {
            _init();
        }
        
        protected void _init()
        {
            SetId(23);
            SetAttributes(new Sybase.Reflection.AttributeMetaDataList());
            SetOperations(new Sybase.Reflection.OperationMetaDataList());
            SetAttributeMap(new Sybase.Reflection.AttributeMap());
            SetOperationMap(new Sybase.Reflection.OperationMap());
            Sybase.Reflection.AttributeMetaData SOLD_TO_attribute = AddAttributeWithParams(0, "SOLD_TO", "string", 10, false, false, false, false, false, false, "\"a\"");
            Sybase.Reflection.AttributeMetaData ORDER_NUMBER_attribute = AddAttributeWithParams(1, "ORDER_NUMBER", "string", 10, false, false, false, false, false, false, "\"b\"");
            Sybase.Reflection.AttributeMetaData MOBILE_ORDER_NUMBER_attribute = AddAttributeWithParams(2, "MOBILE_ORDER_NUMBER", "string", 20, false, false, false, false, false, false, "\"c\"");
            Sybase.Reflection.AttributeMetaData LINE_ITEM_ID_attribute = AddAttributeWithParams(3, "LINE_ITEM_ID", "string", 6, false, false, false, false, false, false, "\"d\"");
            Sybase.Reflection.AttributeMetaData PRODUCT_ID_attribute = AddAttributeWithParams(4, "PRODUCT_ID", "string", 18, false, false, false, false, false, false, "\"e\"");
            Sybase.Reflection.AttributeMetaData CATALOG_NUMBER_attribute = AddAttributeWithParams(5, "CATALOG_NUMBER", "string", 40, false, false, false, false, false, false, "\"f\"");
            Sybase.Reflection.AttributeMetaData MANUFACTURER_NAME_attribute = AddAttributeWithParams(6, "MANUFACTURER_NAME", "string", 35, false, false, false, false, false, false, "\"g\"");
            Sybase.Reflection.AttributeMetaData PRODUCT_STATUS_attribute = AddAttributeWithParams(7, "PRODUCT_STATUS", "string", 1, false, false, false, false, false, false, "\"h\"");
            Sybase.Reflection.AttributeMetaData IS_PICKUP_attribute = AddAttributeWithParams(8, "IS_PICKUP", "string", 1, false, false, false, false, false, false, "\"i\"");
            Sybase.Reflection.AttributeMetaData QUANTITY_attribute = AddAttributeWithParams(9, "QUANTITY", "decimal", -1, false, false, false, false, false, false, "\"j\"");
            Sybase.Reflection.AttributeMetaData PRODUCT_TOTAL_PRICE_attribute = AddAttributeWithParams(10, "PRODUCT_TOTAL_PRICE", "decimal", -1, false, false, false, false, false, false, "\"l\"");
            Sybase.Reflection.AttributeMetaData UNIT_attribute = AddAttributeWithParams(11, "UNIT", "string", 3, false, false, false, false, false, false, "\"m\"");
            Sybase.Reflection.AttributeMetaData PRICE_PER_UNIT_attribute = AddAttributeWithParams(12, "PRICE_PER_UNIT", "decimal", -1, false, false, false, false, false, false, "\"n\"");
            Sybase.Reflection.AttributeMetaData PER_VALUE_attribute = AddAttributeWithParams(13, "PER_VALUE", "decimal", -1, false, false, false, false, false, false, "\"o\"");
            Sybase.Reflection.AttributeMetaData DESCRIPTION_attribute = AddAttributeWithParams(14, "DESCRIPTION", "string", 1024, false, false, false, false, false, false, "\"p\"");
            Sybase.Reflection.AttributeMetaData IMAGE_URL_attribute = AddAttributeWithParams(15, "IMAGE_URL", "string", 1024, false, false, false, false, false, false, "\"q\"");
            Sybase.Reflection.AttributeMetaData PRODUCT_STATUS_TEXT_attribute = AddAttributeWithParams(16, "PRODUCT_STATUS_TEXT", "string", 25, false, false, false, false, false, false, "\"r\"");
            Sybase.Reflection.AttributeMetaData orderHeader_attribute = AddAttributeWithParams(17, "orderHeader", "GB.Order_Header", -1, false, false, false, true, false, false);
            Sybase.Reflection.AttributeMetaData pending_attribute = AddAttributeWithParams(20001, "pending", "boolean", -1, false, false, true, false, false, false, "\"pending\"");
            Sybase.Reflection.AttributeMetaData pendingChange_attribute = AddAttributeWithParams(20002, "pendingChange", "char", 1, false, false, true, false, false, false, "\"_pc\"");
            Sybase.Reflection.AttributeMetaData replayPending_attribute = AddAttributeWithParams(20005, "replayPending", "long", -1, false, false, true, false, false, false, "\"_rp\"");
            Sybase.Reflection.AttributeMetaData replayFailure_attribute = AddAttributeWithParams(20006, "replayFailure", "long", -1, false, false, true, false, false, false, "\"_rf\"");
            Sybase.Reflection.AttributeMetaData cvpOperation_attribute = AddAttributeWithParams(18, "cvpOperation", "bigString?", -1, false, false, true, false, false, false, "\"cvp_operation_header\"");
            Sybase.Reflection.AttributeMetaData cvpOperationLobs_attribute = AddAttributeWithParams(19, "cvpOperationLobs", "bigString?", -1, false, false, true, false, false, false, "\"cvp_operation_lobs\"");
            Sybase.Reflection.AttributeMetaData orderHeaderFK_attribute = AddAttributeWithParams(20, "orderHeaderFK", "long?", -1, false, false, true, false, false, false, "\"orderHeaderFK\"");
            Sybase.Reflection.AttributeMetaData surrogateKey_attribute = AddAttributeWithParams(21, "surrogateKey", "long", -1, false, true, false, false, false, false, "\"s\"");
            Sybase.Reflection.AttributeMetaData replayCounter_attribute = AddAttributeWithParams(20004, "replayCounter", "long", -1, false, false, true, false, false, false, "\"_rc\"");
            Sybase.Reflection.AttributeMetaData disableSubmit_attribute = AddAttributeWithParams(20003, "disableSubmit", "boolean", -1, false, false, true, false, false, false, "\"_ds\"");
            Sybase.Reflection.AttributeMetaData cvpOperationLength_attribute = AddAttributeWithParams(22, "cvpOperationLength", "long", -1, false, false, true, false, false, false, "\"cvpOperation_length\"");
            Sybase.Reflection.AttributeMetaData cvpOperationLobsLength_attribute = AddAttributeWithParams(23, "cvpOperationLobsLength", "long", -1, false, false, true, false, false, false, "\"cvpOperationLobs_length\"");
            InitAttributeMapFromAttributes();
            AddOperationWithParams(1359, "findAll", "Order_Line_Items*", true);
            Sybase.Reflection.OperationMetaData FindByORDER_NUMBER1_operation = AddOperationWithParams(1361, "findByORDER_NUMBER", "Order_Line_Items*", true);
            AddParameterForOperation(FindByORDER_NUMBER1_operation, "ORDER_NUMBER", "string");
            Sybase.Reflection.OperationMetaData FindByMOBILE_ORDER_NUMBER2_operation = AddOperationWithParams(1364, "findByMOBILE_ORDER_NUMBER", "Order_Line_Items*", true);
            AddParameterForOperation(FindByMOBILE_ORDER_NUMBER2_operation, "MOBILE_ORDER_NUMBER", "string");
            Sybase.Reflection.OperationMetaData FindByLINE_ITEM_ID3_operation = AddOperationWithParams(1367, "findByLINE_ITEM_ID", "Order_Line_Items*", true);
            AddParameterForOperation(FindByLINE_ITEM_ID3_operation, "LINE_ITEM_ID", "string");
            Sybase.Reflection.OperationMetaData FindByPrimaryKey4_operation = AddOperationWithParams(1370, "findByPrimaryKey", "Order_Line_Items", true);
            AddParameterForOperation(FindByPrimaryKey4_operation, "ORDER_NUMBER", "string");
            AddParameterForOperation(FindByPrimaryKey4_operation, "MOBILE_ORDER_NUMBER", "string");
            AddParameterForOperation(FindByPrimaryKey4_operation, "LINE_ITEM_ID", "string");
            AddOperationWithParams(1375, "getLineItems", "Sybase.Persistence.QueryResultSet", true);
            AddOperationWithParams(1378, "delete", "void", false, 'D');
            Sybase.Reflection.OperationMetaData GetOrderLineItems_for_Order_Header7_operation = AddOperationWithParams(2816, "getOrderLineItems_for_Order_Header", "Order_Line_Items*", true);
            AddParameterForOperation(GetOrderLineItems_for_Order_Header7_operation, "surrogateKey", "long?");
            Sybase.Reflection.OperationMetaData DeleteOSOrderLineItems_for_Order_Header8_operation = AddOperationWithParams(2819, "deleteOSOrderLineItems_for_Order_Header", "void", true);
            AddParameterForOperation(DeleteOSOrderLineItems_for_Order_Header8_operation, "surrogateKey", "long?");
            Sybase.Reflection.OperationMetaData DeleteOrderLineItems_for_Order_Header9_operation = AddOperationWithParams(2821, "deleteOrderLineItems_for_Order_Header", "void", true);
            AddParameterForOperation(DeleteOrderLineItems_for_Order_Header9_operation, "surrogateKey", "long?");
            AddOperationWithParams(3126, "getPendingObjects", "Order_Line_Items*", true);
            Sybase.Reflection.OperationMetaData GetPendingObjects11_operation = AddOperationWithParams(3128, "getPendingObjects", "Order_Line_Items*", true);
            AddParameterForOperation(GetPendingObjects11_operation, "pendingChange", "char");
            AddOperationWithParams(3131, "getReplayPendingObjects", "Order_Line_Items*", true);
            AddOperationWithParams(4335, "isNew", "boolean", false);
            AddOperationWithParams(4336, "isDirty", "boolean", false);
            AddOperationWithParams(4337, "isDeleted", "boolean", false);
            AddOperationWithParams(4338, "refresh", "void", false);
            AddOperationWithParams(4339, "_pk", "long?", false);
            AddOperationWithParams(4340, "isPending", "boolean", false);
            AddOperationWithParams(4341, "isCreated", "boolean", false);
            AddOperationWithParams(4342, "isUpdated", "boolean", false);
            AddOperationWithParams(4343, "submitPending", "void", false);
            AddOperationWithParams(4344, "cancelPending", "void", false);
            AddOperationWithParams(4345, "submitPendingOperations", "void", true);
            AddOperationWithParams(4346, "cancelPendingOperations", "void", true);
            AddOperationWithParams(4347, "save", "void", false);
            Sybase.Reflection.OperationMetaData FindWithQuery26_operation = AddOperationWithParams(4348, "findWithQuery", "Order_Line_Items*", true);
            AddParameterForOperation(FindWithQuery26_operation, "query", "Sybase.Persistence.Query");
            Sybase.Reflection.OperationMetaData GetSize27_operation = AddOperationWithParams(4350, "getSize", "int", true);
            AddParameterForOperation(GetSize27_operation, "query", "Sybase.Persistence.Query");
            InitOperationMapFromOperations();
            SetName("Order_Line_Items");
            SetTable("\"graybarmobility_1_0_order_line_items\"");
            SetSynchronizationGroup("OrderRelatedSyncGroup");
        }
    }
}