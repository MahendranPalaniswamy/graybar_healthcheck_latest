
namespace GB.intrnl
{
    public class Order_HeaderMetaData
        : Sybase.Reflection.EntityMetaData
    {
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public Order_HeaderMetaData()
        {
            _init();
        }
        
        protected void _init()
        {
            SetId(21);
            SetAttributes(new Sybase.Reflection.AttributeMetaDataList());
            SetOperations(new Sybase.Reflection.OperationMetaDataList());
            SetAttributeMap(new Sybase.Reflection.AttributeMap());
            SetOperationMap(new Sybase.Reflection.OperationMap());
            Sybase.Reflection.AttributeMetaData SOLD_TO_attribute = AddAttributeWithParams(0, "SOLD_TO", "string?", 10, false, false, false, false, false, false, "\"a\"");
            Sybase.Reflection.AttributeMetaData ORDER_NUMBER_attribute = AddAttributeWithParams(1, "ORDER_NUMBER", "string", 10, false, false, false, false, false, false, "\"b\"");
            Sybase.Reflection.AttributeMetaData MOBILE_ORDER_NUMBER_attribute = AddAttributeWithParams(2, "MOBILE_ORDER_NUMBER", "string", 20, false, false, false, false, false, false, "\"c\"");
            Sybase.Reflection.AttributeMetaData MOBILE_ORDER_attribute = AddAttributeWithParams(3, "MOBILE_ORDER", "string?", 1, false, false, false, false, false, false, "\"d\"");
            Sybase.Reflection.AttributeMetaData PURCHASE_ORDER_attribute = AddAttributeWithParams(4, "PURCHASE_ORDER", "string?", 35, false, false, false, false, false, false, "\"e\"");
            Sybase.Reflection.AttributeMetaData SUBMITTED_BY_attribute = AddAttributeWithParams(5, "SUBMITTED_BY", "string?", 30, false, false, false, false, false, false, "\"f\"");
            Sybase.Reflection.AttributeMetaData RELEASE_REFERENCE_attribute = AddAttributeWithParams(6, "RELEASE_REFERENCE", "string?", 30, false, false, false, false, false, false, "\"g\"");
            Sybase.Reflection.AttributeMetaData SHIP_TO_attribute = AddAttributeWithParams(7, "SHIP_TO", "string?", 10, false, false, false, false, false, false, "\"h\"");
            Sybase.Reflection.AttributeMetaData ORDER_TOTAL_attribute = AddAttributeWithParams(8, "ORDER_TOTAL", "decimal?", -1, false, false, false, false, false, false, "\"i\"");
            Sybase.Reflection.AttributeMetaData ORDER_STATUS_attribute = AddAttributeWithParams(9, "ORDER_STATUS", "string?", 1, false, false, false, false, false, false, "\"j\"");
            Sybase.Reflection.AttributeMetaData ORDER_DATE_attribute = AddAttributeWithParams(10, "ORDER_DATE", "date?", -1, false, false, false, false, false, false, "\"l\"");
            Sybase.Reflection.AttributeMetaData ORDER_STATUS_TEXT_attribute = AddAttributeWithParams(11, "ORDER_STATUS_TEXT", "string", 25, false, false, false, false, false, false, "\"m\"");
            Sybase.Reflection.AttributeMetaData orderLineItems_attribute = AddAttributeWithParams(12, "orderLineItems", "GB.Order_Line_Items*", -1, false, false, false, true, false, false , true);
            Sybase.Reflection.AttributeMetaData orderDeliveryHeaders_attribute = AddAttributeWithParams(13, "orderDeliveryHeaders", "GB.Order_Delivery_Header*", -1, false, false, false, true, false, false , true);
            Sybase.Reflection.AttributeMetaData pending_attribute = AddAttributeWithParams(20001, "pending", "boolean", -1, false, false, true, false, false, false, "\"pending\"");
            Sybase.Reflection.AttributeMetaData pendingChange_attribute = AddAttributeWithParams(20002, "pendingChange", "char", 1, false, false, true, false, false, false, "\"_pc\"");
            Sybase.Reflection.AttributeMetaData replayPending_attribute = AddAttributeWithParams(20005, "replayPending", "long", -1, false, false, true, false, false, false, "\"_rp\"");
            Sybase.Reflection.AttributeMetaData replayFailure_attribute = AddAttributeWithParams(20006, "replayFailure", "long", -1, false, false, true, false, false, false, "\"_rf\"");
            Sybase.Reflection.AttributeMetaData cvpOperation_attribute = AddAttributeWithParams(14, "cvpOperation", "bigString?", -1, false, false, true, false, false, false, "\"cvp_operation_header\"");
            Sybase.Reflection.AttributeMetaData cvpOperationLobs_attribute = AddAttributeWithParams(15, "cvpOperationLobs", "bigString?", -1, false, false, true, false, false, false, "\"cvp_operation_lobs\"");
            Sybase.Reflection.AttributeMetaData surrogateKey_attribute = AddAttributeWithParams(16, "surrogateKey", "long", -1, false, true, false, false, false, false, "\"n\"");
            Sybase.Reflection.AttributeMetaData replayCounter_attribute = AddAttributeWithParams(20004, "replayCounter", "long", -1, false, false, true, false, false, false, "\"_rc\"");
            Sybase.Reflection.AttributeMetaData disableSubmit_attribute = AddAttributeWithParams(20003, "disableSubmit", "boolean", -1, false, false, true, false, false, false, "\"_ds\"");
            Sybase.Reflection.AttributeMetaData cvpOperationLength_attribute = AddAttributeWithParams(17, "cvpOperationLength", "long", -1, false, false, true, false, false, false, "\"cvpOperation_length\"");
            Sybase.Reflection.AttributeMetaData cvpOperationLobsLength_attribute = AddAttributeWithParams(18, "cvpOperationLobsLength", "long", -1, false, false, true, false, false, false, "\"cvpOperationLobs_length\"");
            InitAttributeMapFromAttributes();
            AddOperationWithParams(775, "findAll", "Order_Header*", true);
            Sybase.Reflection.OperationMetaData FindByORDER_NUMBER1_operation = AddOperationWithParams(777, "findByORDER_NUMBER", "Order_Header*", true);
            AddParameterForOperation(FindByORDER_NUMBER1_operation, "ORDER_NUMBER", "string");
            Sybase.Reflection.OperationMetaData FindByMOBILE_ORDER_NUMBER2_operation = AddOperationWithParams(780, "findByMOBILE_ORDER_NUMBER", "Order_Header*", true);
            AddParameterForOperation(FindByMOBILE_ORDER_NUMBER2_operation, "MOBILE_ORDER_NUMBER", "string");
            Sybase.Reflection.OperationMetaData FindByPrimaryKey3_operation = AddOperationWithParams(783, "findByPrimaryKey", "Order_Header", true);
            AddParameterForOperation(FindByPrimaryKey3_operation, "ORDER_NUMBER", "string");
            AddParameterForOperation(FindByPrimaryKey3_operation, "MOBILE_ORDER_NUMBER", "string");
            AddOperationWithParams(787, "getAllOrderHeaders", "Sybase.Persistence.QueryResultSet", true);
            Sybase.Reflection.OperationMetaData FilterByMobileOrders5_operation = AddOperationWithParams(789, "filterByMobileOrders", "Sybase.Persistence.QueryResultSet", true);
            AddParameterForOperation(FilterByMobileOrders5_operation, "is_mobile_order", "string");
            Sybase.Reflection.OperationMetaData FilterByStatus6_operation = AddOperationWithParams(792, "filterByStatus", "Sybase.Persistence.QueryResultSet", true);
            AddParameterForOperation(FilterByStatus6_operation, "status", "string");
            Sybase.Reflection.OperationMetaData FilterByDate7_operation = AddOperationWithParams(795, "filterByDate", "Sybase.Persistence.QueryResultSet", true);
            AddParameterForOperation(FilterByDate7_operation, "date", "date?");
            Sybase.Reflection.OperationMetaData FilterByPO8_operation = AddOperationWithParams(798, "filterByPO", "Sybase.Persistence.QueryResultSet", true);
            AddParameterForOperation(FilterByPO8_operation, "PO", "string");
            Sybase.Reflection.OperationMetaData FilterByRELREF9_operation = AddOperationWithParams(801, "filterByRELREF", "Sybase.Persistence.QueryResultSet", true);
            AddParameterForOperation(FilterByRELREF9_operation, "rel_ref", "string");
            Sybase.Reflection.OperationMetaData FilterBy10_operation = AddOperationWithParams(804, "filterBy", "Sybase.Persistence.QueryResultSet", true);
            AddParameterForOperation(FilterBy10_operation, "MOBILE_ORDER", "string?");
            AddParameterForOperation(FilterBy10_operation, "ORDER_STATUS", "string?");
            AddParameterForOperation(FilterBy10_operation, "ORDER_DATE", "date?");
            AddParameterForOperation(FilterBy10_operation, "PURCHASE_ORDER", "string?");
            AddParameterForOperation(FilterBy10_operation, "RELEASE_REFERENCE", "string?");
            AddOperationWithParams(813, "CreateDataWithDCN", "void", false, 'C');
            AddOperationWithParams(839, "delete", "void", false, 'D');
            AddOperationWithParams(843, "create", "void", false, 'C');
            AddOperationWithParams(2787, "getPendingObjects", "Order_Header*", true);
            Sybase.Reflection.OperationMetaData GetPendingObjects15_operation = AddOperationWithParams(2789, "getPendingObjects", "Order_Header*", true);
            AddParameterForOperation(GetPendingObjects15_operation, "pendingChange", "char");
            AddOperationWithParams(2792, "getReplayPendingObjects", "Order_Header*", true);
            AddOperationWithParams(4179, "isNew", "boolean", false);
            AddOperationWithParams(4180, "isDirty", "boolean", false);
            AddOperationWithParams(4181, "isDeleted", "boolean", false);
            AddOperationWithParams(4182, "refresh", "void", false);
            AddOperationWithParams(4183, "_pk", "long?", false);
            AddOperationWithParams(4184, "isPending", "boolean", false);
            AddOperationWithParams(4185, "isCreated", "boolean", false);
            AddOperationWithParams(4186, "isUpdated", "boolean", false);
            AddOperationWithParams(4187, "submitPending", "void", false);
            AddOperationWithParams(4188, "cancelPending", "void", false);
            AddOperationWithParams(4189, "submitPendingOperations", "void", true);
            AddOperationWithParams(4190, "cancelPendingOperations", "void", true);
            AddOperationWithParams(4191, "save", "void", false);
            Sybase.Reflection.OperationMetaData FindWithQuery30_operation = AddOperationWithParams(4192, "findWithQuery", "Order_Header*", true);
            AddParameterForOperation(FindWithQuery30_operation, "query", "Sybase.Persistence.Query");
            Sybase.Reflection.OperationMetaData GetSize31_operation = AddOperationWithParams(4194, "getSize", "int", true);
            AddParameterForOperation(GetSize31_operation, "query", "Sybase.Persistence.Query");
            Sybase.Reflection.OperationMetaData GetOrderLineItemsFilterBy32_operation = AddOperationWithParams(4197, "GetOrderLineItemsFilterBy", "Order_Line_Items*", false);
            AddParameterForOperation(GetOrderLineItemsFilterBy32_operation, "query", "Sybase.Persistence.Query");
            Sybase.Reflection.OperationMetaData GetOrderLineItemsSize33_operation = AddOperationWithParams(4199, "GetOrderLineItemsSize", "int", false);
            AddParameterForOperation(GetOrderLineItemsSize33_operation, "query", "Sybase.Persistence.Query");
            Sybase.Reflection.OperationMetaData GetOrderDeliveryHeadersFilterBy34_operation = AddOperationWithParams(4201, "GetOrderDeliveryHeadersFilterBy", "Order_Delivery_Header*", false);
            AddParameterForOperation(GetOrderDeliveryHeadersFilterBy34_operation, "query", "Sybase.Persistence.Query");
            Sybase.Reflection.OperationMetaData GetOrderDeliveryHeadersSize35_operation = AddOperationWithParams(4203, "GetOrderDeliveryHeadersSize", "int", false);
            AddParameterForOperation(GetOrderDeliveryHeadersSize35_operation, "query", "Sybase.Persistence.Query");
            InitOperationMapFromOperations();
            SetName("Order_Header");
            SetTable("\"graybarmobility_1_0_order_header\"");
            SetSynchronizationGroup("OrderRelatedSyncGroup");
        }
    }
}