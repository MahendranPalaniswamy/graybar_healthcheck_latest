
namespace GB.intrnl
{
    public class Order_WatchMetaData
        : Sybase.Reflection.EntityMetaData
    {
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public Order_WatchMetaData()
        {
            _init();
        }
        
        protected void _init()
        {
            SetId(40);
            SetAttributes(new Sybase.Reflection.AttributeMetaDataList());
            SetOperations(new Sybase.Reflection.OperationMetaDataList());
            SetAttributeMap(new Sybase.Reflection.AttributeMap());
            SetOperationMap(new Sybase.Reflection.OperationMap());
            Sybase.Reflection.AttributeMetaData orderSurroKey_attribute = AddAttributeWithParams(0, "orderSurroKey", "string", 20, false, true, false, false, false, false, "\"a\"");
            InitAttributeMapFromAttributes();
            AddOperationWithParams(940, "findAll", "Order_Watch*", true);
            Sybase.Reflection.OperationMetaData FindByPrimaryKey1_operation = AddOperationWithParams(942, "findByPrimaryKey", "Order_Watch", true);
            AddParameterForOperation(FindByPrimaryKey1_operation, "orderSurroKey", "string");
            AddOperationWithParams(945, "getAllWatchedOrders", "Sybase.Persistence.QueryResultSet", true);
            AddOperationWithParams(948, "create", "void", false, 'C');
            AddOperationWithParams(949, "update", "void", false, 'U');
            AddOperationWithParams(950, "delete", "void", false, 'D');
            AddOperationWithParams(4207, "isNew", "boolean", false);
            AddOperationWithParams(4208, "isDirty", "boolean", false);
            AddOperationWithParams(4209, "isDeleted", "boolean", false);
            AddOperationWithParams(4210, "refresh", "void", false);
            AddOperationWithParams(4211, "_pk", "string?", false);
            AddOperationWithParams(4212, "save", "void", false);
            Sybase.Reflection.OperationMetaData FindWithQuery12_operation = AddOperationWithParams(4213, "findWithQuery", "Order_Watch*", true);
            AddParameterForOperation(FindWithQuery12_operation, "query", "Sybase.Persistence.Query");
            Sybase.Reflection.OperationMetaData GetSize13_operation = AddOperationWithParams(4215, "getSize", "int", true);
            AddParameterForOperation(GetSize13_operation, "query", "Sybase.Persistence.Query");
            InitOperationMapFromOperations();
            SetName("Order_Watch");
            SetTable("\"co_graybarmobility_1_0_order_watch\"");
            SetSynchronizationGroup("");
        }
    }
}