
namespace GB.intrnl
{
    public class PersonalizationParametersMetaData
        : Sybase.Reflection.ClassMetaData
    {
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public PersonalizationParametersMetaData()
        {
            _init();
        }
        
        protected void _init()
        {
            SetId(92);
            SetAttributes(new Sybase.Reflection.AttributeMetaDataList());
            SetOperations(new Sybase.Reflection.OperationMetaDataList());
            SetAttributeMap(new Sybase.Reflection.AttributeMap());
            SetOperationMap(new Sybase.Reflection.OperationMap());
            Sybase.Reflection.AttributeMetaData listClientPK_attribute = AddAttributeWithParams(0, "listClientPK", "GB.ClientPersonalization*", -1, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData listSessionPK_attribute = AddAttributeWithParams(1, "listSessionPK", "GB.SessionPersonalization*", -1, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData Sold_to_attribute = AddAttributeWithParams(2, "Sold_to", "string", -1, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData Sold_toUserDefined_attribute = AddAttributeWithParams(3, "Sold_toUserDefined", "boolean", -1, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData pickup_location_attribute = AddAttributeWithParams(4, "pickup_location", "string", -1, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData pickup_locationUserDefined_attribute = AddAttributeWithParams(5, "pickup_locationUserDefined", "boolean", -1, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData shipping_location_attribute = AddAttributeWithParams(6, "shipping_location", "string", -1, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData shipping_locationUserDefined_attribute = AddAttributeWithParams(7, "shipping_locationUserDefined", "boolean", -1, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData get_comparables_attribute = AddAttributeWithParams(8, "get_comparables", "string", -1, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData get_comparablesUserDefined_attribute = AddAttributeWithParams(9, "get_comparablesUserDefined", "boolean", -1, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData PreAuthorizeInputStructure_attribute = AddAttributeWithParams(10, "PreAuthorizeInputStructure", "GB.PreAuthorizeInputStructure", -1, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData PreAuthorizeInputStructureUserDefined_attribute = AddAttributeWithParams(11, "PreAuthorizeInputStructureUserDefined", "boolean", -1, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData orderPlant_attribute = AddAttributeWithParams(12, "orderPlant", "string", -1, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData orderPlantUserDefined_attribute = AddAttributeWithParams(13, "orderPlantUserDefined", "boolean", -1, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData orderRoute_attribute = AddAttributeWithParams(14, "orderRoute", "string", -1, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData orderRouteUserDefined_attribute = AddAttributeWithParams(15, "orderRouteUserDefined", "boolean", -1, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData orderPlacedDate_attribute = AddAttributeWithParams(16, "orderPlacedDate", "string", -1, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData orderPlacedDateUserDefined_attribute = AddAttributeWithParams(17, "orderPlacedDateUserDefined", "boolean", -1, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData orderPlacedTime_attribute = AddAttributeWithParams(18, "orderPlacedTime", "string", -1, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData orderPlacedTimeUserDefined_attribute = AddAttributeWithParams(19, "orderPlacedTimeUserDefined", "boolean", -1, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData orderCommType_attribute = AddAttributeWithParams(20, "orderCommType", "string", -1, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData orderCommTypeUserDefined_attribute = AddAttributeWithParams(21, "orderCommTypeUserDefined", "boolean", -1, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData orderCommValue_attribute = AddAttributeWithParams(22, "orderCommValue", "string", -1, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData orderCommValueUserDefined_attribute = AddAttributeWithParams(23, "orderCommValueUserDefined", "boolean", -1, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData orderDeviceInfo_attribute = AddAttributeWithParams(24, "orderDeviceInfo", "string", -1, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData orderDeviceInfoUserDefined_attribute = AddAttributeWithParams(25, "orderDeviceInfoUserDefined", "boolean", -1, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData orderNotes_attribute = AddAttributeWithParams(26, "orderNotes", "string", -1, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData orderNotesUserDefined_attribute = AddAttributeWithParams(27, "orderNotesUserDefined", "boolean", -1, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData sessionPickupBranch_attribute = AddAttributeWithParams(28, "sessionPickupBranch", "string?", -1, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData sessionPickupBranchUserDefined_attribute = AddAttributeWithParams(29, "sessionPickupBranchUserDefined", "boolean", -1, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData orderPlacedBy_attribute = AddAttributeWithParams(30, "orderPlacedBy", "string", -1, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData orderPlacedByUserDefined_attribute = AddAttributeWithParams(31, "orderPlacedByUserDefined", "boolean", -1, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData PandAInputStructure_attribute = AddAttributeWithParams(32, "PandAInputStructure", "GB.PandAInputStructure*", -1, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData PandAInputStructureUserDefined_attribute = AddAttributeWithParams(33, "PandAInputStructureUserDefined", "boolean", -1, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData Shipping_Address_attribute = AddAttributeWithParams(34, "Shipping_Address", "GB.Order_OrderHeader_ShippingAddress", -1, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData Shipping_AddressUserDefined_attribute = AddAttributeWithParams(35, "Shipping_AddressUserDefined", "boolean", -1, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData orderAddrName_attribute = AddAttributeWithParams(36, "orderAddrName", "string?", -1, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData orderAddrNameUserDefined_attribute = AddAttributeWithParams(37, "orderAddrNameUserDefined", "boolean", -1, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData orderAddrName1_attribute = AddAttributeWithParams(38, "orderAddrName1", "string?", -1, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData orderAddrName1UserDefined_attribute = AddAttributeWithParams(39, "orderAddrName1UserDefined", "boolean", -1, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData orderAddrCareOf_attribute = AddAttributeWithParams(40, "orderAddrCareOf", "string?", -1, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData orderAddrCareOfUserDefined_attribute = AddAttributeWithParams(41, "orderAddrCareOfUserDefined", "boolean", -1, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData orderAddrStreet1_attribute = AddAttributeWithParams(42, "orderAddrStreet1", "string?", -1, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData orderAddrStreet1UserDefined_attribute = AddAttributeWithParams(43, "orderAddrStreet1UserDefined", "boolean", -1, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData orderAddrStreet2_attribute = AddAttributeWithParams(44, "orderAddrStreet2", "string?", -1, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData orderAddrStreet2UserDefined_attribute = AddAttributeWithParams(45, "orderAddrStreet2UserDefined", "boolean", -1, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData orderAddrCity_attribute = AddAttributeWithParams(46, "orderAddrCity", "string?", -1, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData orderAddrCityUserDefined_attribute = AddAttributeWithParams(47, "orderAddrCityUserDefined", "boolean", -1, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData orderAddrState_attribute = AddAttributeWithParams(48, "orderAddrState", "string?", -1, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData orderAddrStateUserDefined_attribute = AddAttributeWithParams(49, "orderAddrStateUserDefined", "boolean", -1, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData orderAddrZipCode_attribute = AddAttributeWithParams(50, "orderAddrZipCode", "string?", -1, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData orderAddrZipCodeUserDefined_attribute = AddAttributeWithParams(51, "orderAddrZipCodeUserDefined", "boolean", -1, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData orderAddrCountry_attribute = AddAttributeWithParams(52, "orderAddrCountry", "string?", -1, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData orderAddrCountryUserDefined_attribute = AddAttributeWithParams(53, "orderAddrCountryUserDefined", "boolean", -1, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData orderAddrPhone_attribute = AddAttributeWithParams(54, "orderAddrPhone", "string?", -1, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData orderAddrPhoneUserDefined_attribute = AddAttributeWithParams(55, "orderAddrPhoneUserDefined", "boolean", -1, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData orderCCInfoCardType_attribute = AddAttributeWithParams(56, "orderCCInfoCardType", "string?", -1, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData orderCCInfoCardTypeUserDefined_attribute = AddAttributeWithParams(57, "orderCCInfoCardTypeUserDefined", "boolean", -1, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData orderCCInfoCardNumber_attribute = AddAttributeWithParams(58, "orderCCInfoCardNumber", "string?", -1, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData orderCCInfoCardNumberUserDefined_attribute = AddAttributeWithParams(59, "orderCCInfoCardNumberUserDefined", "boolean", -1, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData orderCCInfoExpirationDate_attribute = AddAttributeWithParams(60, "orderCCInfoExpirationDate", "string?", -1, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData orderCCInfoExpirationDateUserDefined_attribute = AddAttributeWithParams(61, "orderCCInfoExpirationDateUserDefined", "boolean", -1, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData orderCCInfoCCName_attribute = AddAttributeWithParams(62, "orderCCInfoCCName", "string?", -1, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData orderCCInfoCCNameUserDefined_attribute = AddAttributeWithParams(63, "orderCCInfoCCNameUserDefined", "boolean", -1, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData orderLineItemInputStructure_attribute = AddAttributeWithParams(64, "orderLineItemInputStructure", "GB.Order_OrderLineitem*", -1, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData orderLineItemInputStructureUserDefined_attribute = AddAttributeWithParams(65, "orderLineItemInputStructureUserDefined", "boolean", -1, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData OrderCCInfo_attribute = AddAttributeWithParams(66, "OrderCCInfo", "GB.Order_OrderHeader_CardInfo", -1, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData OrderCCInfoUserDefined_attribute = AddAttributeWithParams(67, "OrderCCInfoUserDefined", "boolean", -1, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData username_attribute = AddAttributeWithParams(68, "username", "string", -1, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData usernameUserDefined_attribute = AddAttributeWithParams(69, "usernameUserDefined", "boolean", -1, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData password_attribute = AddAttributeWithParams(70, "password", "string", -1, false, false, true, false, false, true);
            Sybase.Reflection.AttributeMetaData passwordUserDefined_attribute = AddAttributeWithParams(71, "passwordUserDefined", "boolean", -1, false, false, true, false, false, false);
            InitAttributeMapFromAttributes();
            AddOperationWithParams(2354, "load", "void", false);
            AddOperationWithParams(2391, "save", "void", false);
            AddOperationWithParams(2428, "saveUserNamePassword", "void", false);
            InitOperationMapFromOperations();
            SetName("PersonalizationParameters");
            
        }
        
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public  override bool IsEntity()
        {
            return false;
        }
        
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public  override bool IsService()
        {
            return false;
        }
    }
}