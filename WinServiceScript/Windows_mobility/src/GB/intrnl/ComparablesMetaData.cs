
namespace GB.intrnl
{
    public class ComparablesMetaData
        : Sybase.Reflection.EntityMetaData
    {
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public ComparablesMetaData()
        {
            _init();
        }
        
        protected void _init()
        {
            SetId(2);
            SetAttributes(new Sybase.Reflection.AttributeMetaDataList());
            SetOperations(new Sybase.Reflection.OperationMetaDataList());
            SetAttributeMap(new Sybase.Reflection.AttributeMap());
            SetOperationMap(new Sybase.Reflection.OperationMap());
            Sybase.Reflection.AttributeMetaData PRODUCT_ID_attribute = AddAttributeWithParams(0, "PRODUCT_ID", "string", 18, false, false, false, false, false, false, "\"a\"");
            Sybase.Reflection.AttributeMetaData CATALOG_NUMBER_attribute = AddAttributeWithParams(1, "CATALOG_NUMBER", "string", 40, false, false, false, false, false, false, "\"b\"");
            Sybase.Reflection.AttributeMetaData MANUFACTURER_NAME_attribute = AddAttributeWithParams(2, "MANUFACTURER_NAME", "string", 35, false, false, false, false, false, false, "\"c\"");
            Sybase.Reflection.AttributeMetaData DESCRIPTION_attribute = AddAttributeWithParams(3, "DESCRIPTION", "string", 1024, false, false, false, false, false, false, "\"d\"");
            Sybase.Reflection.AttributeMetaData IMAGE_URL_attribute = AddAttributeWithParams(4, "IMAGE_URL", "string", 1024, false, false, false, false, false, false, "\"e\"");
            Sybase.Reflection.AttributeMetaData surrogateKey_attribute = AddAttributeWithParams(5, "surrogateKey", "long", -1, false, true, false, false, false, false, "\"f\"");
            InitAttributeMapFromAttributes();
            AddOperationWithParams(239, "findAll", "Comparables*", true);
            Sybase.Reflection.OperationMetaData FindByPrimaryKey1_operation = AddOperationWithParams(241, "findByPrimaryKey", "Comparables", true);
            AddParameterForOperation(FindByPrimaryKey1_operation, "PRODUCT_ID", "string");
            AddOperationWithParams(244, "getComparables", "Sybase.Persistence.QueryResultSet", true);
            AddOperationWithParams(4027, "isNew", "boolean", false);
            AddOperationWithParams(4028, "isDirty", "boolean", false);
            AddOperationWithParams(4029, "isDeleted", "boolean", false);
            AddOperationWithParams(4030, "refresh", "void", false);
            AddOperationWithParams(4031, "_pk", "long?", false);
            Sybase.Reflection.OperationMetaData FindWithQuery8_operation = AddOperationWithParams(4032, "findWithQuery", "Comparables*", true);
            AddParameterForOperation(FindWithQuery8_operation, "query", "Sybase.Persistence.Query");
            Sybase.Reflection.OperationMetaData GetSize9_operation = AddOperationWithParams(4034, "getSize", "int", true);
            AddParameterForOperation(GetSize9_operation, "query", "Sybase.Persistence.Query");
            InitOperationMapFromOperations();
            SetName("Comparables");
            SetTable("\"graybarmobility_1_0_comparables\"");
            SetSynchronizationGroup("PandAOnDemandSyncGroup");
        }
    }
}