
namespace GB.intrnl
{
    public class ChangeLogImplMetaData
        : Sybase.Reflection.EntityMetaData
    {
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public ChangeLogImplMetaData()
        {
            _init();
        }
        
        protected void _init()
        {
            SetId(1);
            SetAttributes(new Sybase.Reflection.AttributeMetaDataList());
            SetOperations(new Sybase.Reflection.OperationMetaDataList());
            SetAttributeMap(new Sybase.Reflection.AttributeMap());
            SetOperationMap(new Sybase.Reflection.OperationMap());
            Sybase.Reflection.AttributeMetaData operationType_attribute = AddAttributeWithParams(0, "operationType", "char", 1, false, false, false, false, false, false, "\"c\"");
            Sybase.Reflection.AttributeMetaData rootEntityType_attribute = AddAttributeWithParams(1, "rootEntityType", "int", -1, false, false, false, false, false, false, "\"d\"");
            Sybase.Reflection.AttributeMetaData rootSurrogateKey_attribute = AddAttributeWithParams(2, "rootSurrogateKey", "long", -1, false, false, false, false, false, false, "\"e\"");
            Sybase.Reflection.AttributeMetaData entityType_attribute = AddAttributeWithParams(3, "entityType", "int", -1, false, true, false, false, false, false, "\"a\"");
            Sybase.Reflection.AttributeMetaData surrogateKey_attribute = AddAttributeWithParams(4, "surrogateKey", "long", -1, false, true, false, false, false, false, "\"b\"");
            InitAttributeMapFromAttributes();
            AddOperationWithParams(4456, "isNew", "boolean", false);
            AddOperationWithParams(4457, "isDirty", "boolean", false);
            AddOperationWithParams(4458, "isDeleted", "boolean", false);
            AddOperationWithParams(4459, "refresh", "void", false);
            AddOperationWithParams(4460, "save", "void", false);
            Sybase.Reflection.OperationMetaData FindWithQuery5_operation = AddOperationWithParams(4461, "findWithQuery", "ChangeLogImpl*", true);
            AddParameterForOperation(FindWithQuery5_operation, "query", "Sybase.Persistence.Query");
            Sybase.Reflection.OperationMetaData GetSize6_operation = AddOperationWithParams(4463, "getSize", "int", true);
            AddParameterForOperation(GetSize6_operation, "query", "Sybase.Persistence.Query");
            InitOperationMapFromOperations();
            SetName("ChangeLogImpl");
            SetTable("\"graybarmobility_1_0_changelogimpl\"");
            SetSynchronizationGroup("SoldToInfoSyncGroup");
        }
    }
}