using System;
using GB;

namespace GB.intrnl
{
    public class SynchronizationGroup  : Sybase.Persistence.ISynchronizationGroup
    {        
        private string name;
        private SISSubscription __sisSubscription;

        private string GetRemoteId()
        {
            foreach (GB.KeyGenerator kg in GB.KeyGenerator.FindAll())
            {
                if (!kg.RemoteId.Equals(".")) return kg.RemoteId;
            }
            return "";
        }
        
        private SynchronizationGroup(String ___name)
        {     
            name = ___name; 
            __sisSubscription = null;
        }
		
        private void initialize()
        {
            SISSubscriptionKey _key = new SISSubscriptionKey();
            _key.ClientId = GetRemoteId();
            string domainName = GraybarMobilityDB.GetSynchronizationProfile().DomainName;
            if(domainName == null || domainName.Length == 0)
            {
                domainName = Sybase.Mobile.Application.GetInstance().ApplicationSettings.GetStringProperty(Sybase.Mobile.ConnectionPropertyType.DomainName);
            }
            if(domainName == null || domainName.Length == 0)
            {
                domainName = "default";
            }
            _key.Domain = domainName;
            _key.Package = GraybarMobilityDB.GetPackageName();
            _key.SyncGroup = name;
            __sisSubscription = SISSubscription.Find(_key);
            if(__sisSubscription == null)
            {
                __sisSubscription = new SISSubscription();
                __sisSubscription.Domain = _key.Domain;
                __sisSubscription.Package = _key.Package;
                __sisSubscription.SyncGroup = _key.SyncGroup;
                __sisSubscription.ClientId = _key.ClientId;
                __sisSubscription.Username = GraybarMobilityDB.GetSyncUsername();
                __sisSubscription.DeviceId = com.sybase.afx.util.PidUtil.GetId();
            }
        }

        private void checkInitialize()
        {
            if (__sisSubscription == null)
            {
                lock (this)
                {
                    if (__sisSubscription == null)
                    {
                        initialize();
                    }
                }
            }
        }

        public static SynchronizationGroup GetInstance(string ___name)
        {
            return new SynchronizationGroup(___name);
        }
                

        public string Name
        {
            get
            {               
                return name;
            }
        }

        public bool AdminLock
        {
            get
            {
                checkInitialize();
                return __sisSubscription.AdminLock;
            }
        }

        public bool EnableSIS
        {
            get
            {
                checkInitialize();
                return __sisSubscription.Enable;
            }
            set
            {
                checkInitialize(); 
                __sisSubscription.Enable = value;
            }
        }

        public int Interval
        {
            get
            {
                checkInitialize();
                return __sisSubscription.Interval;
            }
            set
            {
                checkInitialize(); 
                __sisSubscription.Interval = value;
            }
        }

        public  void Save()
        {
            checkInitialize(); 
            __sisSubscription.Protocol = GraybarMobilityDB.GetSynchronizationProfile().GetProperty("protocol", "TCN");
            __sisSubscription.Address = GraybarMobilityDB.GetSynchronizationProfile().GetProperty("address");
            __sisSubscription.Appname = GraybarMobilityDB.GetSynchronizationProfile().GetProperty("appname", "sup");
            __sisSubscription.Save();
        }
        
        public string ToString()
        {
            return name;
        }
    }
}