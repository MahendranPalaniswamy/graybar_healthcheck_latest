
namespace GB.intrnl
{
    public class Shipping_AddressMetaData
        : Sybase.Reflection.EntityMetaData
    {
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public Shipping_AddressMetaData()
        {
            _init();
        }
        
        protected void _init()
        {
            SetId(32);
            SetAttributes(new Sybase.Reflection.AttributeMetaDataList());
            SetOperations(new Sybase.Reflection.OperationMetaDataList());
            SetAttributeMap(new Sybase.Reflection.AttributeMap());
            SetOperationMap(new Sybase.Reflection.OperationMap());
            Sybase.Reflection.AttributeMetaData SOLD_TO_attribute = AddAttributeWithParams(0, "SOLD_TO", "string", 10, false, false, false, false, false, false, "\"a\"");
            Sybase.Reflection.AttributeMetaData SHIP_TO_attribute = AddAttributeWithParams(1, "SHIP_TO", "string", 10, false, false, false, false, false, false, "\"b\"");
            Sybase.Reflection.AttributeMetaData DEFAULT_SHIPTO_attribute = AddAttributeWithParams(2, "DEFAULT_SHIPTO", "string", 1, false, false, false, false, false, false, "\"c\"");
            Sybase.Reflection.AttributeMetaData NAME_attribute = AddAttributeWithParams(3, "NAME", "string", 40, false, false, false, false, false, false, "\"d\"");
            Sybase.Reflection.AttributeMetaData NAME2_attribute = AddAttributeWithParams(4, "NAME2", "string", 40, false, false, false, false, false, false, "\"e\"");
            Sybase.Reflection.AttributeMetaData CARE_OF_attribute = AddAttributeWithParams(5, "CARE_OF", "string", 40, false, false, false, false, false, false, "\"f\"");
            Sybase.Reflection.AttributeMetaData STREET1_attribute = AddAttributeWithParams(6, "STREET1", "string", 60, false, false, false, false, false, false, "\"g\"");
            Sybase.Reflection.AttributeMetaData STREET2_attribute = AddAttributeWithParams(7, "STREET2", "string", 60, false, false, false, false, false, false, "\"h\"");
            Sybase.Reflection.AttributeMetaData CITY_attribute = AddAttributeWithParams(8, "CITY", "string", 40, false, false, false, false, false, false, "\"i\"");
            Sybase.Reflection.AttributeMetaData STATE_attribute = AddAttributeWithParams(9, "STATE", "string", 3, false, false, false, false, false, false, "\"j\"");
            Sybase.Reflection.AttributeMetaData ZIPCODE_attribute = AddAttributeWithParams(10, "ZIPCODE", "string", 10, false, false, false, false, false, false, "\"l\"");
            Sybase.Reflection.AttributeMetaData COUNTRY_attribute = AddAttributeWithParams(11, "COUNTRY", "string", 3, false, false, false, false, false, false, "\"m\"");
            Sybase.Reflection.AttributeMetaData surrogateKey_attribute = AddAttributeWithParams(12, "surrogateKey", "long", -1, false, true, false, false, false, false, "\"n\"");
            InitAttributeMapFromAttributes();
            AddOperationWithParams(1155, "findAll", "Shipping_Address*", true);
            Sybase.Reflection.OperationMetaData FindBySOLD_TO1_operation = AddOperationWithParams(1157, "findBySOLD_TO", "Shipping_Address*", true);
            AddParameterForOperation(FindBySOLD_TO1_operation, "SOLD_TO", "string");
            Sybase.Reflection.OperationMetaData FindBySHIP_TO2_operation = AddOperationWithParams(1160, "findBySHIP_TO", "Shipping_Address*", true);
            AddParameterForOperation(FindBySHIP_TO2_operation, "SHIP_TO", "string");
            Sybase.Reflection.OperationMetaData FindByPrimaryKey3_operation = AddOperationWithParams(1163, "findByPrimaryKey", "Shipping_Address", true);
            AddParameterForOperation(FindByPrimaryKey3_operation, "SOLD_TO", "string");
            AddParameterForOperation(FindByPrimaryKey3_operation, "SHIP_TO", "string");
            AddOperationWithParams(1167, "getAllShippingAddresses", "Sybase.Persistence.QueryResultSet", true);
            Sybase.Reflection.OperationMetaData GetByPrimaryKey5_operation = AddOperationWithParams(1169, "getByPrimaryKey", "Sybase.Persistence.QueryResultSet", true);
            AddParameterForOperation(GetByPrimaryKey5_operation, "ship_to", "string");
            AddOperationWithParams(4270, "isNew", "boolean", false);
            AddOperationWithParams(4271, "isDirty", "boolean", false);
            AddOperationWithParams(4272, "isDeleted", "boolean", false);
            AddOperationWithParams(4273, "refresh", "void", false);
            AddOperationWithParams(4274, "_pk", "long?", false);
            Sybase.Reflection.OperationMetaData FindWithQuery11_operation = AddOperationWithParams(4275, "findWithQuery", "Shipping_Address*", true);
            AddParameterForOperation(FindWithQuery11_operation, "query", "Sybase.Persistence.Query");
            Sybase.Reflection.OperationMetaData GetSize12_operation = AddOperationWithParams(4277, "getSize", "int", true);
            AddParameterForOperation(GetSize12_operation, "query", "Sybase.Persistence.Query");
            InitOperationMapFromOperations();
            SetName("Shipping_Address");
            SetTable("\"graybarmobility_1_0_shipping_address\"");
            SetSynchronizationGroup("ReferenceSyncGroup");
        }
    }
}