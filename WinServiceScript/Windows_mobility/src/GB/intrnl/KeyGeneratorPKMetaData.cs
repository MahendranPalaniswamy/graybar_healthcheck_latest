
namespace GB.intrnl
{
    public class KeyGeneratorPKMetaData
        : Sybase.Reflection.ClassMetaData
    {
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public KeyGeneratorPKMetaData()
        {
            _init();
        }
        
        protected void _init()
        {
            SetId(94);
            SetAttributes(new Sybase.Reflection.AttributeMetaDataList());
            SetOperations(new Sybase.Reflection.OperationMetaDataList());
            SetAttributeMap(new Sybase.Reflection.AttributeMap());
            SetOperationMap(new Sybase.Reflection.OperationMap());
            Sybase.Reflection.AttributeMetaData remoteId_attribute = AddAttributeWithParams(0, "remoteId", "string", -1, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData batchId_attribute = AddAttributeWithParams(1, "batchId", "long", -1, false, false, true, false, false, false);
            InitAttributeMapFromAttributes();
            SetName("KeyGeneratorPK");
            
        }
        
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public  override bool IsEntity()
        {
            return false;
        }
        
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public  override bool IsService()
        {
            return false;
        }
    }
}