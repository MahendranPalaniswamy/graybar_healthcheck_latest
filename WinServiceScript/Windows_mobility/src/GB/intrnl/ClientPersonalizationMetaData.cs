
namespace GB.intrnl
{
    public class ClientPersonalizationMetaData
        : Sybase.Reflection.EntityMetaData
    {
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public ClientPersonalizationMetaData()
        {
            _init();
        }
        
        protected void _init()
        {
            SetId(55);
            SetAttributes(new Sybase.Reflection.AttributeMetaDataList());
            SetOperations(new Sybase.Reflection.OperationMetaDataList());
            SetAttributeMap(new Sybase.Reflection.AttributeMap());
            SetOperationMap(new Sybase.Reflection.OperationMap());
            Sybase.Reflection.AttributeMetaData key_name_attribute = AddAttributeWithParams(0, "key_name", "string", 300, false, false, true, false, false, false, "\"key_name\"");
            Sybase.Reflection.AttributeMetaData user_attribute = AddAttributeWithParams(1, "user", "string", 300, false, false, true, false, false, false, "\"user\"");
            Sybase.Reflection.AttributeMetaData value_attribute = AddAttributeWithParams(2, "value", "string?", -1, false, false, true, false, false, false, "\"value\"");
            Sybase.Reflection.AttributeMetaData user_defined_attribute = AddAttributeWithParams(3, "user_defined", "boolean", -1, false, false, true, false, false, false, "\"user_defined\"");
            Sybase.Reflection.AttributeMetaData description_attribute = AddAttributeWithParams(4, "description", "string?", 300, false, false, true, false, false, false, "\"description\"");
            Sybase.Reflection.AttributeMetaData id_attribute = AddAttributeWithParams(5, "id", "long", -1, false, true, true, false, false, false, "\"id\"");
            InitAttributeMapFromAttributes();
            Sybase.Reflection.OperationMetaData FindByUser0_operation = AddOperationWithParams(2240, "findByUser", "ClientPersonalization*", true);
            AddParameterForOperation(FindByUser0_operation, "user", "string");
            AddOperationWithParams(2243, "findAll", "ClientPersonalization*", true);
            AddOperationWithParams(2247, "getRealValue", "string", false);
            AddOperationWithParams(4641, "isNew", "boolean", false);
            AddOperationWithParams(4642, "isDirty", "boolean", false);
            AddOperationWithParams(4643, "isDeleted", "boolean", false);
            AddOperationWithParams(4644, "refresh", "void", false);
            AddOperationWithParams(4645, "_pk", "long?", false);
            AddOperationWithParams(4646, "save", "void", false);
            InitOperationMapFromOperations();
            SetName("ClientPersonalization");
            SetTable("\"co_graybarmobility_1_0_clientpersonalization\"");
            SetSynchronizationGroup("");
        }
    }
}