
namespace GB.intrnl
{
    public class PreAuthorizeMetaData
        : Sybase.Reflection.EntityMetaData
    {
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public PreAuthorizeMetaData()
        {
            _init();
        }
        
        protected void _init()
        {
            SetId(26);
            SetAttributes(new Sybase.Reflection.AttributeMetaDataList());
            SetOperations(new Sybase.Reflection.OperationMetaDataList());
            SetAttributeMap(new Sybase.Reflection.AttributeMap());
            SetOperationMap(new Sybase.Reflection.OperationMap());
            Sybase.Reflection.AttributeMetaData TYPE_attribute = AddAttributeWithParams(0, "TYPE", "string", 1, false, false, false, false, false, false, "\"a\"");
            Sybase.Reflection.AttributeMetaData ID_attribute = AddAttributeWithParams(1, "ID", "string", 20, false, false, false, false, false, false, "\"b\"");
            Sybase.Reflection.AttributeMetaData NUMBER_attribute = AddAttributeWithParams(2, "NUMBER", "string", 3, false, false, false, false, false, false, "\"c\"");
            Sybase.Reflection.AttributeMetaData MESSAGE_attribute = AddAttributeWithParams(3, "MESSAGE", "string", 220, false, false, false, false, false, false, "\"d\"");
            Sybase.Reflection.AttributeMetaData LOG_NO_attribute = AddAttributeWithParams(4, "LOG_NO", "string", 20, false, false, false, false, false, false, "\"e\"");
            Sybase.Reflection.AttributeMetaData LOG_MSG_NO_attribute = AddAttributeWithParams(5, "LOG_MSG_NO", "string", 6, false, false, false, false, false, false, "\"f\"");
            Sybase.Reflection.AttributeMetaData MESSAGE_V1_attribute = AddAttributeWithParams(6, "MESSAGE_V1", "string", 50, false, false, false, false, false, false, "\"g\"");
            Sybase.Reflection.AttributeMetaData MESSAGE_V2_attribute = AddAttributeWithParams(7, "MESSAGE_V2", "string", 50, false, false, false, false, false, false, "\"h\"");
            Sybase.Reflection.AttributeMetaData MESSAGE_V3_attribute = AddAttributeWithParams(8, "MESSAGE_V3", "string", 50, false, false, false, false, false, false, "\"i\"");
            Sybase.Reflection.AttributeMetaData MESSAGE_V4_attribute = AddAttributeWithParams(9, "MESSAGE_V4", "string", 50, false, false, false, false, false, false, "\"j\"");
            Sybase.Reflection.AttributeMetaData PARAMETER_attribute = AddAttributeWithParams(10, "PARAMETER", "string", 32, false, false, false, false, false, false, "\"l\"");
            Sybase.Reflection.AttributeMetaData ROW_attribute = AddAttributeWithParams(11, "ROW", "int", -1, false, false, false, false, false, false, "\"m\"");
            Sybase.Reflection.AttributeMetaData FIELD_attribute = AddAttributeWithParams(12, "FIELD", "string", 30, false, false, false, false, false, false, "\"n\"");
            Sybase.Reflection.AttributeMetaData SYSTEM_attribute = AddAttributeWithParams(13, "SYSTEM", "string", 10, false, false, false, false, false, false, "\"o\"");
            Sybase.Reflection.AttributeMetaData surrogateKey_attribute = AddAttributeWithParams(14, "surrogateKey", "long", -1, false, true, false, false, false, false, "\"p\"");
            InitAttributeMapFromAttributes();
            AddOperationWithParams(1004, "findAll", "PreAuthorize*", true);
            Sybase.Reflection.OperationMetaData FindByPrimaryKey1_operation = AddOperationWithParams(1006, "findByPrimaryKey", "PreAuthorize", true);
            AddParameterForOperation(FindByPrimaryKey1_operation, "ID", "string");
            AddOperationWithParams(1009, "getPreAuthorization", "Sybase.Persistence.QueryResultSet", true);
            AddOperationWithParams(4228, "isNew", "boolean", false);
            AddOperationWithParams(4229, "isDirty", "boolean", false);
            AddOperationWithParams(4230, "isDeleted", "boolean", false);
            AddOperationWithParams(4231, "refresh", "void", false);
            AddOperationWithParams(4232, "_pk", "long?", false);
            Sybase.Reflection.OperationMetaData FindWithQuery8_operation = AddOperationWithParams(4233, "findWithQuery", "PreAuthorize*", true);
            AddParameterForOperation(FindWithQuery8_operation, "query", "Sybase.Persistence.Query");
            Sybase.Reflection.OperationMetaData GetSize9_operation = AddOperationWithParams(4235, "getSize", "int", true);
            AddParameterForOperation(GetSize9_operation, "query", "Sybase.Persistence.Query");
            InitOperationMapFromOperations();
            SetName("PreAuthorize");
            SetTable("\"graybarmobility_1_0_preauthorize\"");
            SetSynchronizationGroup("PreAuthOnDemandSyncGroup");
        }
    }
}