
namespace GB.intrnl
{
    public class PandAInputStructureMetaData
        : Sybase.Reflection.ClassMetaData
    {
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public PandAInputStructureMetaData()
        {
            _init();
        }
        
        protected void _init()
        {
            SetId(61);
            SetAttributes(new Sybase.Reflection.AttributeMetaDataList());
            SetOperations(new Sybase.Reflection.OperationMetaDataList());
            SetAttributeMap(new Sybase.Reflection.AttributeMap());
            SetOperationMap(new Sybase.Reflection.OperationMap());
            Sybase.Reflection.AttributeMetaData MATERIAL_attribute = AddAttributeWithParams(0, "MATERIAL", "string", 18, false, false, false, false, false, false);
            Sybase.Reflection.AttributeMetaData UPC_CODE_attribute = AddAttributeWithParams(1, "UPC_CODE", "string", 18, false, false, false, false, false, false);
            Sybase.Reflection.AttributeMetaData CHECK_PRICE_attribute = AddAttributeWithParams(2, "CHECK_PRICE", "string", 1, false, false, false, false, false, false);
            Sybase.Reflection.AttributeMetaData CHECK_AVAIL_attribute = AddAttributeWithParams(3, "CHECK_AVAIL", "string", 1, false, false, false, false, false, false);
            InitAttributeMapFromAttributes();
            SetName("PandAInputStructure");
            
        }
        
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public  override bool IsEntity()
        {
            return false;
        }
        
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public  override bool IsService()
        {
            return false;
        }
    }
}