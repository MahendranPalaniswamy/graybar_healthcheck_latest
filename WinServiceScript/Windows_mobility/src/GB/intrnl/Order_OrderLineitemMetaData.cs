
namespace GB.intrnl
{
    public class Order_OrderLineitemMetaData
        : Sybase.Reflection.ClassMetaData
    {
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public Order_OrderLineitemMetaData()
        {
            _init();
        }
        
        protected void _init()
        {
            SetId(64);
            SetAttributes(new Sybase.Reflection.AttributeMetaDataList());
            SetOperations(new Sybase.Reflection.OperationMetaDataList());
            SetAttributeMap(new Sybase.Reflection.AttributeMap());
            SetOperationMap(new Sybase.Reflection.OperationMap());
            Sybase.Reflection.AttributeMetaData SoldToPartner_attribute = AddAttributeWithParams(0, "SoldToPartner", "string", 300, false, false, false, false, false, false);
            Sybase.Reflection.AttributeMetaData OrderNumber_attribute = AddAttributeWithParams(1, "OrderNumber", "string", 300, false, false, false, false, false, false);
            Sybase.Reflection.AttributeMetaData MobileOrderNumber_attribute = AddAttributeWithParams(2, "MobileOrderNumber", "string", 300, false, false, false, false, false, false);
            Sybase.Reflection.AttributeMetaData LineItemId_attribute = AddAttributeWithParams(3, "LineItemId", "string", 300, false, false, false, false, false, false);
            Sybase.Reflection.AttributeMetaData ProductId_attribute = AddAttributeWithParams(4, "ProductId", "string", 300, false, false, false, false, false, false);
            Sybase.Reflection.AttributeMetaData CatalogNumber_attribute = AddAttributeWithParams(5, "CatalogNumber", "string", 300, false, false, false, false, false, false);
            Sybase.Reflection.AttributeMetaData ManufacturerName_attribute = AddAttributeWithParams(6, "ManufacturerName", "string", 300, false, false, false, false, false, false);
            Sybase.Reflection.AttributeMetaData ProductStatus_attribute = AddAttributeWithParams(7, "ProductStatus", "string", 300, false, false, false, false, false, false);
            Sybase.Reflection.AttributeMetaData ProductStatusText_attribute = AddAttributeWithParams(8, "ProductStatusText", "string", 300, false, false, false, false, false, false);
            Sybase.Reflection.AttributeMetaData IsPickup_attribute = AddAttributeWithParams(9, "IsPickup", "string", 300, false, false, false, false, false, false);
            Sybase.Reflection.AttributeMetaData Quantity_attribute = AddAttributeWithParams(10, "Quantity", "decimal", -1, false, false, false, false, false, false);
            Sybase.Reflection.AttributeMetaData ProductTotalPrice_attribute = AddAttributeWithParams(11, "ProductTotalPrice", "decimal", -1, false, false, false, false, false, false);
            Sybase.Reflection.AttributeMetaData Unit_attribute = AddAttributeWithParams(12, "Unit", "string", 300, false, false, false, false, false, false);
            Sybase.Reflection.AttributeMetaData PricePerUnit_attribute = AddAttributeWithParams(13, "PricePerUnit", "decimal", -1, false, false, false, false, false, false);
            Sybase.Reflection.AttributeMetaData PerValue_attribute = AddAttributeWithParams(14, "PerValue", "decimal", -1, false, false, false, false, false, false);
            Sybase.Reflection.AttributeMetaData Description_attribute = AddAttributeWithParams(15, "Description", "string", 300, false, false, false, false, false, false);
            Sybase.Reflection.AttributeMetaData ImageUrl_attribute = AddAttributeWithParams(16, "ImageUrl", "string", 300, false, false, false, false, false, false);
            Sybase.Reflection.AttributeMetaData Route_attribute = AddAttributeWithParams(17, "Route", "string", 300, false, false, false, false, false, false);
            Sybase.Reflection.AttributeMetaData Plant_attribute = AddAttributeWithParams(18, "Plant", "string", 300, false, false, false, false, false, false);
            InitAttributeMapFromAttributes();
            SetName("Order_OrderLineitem");
            
        }
        
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public  override bool IsEntity()
        {
            return false;
        }
        
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public  override bool IsService()
        {
            return false;
        }
    }
}