
namespace GB.intrnl
{
    public class FavoriteItem_pull_pqMetaData
        : Sybase.Reflection.EntityMetaData
    {
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public FavoriteItem_pull_pqMetaData()
        {
            _init();
        }
        
        protected void _init()
        {
            SetId(8);
            SetAttributes(new Sybase.Reflection.AttributeMetaDataList());
            SetOperations(new Sybase.Reflection.OperationMetaDataList());
            SetAttributeMap(new Sybase.Reflection.AttributeMap());
            SetOperationMap(new Sybase.Reflection.OperationMap());
            Sybase.Reflection.AttributeMetaData username_attribute = AddAttributeWithParams(0, "username", "string?", 300, false, false, true, false, false, false, "\"username\"");
            Sybase.Reflection.AttributeMetaData remoteId_attribute = AddAttributeWithParams(1, "remoteId", "string?", 300, false, false, true, false, false, false, "\"remoteId\"");
            Sybase.Reflection.AttributeMetaData user_idParam_attribute = AddAttributeWithParams(2, "user_idParam", "string?", 100, false, false, true, false, false, false, "\"user_idParam\"");
            Sybase.Reflection.AttributeMetaData id_attribute = AddAttributeWithParams(3, "id", "long", -1, false, true, true, false, false, false, "\"id\"");
            InitAttributeMapFromAttributes();
            AddOperationWithParams(1797, "findAll", "FavoriteItem_pull_pq*", true);
            Sybase.Reflection.OperationMetaData FindSub1_operation = AddOperationWithParams(1799, "findSub", "FavoriteItem_pull_pq?", true);
            AddParameterForOperation(FindSub1_operation, "_username", "string?");
            AddParameterForOperation(FindSub1_operation, "user_id", "string?");
            AddOperationWithParams(4508, "isNew", "boolean", false);
            AddOperationWithParams(4509, "isDirty", "boolean", false);
            AddOperationWithParams(4510, "isDeleted", "boolean", false);
            AddOperationWithParams(4511, "refresh", "void", false);
            AddOperationWithParams(4512, "_pk", "long?", false);
            AddOperationWithParams(4513, "save", "void", false);
            InitOperationMapFromOperations();
            SetName("FavoriteItem_pull_pq");
            SetTable("\"graybarmobility_1_0_favoriteitem_pull_pq\"");
            SetSynchronizationGroup("unsubscribe");
        }
    }
}