
namespace GB.intrnl
{
    public class Order_OrderHeaderMetaData
        : Sybase.Reflection.ClassMetaData
    {
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public Order_OrderHeaderMetaData()
        {
            _init();
        }
        
        protected void _init()
        {
            SetId(63);
            SetAttributes(new Sybase.Reflection.AttributeMetaDataList());
            SetOperations(new Sybase.Reflection.OperationMetaDataList());
            SetAttributeMap(new Sybase.Reflection.AttributeMap());
            SetOperationMap(new Sybase.Reflection.OperationMap());
            Sybase.Reflection.AttributeMetaData PlacedDate_attribute = AddAttributeWithParams(0, "PlacedDate", "string", 300, false, false, false, false, false, false);
            Sybase.Reflection.AttributeMetaData PlacedTime_attribute = AddAttributeWithParams(1, "PlacedTime", "string", 300, false, false, false, false, false, false);
            Sybase.Reflection.AttributeMetaData PlacedBy_attribute = AddAttributeWithParams(2, "PlacedBy", "string", 300, false, false, false, false, false, false);
            Sybase.Reflection.AttributeMetaData DeviceInformation_attribute = AddAttributeWithParams(3, "DeviceInformation", "string", 300, false, false, false, false, false, false);
            Sybase.Reflection.AttributeMetaData Notes_attribute = AddAttributeWithParams(4, "Notes", "string", 300, false, false, false, false, false, false);
            Sybase.Reflection.AttributeMetaData CommunicationMethodType_attribute = AddAttributeWithParams(5, "CommunicationMethodType", "string", 300, false, false, false, false, false, false);
            Sybase.Reflection.AttributeMetaData CommunicationMethodValue_attribute = AddAttributeWithParams(6, "CommunicationMethodValue", "string", 300, false, false, false, false, false, false);
            Sybase.Reflection.AttributeMetaData SoldToPartner_attribute = AddAttributeWithParams(7, "SoldToPartner", "string", 300, false, false, false, false, false, false);
            Sybase.Reflection.AttributeMetaData ShipToPartner_attribute = AddAttributeWithParams(8, "ShipToPartner", "string", 300, false, false, false, false, false, false);
            Sybase.Reflection.AttributeMetaData OrderNumber_attribute = AddAttributeWithParams(9, "OrderNumber", "string", 300, false, false, false, false, false, false);
            Sybase.Reflection.AttributeMetaData MobileOrderNumber_attribute = AddAttributeWithParams(10, "MobileOrderNumber", "string", 300, false, false, false, false, false, false);
            Sybase.Reflection.AttributeMetaData IsMobileOrder_attribute = AddAttributeWithParams(11, "IsMobileOrder", "string", 300, false, false, false, false, false, false);
            Sybase.Reflection.AttributeMetaData CustomerPONumber_attribute = AddAttributeWithParams(12, "CustomerPONumber", "string", 300, false, false, false, false, false, false);
            Sybase.Reflection.AttributeMetaData EmailAddressOfCustomer_attribute = AddAttributeWithParams(13, "EmailAddressOfCustomer", "string", 300, false, false, false, false, false, false);
            Sybase.Reflection.AttributeMetaData ReleaseNumber_attribute = AddAttributeWithParams(14, "ReleaseNumber", "string", 300, false, false, false, false, false, false);
            Sybase.Reflection.AttributeMetaData OrderTotal_attribute = AddAttributeWithParams(15, "OrderTotal", "decimal", -1, false, false, false, false, false, false);
            Sybase.Reflection.AttributeMetaData OrderStatus_attribute = AddAttributeWithParams(16, "OrderStatus", "string", 300, false, false, false, false, false, false);
            Sybase.Reflection.AttributeMetaData OrderStatusText_attribute = AddAttributeWithParams(17, "OrderStatusText", "string", 300, false, false, false, false, false, false);
            Sybase.Reflection.AttributeMetaData OrderDate_attribute = AddAttributeWithParams(18, "OrderDate", "date", -1, false, false, false, false, false, false);
            Sybase.Reflection.AttributeMetaData ShippingAddress_attribute = AddAttributeWithParams(19, "ShippingAddress", "GB.Order_OrderHeader_ShippingAddress", -1, false, false, false, false, false, false);
            Sybase.Reflection.AttributeMetaData CardInfo_attribute = AddAttributeWithParams(20, "CardInfo", "GB.Order_OrderHeader_CardInfo", -1, false, false, false, false, false, false);
            InitAttributeMapFromAttributes();
            SetName("Order_OrderHeader");
            
        }
        
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public  override bool IsEntity()
        {
            return false;
        }
        
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public  override bool IsService()
        {
            return false;
        }
    }
}