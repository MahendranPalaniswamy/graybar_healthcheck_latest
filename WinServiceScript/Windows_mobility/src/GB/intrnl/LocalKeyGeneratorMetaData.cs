
namespace GB.intrnl
{
    public class LocalKeyGeneratorMetaData
        : Sybase.Reflection.EntityMetaData
    {
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public LocalKeyGeneratorMetaData()
        {
            _init();
        }
        
        protected void _init()
        {
            SetId(56);
            SetAttributes(new Sybase.Reflection.AttributeMetaDataList());
            SetOperations(new Sybase.Reflection.OperationMetaDataList());
            SetAttributeMap(new Sybase.Reflection.AttributeMap());
            SetOperationMap(new Sybase.Reflection.OperationMap());
            Sybase.Reflection.AttributeMetaData firstId_attribute = AddAttributeWithParams(0, "firstId", "long", -1, false, false, true, false, false, false, "\"first_id\"");
            Sybase.Reflection.AttributeMetaData lastId_attribute = AddAttributeWithParams(1, "lastId", "long", -1, false, false, true, false, false, false, "\"last_id\"");
            Sybase.Reflection.AttributeMetaData nextId_attribute = AddAttributeWithParams(2, "nextId", "long", -1, false, false, true, false, false, false, "\"next_id\"");
            Sybase.Reflection.AttributeMetaData remoteId_attribute = AddAttributeWithParams(3, "remoteId", "string", 300, false, true, true, false, false, false, "\"remote_id\"");
            Sybase.Reflection.AttributeMetaData batchId_attribute = AddAttributeWithParams(4, "batchId", "long", -1, false, true, true, false, false, false, "\"batch_id\"");
            InitAttributeMapFromAttributes();
            AddOperationWithParams(4010, "findAll", "LocalKeyGenerator*", true);
            AddOperationWithParams(4654, "isNew", "boolean", false);
            AddOperationWithParams(4655, "isDirty", "boolean", false);
            AddOperationWithParams(4656, "isDeleted", "boolean", false);
            AddOperationWithParams(4657, "refresh", "void", false);
            AddOperationWithParams(4658, "save", "void", false);
            InitOperationMapFromOperations();
            SetName("LocalKeyGenerator");
            SetTable("\"co_graybarmobility_1_0_localkeygenerator\"");
            SetSynchronizationGroup("");
        }
    }
}