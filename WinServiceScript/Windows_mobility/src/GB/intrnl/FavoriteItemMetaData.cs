
namespace GB.intrnl
{
    public class FavoriteItemMetaData
        : Sybase.Reflection.EntityMetaData
    {
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public FavoriteItemMetaData()
        {
            _init();
        }
        
        protected void _init()
        {
            SetId(7);
            SetAttributes(new Sybase.Reflection.AttributeMetaDataList());
            SetOperations(new Sybase.Reflection.OperationMetaDataList());
            SetAttributeMap(new Sybase.Reflection.AttributeMap());
            SetOperationMap(new Sybase.Reflection.OperationMap());
            Sybase.Reflection.AttributeMetaData user_id_attribute = AddAttributeWithParams(0, "user_id", "string?", 100, false, false, false, false, false, false, "\"a\"");
            Sybase.Reflection.AttributeMetaData catalog_number_attribute = AddAttributeWithParams(1, "catalog_number", "string?", 40, false, false, false, false, false, false, "\"b\"");
            Sybase.Reflection.AttributeMetaData description_attribute = AddAttributeWithParams(2, "description", "string?", 1024, false, false, false, false, false, false, "\"c\"");
            Sybase.Reflection.AttributeMetaData fav_item_id_attribute = AddAttributeWithParams(3, "fav_item_id", "string", 100, false, false, false, false, false, false, "\"d\"");
            Sybase.Reflection.AttributeMetaData fav_list_id_attribute = AddAttributeWithParams(4, "fav_list_id", "string?", 100, false, false, false, false, false, false, "\"e\"");
            Sybase.Reflection.AttributeMetaData image_url_attribute = AddAttributeWithParams(5, "image_url", "string?", 1024, false, false, false, false, false, false, "\"f\"");
            Sybase.Reflection.AttributeMetaData last_access_attribute = AddAttributeWithParams(6, "last_access", "string?", 100, false, false, false, false, false, false, "\"g\"");
            Sybase.Reflection.AttributeMetaData manufacturer_name_attribute = AddAttributeWithParams(7, "manufacturer_name", "string?", 35, false, false, false, false, false, false, "\"h\"");
            Sybase.Reflection.AttributeMetaData product_id_attribute = AddAttributeWithParams(8, "product_id", "string?", 18, false, false, false, false, false, false, "\"i\"");
            Sybase.Reflection.AttributeMetaData customerPartNumber_attribute = AddAttributeWithParams(9, "customerPartNumber", "string?", 35, false, false, false, false, false, false, "\"j\"");
            Sybase.Reflection.AttributeMetaData pending_attribute = AddAttributeWithParams(20001, "pending", "boolean", -1, false, false, true, false, false, false, "\"pending\"");
            Sybase.Reflection.AttributeMetaData pendingChange_attribute = AddAttributeWithParams(20002, "pendingChange", "char", 1, false, false, true, false, false, false, "\"_pc\"");
            Sybase.Reflection.AttributeMetaData replayPending_attribute = AddAttributeWithParams(20005, "replayPending", "long", -1, false, false, true, false, false, false, "\"_rp\"");
            Sybase.Reflection.AttributeMetaData replayFailure_attribute = AddAttributeWithParams(20006, "replayFailure", "long", -1, false, false, true, false, false, false, "\"_rf\"");
            Sybase.Reflection.AttributeMetaData cvpOperation_attribute = AddAttributeWithParams(10, "cvpOperation", "bigString?", -1, false, false, true, false, false, false, "\"cvp_operation_header\"");
            Sybase.Reflection.AttributeMetaData cvpOperationLobs_attribute = AddAttributeWithParams(11, "cvpOperationLobs", "bigString?", -1, false, false, true, false, false, false, "\"cvp_operation_lobs\"");
            Sybase.Reflection.AttributeMetaData surrogateKey_attribute = AddAttributeWithParams(12, "surrogateKey", "long", -1, false, true, false, false, false, false, "\"l\"");
            Sybase.Reflection.AttributeMetaData replayCounter_attribute = AddAttributeWithParams(20004, "replayCounter", "long", -1, false, false, true, false, false, false, "\"_rc\"");
            Sybase.Reflection.AttributeMetaData disableSubmit_attribute = AddAttributeWithParams(20003, "disableSubmit", "boolean", -1, false, false, true, false, false, false, "\"_ds\"");
            Sybase.Reflection.AttributeMetaData cvpOperationLength_attribute = AddAttributeWithParams(13, "cvpOperationLength", "long", -1, false, false, true, false, false, false, "\"cvpOperation_length\"");
            Sybase.Reflection.AttributeMetaData cvpOperationLobsLength_attribute = AddAttributeWithParams(14, "cvpOperationLobsLength", "long", -1, false, false, true, false, false, false, "\"cvpOperationLobs_length\"");
            InitAttributeMapFromAttributes();
            AddOperationWithParams(487, "findAll", "FavoriteItem*", true);
            Sybase.Reflection.OperationMetaData FindByPrimaryKey1_operation = AddOperationWithParams(489, "findByPrimaryKey", "FavoriteItem", true);
            AddParameterForOperation(FindByPrimaryKey1_operation, "fav_item_id", "string");
            Sybase.Reflection.OperationMetaData GetByPrimaryKey2_operation = AddOperationWithParams(492, "getByPrimaryKey", "Sybase.Persistence.QueryResultSet", true);
            AddParameterForOperation(GetByPrimaryKey2_operation, "fav_item_id", "string");
            AddOperationWithParams(495, "getAllFavoriteItems", "Sybase.Persistence.QueryResultSet", true);
            Sybase.Reflection.OperationMetaData GetFavoriteItems4_operation = AddOperationWithParams(497, "getFavoriteItems", "Sybase.Persistence.QueryResultSet", true);
            AddParameterForOperation(GetFavoriteItems4_operation, "fav_list_id", "string?");
            AddOperationWithParams(502, "create", "void", false, 'C');
            AddOperationWithParams(518, "update", "void", false, 'U');
            AddOperationWithParams(540, "delete", "void", false, 'D');
            AddOperationWithParams(2616, "getPendingObjects", "FavoriteItem*", true);
            Sybase.Reflection.OperationMetaData GetPendingObjects9_operation = AddOperationWithParams(2618, "getPendingObjects", "FavoriteItem*", true);
            AddParameterForOperation(GetPendingObjects9_operation, "pendingChange", "char");
            AddOperationWithParams(2621, "getReplayPendingObjects", "FavoriteItem*", true);
            AddOperationWithParams(4105, "isNew", "boolean", false);
            AddOperationWithParams(4106, "isDirty", "boolean", false);
            AddOperationWithParams(4107, "isDeleted", "boolean", false);
            AddOperationWithParams(4108, "refresh", "void", false);
            AddOperationWithParams(4109, "_pk", "long?", false);
            AddOperationWithParams(4110, "isPending", "boolean", false);
            AddOperationWithParams(4111, "isCreated", "boolean", false);
            AddOperationWithParams(4112, "isUpdated", "boolean", false);
            AddOperationWithParams(4113, "submitPending", "void", false);
            AddOperationWithParams(4114, "cancelPending", "void", false);
            AddOperationWithParams(4115, "submitPendingOperations", "void", true);
            AddOperationWithParams(4116, "cancelPendingOperations", "void", true);
            AddOperationWithParams(4117, "save", "void", false);
            Sybase.Reflection.OperationMetaData FindWithQuery24_operation = AddOperationWithParams(4118, "findWithQuery", "FavoriteItem*", true);
            AddParameterForOperation(FindWithQuery24_operation, "query", "Sybase.Persistence.Query");
            Sybase.Reflection.OperationMetaData GetSize25_operation = AddOperationWithParams(4120, "getSize", "int", true);
            AddParameterForOperation(GetSize25_operation, "query", "Sybase.Persistence.Query");
            InitOperationMapFromOperations();
            SetName("FavoriteItem");
            SetTable("\"graybarmobility_1_0_favoriteitem\"");
            SetSynchronizationGroup("FavoriteSyncGroup");
        }
    }
}