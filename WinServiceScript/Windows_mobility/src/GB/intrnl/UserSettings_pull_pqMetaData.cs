
namespace GB.intrnl
{
    public class UserSettings_pull_pqMetaData
        : Sybase.Reflection.EntityMetaData
    {
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public UserSettings_pull_pqMetaData()
        {
            _init();
        }
        
        protected void _init()
        {
            SetId(38);
            SetAttributes(new Sybase.Reflection.AttributeMetaDataList());
            SetOperations(new Sybase.Reflection.OperationMetaDataList());
            SetAttributeMap(new Sybase.Reflection.AttributeMap());
            SetOperationMap(new Sybase.Reflection.OperationMap());
            Sybase.Reflection.AttributeMetaData username_attribute = AddAttributeWithParams(0, "username", "string?", 300, false, false, true, false, false, false, "\"username\"");
            Sybase.Reflection.AttributeMetaData remoteId_attribute = AddAttributeWithParams(1, "remoteId", "string?", 300, false, false, true, false, false, false, "\"remoteId\"");
            Sybase.Reflection.AttributeMetaData user_idParam_attribute = AddAttributeWithParams(2, "user_idParam", "string?", 100, false, false, true, false, false, false, "\"user_idParam\"");
            Sybase.Reflection.AttributeMetaData id_attribute = AddAttributeWithParams(3, "id", "long", -1, false, true, true, false, false, false, "\"id\"");
            InitAttributeMapFromAttributes();
            AddOperationWithParams(2148, "findAll", "UserSettings_pull_pq*", true);
            Sybase.Reflection.OperationMetaData FindSub1_operation = AddOperationWithParams(2150, "findSub", "UserSettings_pull_pq?", true);
            AddParameterForOperation(FindSub1_operation, "_username", "string?");
            AddParameterForOperation(FindSub1_operation, "user_id", "string?");
            AddOperationWithParams(4634, "isNew", "boolean", false);
            AddOperationWithParams(4635, "isDirty", "boolean", false);
            AddOperationWithParams(4636, "isDeleted", "boolean", false);
            AddOperationWithParams(4637, "refresh", "void", false);
            AddOperationWithParams(4638, "_pk", "long?", false);
            AddOperationWithParams(4639, "save", "void", false);
            InitOperationMapFromOperations();
            SetName("UserSettings_pull_pq");
            SetTable("\"graybarmobility_1_0_usersettings_pull_pq\"");
            SetSynchronizationGroup("unsubscribe");
        }
    }
}