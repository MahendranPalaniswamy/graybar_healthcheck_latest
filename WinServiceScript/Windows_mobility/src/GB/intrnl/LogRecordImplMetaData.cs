
namespace GB.intrnl
{
    public class LogRecordImplMetaData
        : Sybase.Reflection.EntityMetaData
    {
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public LogRecordImplMetaData()
        {
            _init();
        }
        
        protected void _init()
        {
            SetId(13);
            SetAttributes(new Sybase.Reflection.AttributeMetaDataList());
            SetOperations(new Sybase.Reflection.OperationMetaDataList());
            SetAttributeMap(new Sybase.Reflection.AttributeMap());
            SetOperationMap(new Sybase.Reflection.OperationMap());
            Sybase.Reflection.AttributeMetaData level_attribute = AddAttributeWithParams(0, "level", "int", -1, false, false, false, false, false, false, "\"e\"");
            Sybase.Reflection.AttributeMetaData code_attribute = AddAttributeWithParams(1, "code", "int", -1, false, false, false, false, false, false, "\"f\"");
            Sybase.Reflection.AttributeMetaData eisCode_attribute = AddAttributeWithParams(2, "eisCode", "string?", 10, false, false, false, false, false, false, "\"g\"");
            Sybase.Reflection.AttributeMetaData message_attribute = AddAttributeWithParams(3, "message", "string?", 2000, false, false, false, false, false, false, "\"h\"");
            Sybase.Reflection.AttributeMetaData component_attribute = AddAttributeWithParams(4, "component", "string?", 200, false, false, false, false, false, false, "\"i\"");
            Sybase.Reflection.AttributeMetaData entityKey_attribute = AddAttributeWithParams(5, "entityKey", "string?", 256, false, false, false, false, false, false, "\"j\"");
            Sybase.Reflection.AttributeMetaData operation_attribute = AddAttributeWithParams(6, "operation", "string?", 100, false, false, false, false, false, false, "\"l\"");
            Sybase.Reflection.AttributeMetaData requestId_attribute = AddAttributeWithParams(7, "requestId", "string?", 100, false, false, false, false, false, false, "\"m\"");
            Sybase.Reflection.AttributeMetaData timestamp_attribute = AddAttributeWithParams(8, "timestamp", "dateTime?", -1, false, false, false, false, false, false, "\"n\"");
            Sybase.Reflection.AttributeMetaData pending_attribute = AddAttributeWithParams(20001, "pending", "boolean", -1, false, false, true, false, false, false, "\"pending\"");
            Sybase.Reflection.AttributeMetaData pendingChange_attribute = AddAttributeWithParams(20002, "pendingChange", "char", 1, false, false, true, false, false, false, "\"_pc\"");
            Sybase.Reflection.AttributeMetaData replayPending_attribute = AddAttributeWithParams(20005, "replayPending", "long", -1, false, false, true, false, false, false, "\"_rp\"");
            Sybase.Reflection.AttributeMetaData replayFailure_attribute = AddAttributeWithParams(20006, "replayFailure", "long", -1, false, false, true, false, false, false, "\"_rf\"");
            Sybase.Reflection.AttributeMetaData messageId_attribute = AddAttributeWithParams(9, "messageId", "long", -1, false, true, false, false, false, false, "\"d\"");
            Sybase.Reflection.AttributeMetaData replayCounter_attribute = AddAttributeWithParams(20004, "replayCounter", "long", -1, false, false, true, false, false, false, "\"_rc\"");
            Sybase.Reflection.AttributeMetaData disableSubmit_attribute = AddAttributeWithParams(20003, "disableSubmit", "boolean", -1, false, false, true, false, false, false, "\"_ds\"");
            InitAttributeMapFromAttributes();
            AddOperationWithParams(1533, "create", "void", false, 'C');
            AddOperationWithParams(1534, "update", "void", false, 'U');
            AddOperationWithParams(1535, "delete", "void", false, 'D');
            AddOperationWithParams(3288, "getPendingObjects", "LogRecordImpl*", true);
            Sybase.Reflection.OperationMetaData GetPendingObjects4_operation = AddOperationWithParams(3290, "getPendingObjects", "LogRecordImpl*", true);
            AddParameterForOperation(GetPendingObjects4_operation, "pendingChange", "char");
            AddOperationWithParams(3293, "getReplayPendingObjects", "LogRecordImpl*", true);
            AddOperationWithParams(4405, "isNew", "boolean", false);
            AddOperationWithParams(4406, "isDirty", "boolean", false);
            AddOperationWithParams(4407, "isDeleted", "boolean", false);
            AddOperationWithParams(4408, "refresh", "void", false);
            AddOperationWithParams(4409, "_pk", "long?", false);
            AddOperationWithParams(4410, "isPending", "boolean", false);
            AddOperationWithParams(4411, "isCreated", "boolean", false);
            AddOperationWithParams(4412, "isUpdated", "boolean", false);
            AddOperationWithParams(4413, "submitPending", "void", false);
            AddOperationWithParams(4414, "cancelPending", "void", false);
            AddOperationWithParams(4415, "submitPendingOperations", "void", true);
            AddOperationWithParams(4416, "cancelPendingOperations", "void", true);
            AddOperationWithParams(4417, "save", "void", false);
            Sybase.Reflection.OperationMetaData FindWithQuery19_operation = AddOperationWithParams(4418, "findWithQuery", "LogRecordImpl*", true);
            AddParameterForOperation(FindWithQuery19_operation, "query", "Sybase.Persistence.Query");
            Sybase.Reflection.OperationMetaData GetSize20_operation = AddOperationWithParams(4420, "getSize", "int", true);
            AddParameterForOperation(GetSize20_operation, "query", "Sybase.Persistence.Query");
            InitOperationMapFromOperations();
            SetName("LogRecordImpl");
            SetTable("\"graybarmobility_1_0_logrecordimpl\"");
            SetSynchronizationGroup("system");
        }
    }
}