
namespace GB.intrnl
{
    public class Order_Delivery_HeaderMetaData
        : Sybase.Reflection.EntityMetaData
    {
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public Order_Delivery_HeaderMetaData()
        {
            _init();
        }
        
        protected void _init()
        {
            SetId(19);
            SetAttributes(new Sybase.Reflection.AttributeMetaDataList());
            SetOperations(new Sybase.Reflection.OperationMetaDataList());
            SetAttributeMap(new Sybase.Reflection.AttributeMap());
            SetOperationMap(new Sybase.Reflection.OperationMap());
            Sybase.Reflection.AttributeMetaData SOLD_TO_attribute = AddAttributeWithParams(0, "SOLD_TO", "string", 10, false, false, false, false, false, false, "\"a\"");
            Sybase.Reflection.AttributeMetaData ORDER_NUMBER_attribute = AddAttributeWithParams(1, "ORDER_NUMBER", "string", 10, false, false, false, false, false, false, "\"b\"");
            Sybase.Reflection.AttributeMetaData MOBILE_ORDER_NUMBER_attribute = AddAttributeWithParams(2, "MOBILE_ORDER_NUMBER", "string", 20, false, false, false, false, false, false, "\"c\"");
            Sybase.Reflection.AttributeMetaData DELIVERY_NUMBER_attribute = AddAttributeWithParams(3, "DELIVERY_NUMBER", "string", 10, false, false, false, false, false, false, "\"d\"");
            Sybase.Reflection.AttributeMetaData DELIVERY_STATUS_attribute = AddAttributeWithParams(4, "DELIVERY_STATUS", "string", 1, false, false, false, false, false, false, "\"e\"");
            Sybase.Reflection.AttributeMetaData DELIVERY_STATUS_TEXT_attribute = AddAttributeWithParams(5, "DELIVERY_STATUS_TEXT", "string", 25, false, false, false, false, false, false, "\"f\"");
            Sybase.Reflection.AttributeMetaData DELIVERY_STATUS_DATE_attribute = AddAttributeWithParams(6, "DELIVERY_STATUS_DATE", "date?", -1, false, false, false, false, false, false, "\"g\"");
            Sybase.Reflection.AttributeMetaData SHIPPING_METHOD_attribute = AddAttributeWithParams(7, "SHIPPING_METHOD", "string", 1024, false, false, false, false, false, false, "\"h\"");
            Sybase.Reflection.AttributeMetaData IS_PICKUP_attribute = AddAttributeWithParams(8, "IS_PICKUP", "string", 1, false, false, false, false, false, false, "\"i\"");
            Sybase.Reflection.AttributeMetaData SIGNED_FOR_BY_attribute = AddAttributeWithParams(9, "SIGNED_FOR_BY", "string", 50, false, false, false, false, false, false, "\"j\"");
            Sybase.Reflection.AttributeMetaData NAME1_attribute = AddAttributeWithParams(10, "NAME1", "string", 40, false, false, false, false, false, false, "\"l\"");
            Sybase.Reflection.AttributeMetaData NAME2_attribute = AddAttributeWithParams(11, "NAME2", "string", 40, false, false, false, false, false, false, "\"m\"");
            Sybase.Reflection.AttributeMetaData STREET1_attribute = AddAttributeWithParams(12, "STREET1", "string", 60, false, false, false, false, false, false, "\"n\"");
            Sybase.Reflection.AttributeMetaData STREET2_attribute = AddAttributeWithParams(13, "STREET2", "string", 40, false, false, false, false, false, false, "\"o\"");
            Sybase.Reflection.AttributeMetaData CITY_attribute = AddAttributeWithParams(14, "CITY", "string", 40, false, false, false, false, false, false, "\"p\"");
            Sybase.Reflection.AttributeMetaData STATE_attribute = AddAttributeWithParams(15, "STATE", "string", 3, false, false, false, false, false, false, "\"q\"");
            Sybase.Reflection.AttributeMetaData ZIPCODE_attribute = AddAttributeWithParams(16, "ZIPCODE", "string", 10, false, false, false, false, false, false, "\"r\"");
            Sybase.Reflection.AttributeMetaData COUNTRY_attribute = AddAttributeWithParams(17, "COUNTRY", "string", 3, false, false, false, false, false, false, "\"s\"");
            Sybase.Reflection.AttributeMetaData orderHeader_attribute = AddAttributeWithParams(18, "orderHeader", "GB.Order_Header", -1, false, false, false, true, false, false);
            Sybase.Reflection.AttributeMetaData orderDeliveryLineItems_attribute = AddAttributeWithParams(19, "orderDeliveryLineItems", "GB.Order_Delivery_Line_Item*", -1, false, false, false, true, false, false , true);
            Sybase.Reflection.AttributeMetaData pending_attribute = AddAttributeWithParams(20001, "pending", "boolean", -1, false, false, true, false, false, false, "\"pending\"");
            Sybase.Reflection.AttributeMetaData pendingChange_attribute = AddAttributeWithParams(20002, "pendingChange", "char", 1, false, false, true, false, false, false, "\"_pc\"");
            Sybase.Reflection.AttributeMetaData replayPending_attribute = AddAttributeWithParams(20005, "replayPending", "long", -1, false, false, true, false, false, false, "\"_rp\"");
            Sybase.Reflection.AttributeMetaData replayFailure_attribute = AddAttributeWithParams(20006, "replayFailure", "long", -1, false, false, true, false, false, false, "\"_rf\"");
            Sybase.Reflection.AttributeMetaData cvpOperation_attribute = AddAttributeWithParams(20, "cvpOperation", "bigString?", -1, false, false, true, false, false, false, "\"cvp_operation_header\"");
            Sybase.Reflection.AttributeMetaData cvpOperationLobs_attribute = AddAttributeWithParams(21, "cvpOperationLobs", "bigString?", -1, false, false, true, false, false, false, "\"cvp_operation_lobs\"");
            Sybase.Reflection.AttributeMetaData orderHeaderFK_attribute = AddAttributeWithParams(22, "orderHeaderFK", "long?", -1, false, false, true, false, false, false, "\"orderHeaderFK\"");
            Sybase.Reflection.AttributeMetaData surrogateKey_attribute = AddAttributeWithParams(23, "surrogateKey", "long", -1, false, true, false, false, false, false, "\"t\"");
            Sybase.Reflection.AttributeMetaData replayCounter_attribute = AddAttributeWithParams(20004, "replayCounter", "long", -1, false, false, true, false, false, false, "\"_rc\"");
            Sybase.Reflection.AttributeMetaData disableSubmit_attribute = AddAttributeWithParams(20003, "disableSubmit", "boolean", -1, false, false, true, false, false, false, "\"_ds\"");
            Sybase.Reflection.AttributeMetaData cvpOperationLength_attribute = AddAttributeWithParams(24, "cvpOperationLength", "long", -1, false, false, true, false, false, false, "\"cvpOperation_length\"");
            Sybase.Reflection.AttributeMetaData cvpOperationLobsLength_attribute = AddAttributeWithParams(25, "cvpOperationLobsLength", "long", -1, false, false, true, false, false, false, "\"cvpOperationLobs_length\"");
            InitAttributeMapFromAttributes();
            AddOperationWithParams(1420, "findAll", "Order_Delivery_Header*", true);
            Sybase.Reflection.OperationMetaData FindByORDER_NUMBER1_operation = AddOperationWithParams(1422, "findByORDER_NUMBER", "Order_Delivery_Header*", true);
            AddParameterForOperation(FindByORDER_NUMBER1_operation, "ORDER_NUMBER", "string");
            Sybase.Reflection.OperationMetaData FindByMOBILE_ORDER_NUMBER2_operation = AddOperationWithParams(1425, "findByMOBILE_ORDER_NUMBER", "Order_Delivery_Header*", true);
            AddParameterForOperation(FindByMOBILE_ORDER_NUMBER2_operation, "MOBILE_ORDER_NUMBER", "string");
            Sybase.Reflection.OperationMetaData FindByDELIVERY_NUMBER3_operation = AddOperationWithParams(1428, "findByDELIVERY_NUMBER", "Order_Delivery_Header*", true);
            AddParameterForOperation(FindByDELIVERY_NUMBER3_operation, "DELIVERY_NUMBER", "string");
            Sybase.Reflection.OperationMetaData FindByPrimaryKey4_operation = AddOperationWithParams(1431, "findByPrimaryKey", "Order_Delivery_Header", true);
            AddParameterForOperation(FindByPrimaryKey4_operation, "ORDER_NUMBER", "string");
            AddParameterForOperation(FindByPrimaryKey4_operation, "MOBILE_ORDER_NUMBER", "string");
            AddParameterForOperation(FindByPrimaryKey4_operation, "DELIVERY_NUMBER", "string");
            Sybase.Reflection.OperationMetaData GetDeliveryHeaderForOrder5_operation = AddOperationWithParams(1436, "getDeliveryHeaderForOrder", "Sybase.Persistence.QueryResultSet", true);
            AddParameterForOperation(GetDeliveryHeaderForOrder5_operation, "order_number", "string");
            AddParameterForOperation(GetDeliveryHeaderForOrder5_operation, "mobile_order_number", "string");
            Sybase.Reflection.OperationMetaData GetOrderDeliveryHeaders_for_Order_Header6_operation = AddOperationWithParams(2824, "getOrderDeliveryHeaders_for_Order_Header", "Order_Delivery_Header*", true);
            AddParameterForOperation(GetOrderDeliveryHeaders_for_Order_Header6_operation, "surrogateKey", "long?");
            Sybase.Reflection.OperationMetaData DeleteOSOrderDeliveryHeaders_for_Order_Header7_operation = AddOperationWithParams(2827, "deleteOSOrderDeliveryHeaders_for_Order_Header", "void", true);
            AddParameterForOperation(DeleteOSOrderDeliveryHeaders_for_Order_Header7_operation, "surrogateKey", "long?");
            Sybase.Reflection.OperationMetaData DeleteOrderDeliveryHeaders_for_Order_Header8_operation = AddOperationWithParams(2829, "deleteOrderDeliveryHeaders_for_Order_Header", "void", true);
            AddParameterForOperation(DeleteOrderDeliveryHeaders_for_Order_Header8_operation, "surrogateKey", "long?");
            AddOperationWithParams(3178, "getPendingObjects", "Order_Delivery_Header*", true);
            Sybase.Reflection.OperationMetaData GetPendingObjects10_operation = AddOperationWithParams(3180, "getPendingObjects", "Order_Delivery_Header*", true);
            AddParameterForOperation(GetPendingObjects10_operation, "pendingChange", "char");
            AddOperationWithParams(3183, "getReplayPendingObjects", "Order_Delivery_Header*", true);
            AddOperationWithParams(4357, "isNew", "boolean", false);
            AddOperationWithParams(4358, "isDirty", "boolean", false);
            AddOperationWithParams(4359, "isDeleted", "boolean", false);
            AddOperationWithParams(4360, "refresh", "void", false);
            AddOperationWithParams(4361, "_pk", "long?", false);
            AddOperationWithParams(4362, "isPending", "boolean", false);
            AddOperationWithParams(4363, "isCreated", "boolean", false);
            AddOperationWithParams(4364, "isUpdated", "boolean", false);
            AddOperationWithParams(4365, "submitPending", "void", false);
            AddOperationWithParams(4366, "cancelPending", "void", false);
            AddOperationWithParams(4367, "submitPendingOperations", "void", true);
            AddOperationWithParams(4368, "cancelPendingOperations", "void", true);
            AddOperationWithParams(4369, "save", "void", false);
            Sybase.Reflection.OperationMetaData FindWithQuery25_operation = AddOperationWithParams(4370, "findWithQuery", "Order_Delivery_Header*", true);
            AddParameterForOperation(FindWithQuery25_operation, "query", "Sybase.Persistence.Query");
            Sybase.Reflection.OperationMetaData GetSize26_operation = AddOperationWithParams(4372, "getSize", "int", true);
            AddParameterForOperation(GetSize26_operation, "query", "Sybase.Persistence.Query");
            Sybase.Reflection.OperationMetaData GetOrderDeliveryLineItemsFilterBy27_operation = AddOperationWithParams(4375, "GetOrderDeliveryLineItemsFilterBy", "Order_Delivery_Line_Item*", false);
            AddParameterForOperation(GetOrderDeliveryLineItemsFilterBy27_operation, "query", "Sybase.Persistence.Query");
            Sybase.Reflection.OperationMetaData GetOrderDeliveryLineItemsSize28_operation = AddOperationWithParams(4377, "GetOrderDeliveryLineItemsSize", "int", false);
            AddParameterForOperation(GetOrderDeliveryLineItemsSize28_operation, "query", "Sybase.Persistence.Query");
            InitOperationMapFromOperations();
            SetName("Order_Delivery_Header");
            SetTable("\"graybarmobility_1_0_order_delivery_header\"");
            SetSynchronizationGroup("OrderRelatedSyncGroup");
        }
    }
}