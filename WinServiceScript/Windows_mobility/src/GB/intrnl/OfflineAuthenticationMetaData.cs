
namespace GB.intrnl
{
    public class OfflineAuthenticationMetaData
        : Sybase.Reflection.EntityMetaData
    {
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public OfflineAuthenticationMetaData()
        {
            _init();
        }
        
        protected void _init()
        {
            SetId(42);
            SetAttributes(new Sybase.Reflection.AttributeMetaDataList());
            SetOperations(new Sybase.Reflection.OperationMetaDataList());
            SetAttributeMap(new Sybase.Reflection.AttributeMap());
            SetOperationMap(new Sybase.Reflection.OperationMap());
            Sybase.Reflection.AttributeMetaData passwordHash_attribute = AddAttributeWithParams(0, "passwordHash", "int", -1, false, false, false, false, false, false, "\"b\"");
            Sybase.Reflection.AttributeMetaData user_attribute = AddAttributeWithParams(1, "user", "string", 300, false, true, false, false, false, false, "\"a\"");
            InitAttributeMapFromAttributes();
            Sybase.Reflection.OperationMetaData Login0_operation = AddOperationWithParams(1673, "login", "boolean", true);
            AddParameterForOperation(Login0_operation, "user", "string");
            AddParameterForOperation(Login0_operation, "password", "string");
            Sybase.Reflection.OperationMetaData Store1_operation = AddOperationWithParams(1678, "store", "void", true);
            AddParameterForOperation(Store1_operation, "user", "string");
            AddParameterForOperation(Store1_operation, "password", "string");
            AddOperationWithParams(4466, "isNew", "boolean", false);
            AddOperationWithParams(4467, "isDirty", "boolean", false);
            AddOperationWithParams(4468, "isDeleted", "boolean", false);
            AddOperationWithParams(4469, "refresh", "void", false);
            AddOperationWithParams(4470, "_pk", "string?", false);
            AddOperationWithParams(4471, "save", "void", false);
            InitOperationMapFromOperations();
            SetName("OfflineAuthentication");
            SetTable("\"co_graybarmobility_1_0_offlineauthentication\"");
            SetSynchronizationGroup("");
        }
    }
}