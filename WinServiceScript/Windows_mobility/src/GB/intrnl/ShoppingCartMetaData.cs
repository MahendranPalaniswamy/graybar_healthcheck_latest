
namespace GB.intrnl
{
    public class ShoppingCartMetaData
        : Sybase.Reflection.EntityMetaData
    {
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public ShoppingCartMetaData()
        {
            _init();
        }
        
        protected void _init()
        {
            SetId(41);
            SetAttributes(new Sybase.Reflection.AttributeMetaDataList());
            SetOperations(new Sybase.Reflection.OperationMetaDataList());
            SetAttributeMap(new Sybase.Reflection.AttributeMap());
            SetOperationMap(new Sybase.Reflection.OperationMap());
            Sybase.Reflection.AttributeMetaData catalog_number_attribute = AddAttributeWithParams(0, "catalog_number", "string?", 40, false, false, false, false, false, false, "\"b\"");
            Sybase.Reflection.AttributeMetaData manufacturer_name_attribute = AddAttributeWithParams(1, "manufacturer_name", "string?", 35, false, false, false, false, false, false, "\"c\"");
            Sybase.Reflection.AttributeMetaData description_attribute = AddAttributeWithParams(2, "description", "string?", 1024, false, false, false, false, false, false, "\"d\"");
            Sybase.Reflection.AttributeMetaData image_url_attribute = AddAttributeWithParams(3, "image_url", "string?", 1024, false, false, false, false, false, false, "\"e\"");
            Sybase.Reflection.AttributeMetaData pickup_quantity_attribute = AddAttributeWithParams(4, "pickup_quantity", "string?", 20, false, false, false, false, false, false, "\"f\"");
            Sybase.Reflection.AttributeMetaData shipping_quantity_attribute = AddAttributeWithParams(5, "shipping_quantity", "string?", 20, false, false, false, false, false, false, "\"g\"");
            Sybase.Reflection.AttributeMetaData price_attribute = AddAttributeWithParams(6, "price", "string?", 200, false, false, false, false, false, false, "\"h\"");
            Sybase.Reflection.AttributeMetaData per_value_attribute = AddAttributeWithParams(7, "per_value", "string?", 10, false, false, false, false, false, false, "\"i\"");
            Sybase.Reflection.AttributeMetaData total_attribute = AddAttributeWithParams(8, "total", "string?", 200, false, false, false, false, false, false, "\"j\"");
            Sybase.Reflection.AttributeMetaData last_price_check_attribute = AddAttributeWithParams(9, "last_price_check", "string?", 100, false, false, false, false, false, false, "\"l\"");
            Sybase.Reflection.AttributeMetaData last_avail_check_attribute = AddAttributeWithParams(10, "last_avail_check", "string?", 100, false, false, false, false, false, false, "\"m\"");
            Sybase.Reflection.AttributeMetaData unit_attribute = AddAttributeWithParams(11, "unit", "string?", 20, false, false, false, false, false, false, "\"n\"");
            Sybase.Reflection.AttributeMetaData pickup_available_attribute = AddAttributeWithParams(12, "pickup_available", "string?", 100, false, false, false, false, false, false, "\"o\"");
            Sybase.Reflection.AttributeMetaData shipping_available_attribute = AddAttributeWithParams(13, "shipping_available", "string?", 100, false, false, false, false, false, false, "\"p\"");
            Sybase.Reflection.AttributeMetaData product_id_attribute = AddAttributeWithParams(14, "product_id", "string", 18, false, true, false, false, false, false, "\"a\"");
            InitAttributeMapFromAttributes();
            AddOperationWithParams(1205, "findAll", "ShoppingCart*", true);
            Sybase.Reflection.OperationMetaData FindByPrimaryKey1_operation = AddOperationWithParams(1207, "findByPrimaryKey", "ShoppingCart", true);
            AddParameterForOperation(FindByPrimaryKey1_operation, "product_id", "string");
            AddOperationWithParams(1210, "getShoppingCartItems", "Sybase.Persistence.QueryResultSet", true);
            AddOperationWithParams(1213, "create", "void", false, 'C');
            AddOperationWithParams(4280, "isNew", "boolean", false);
            AddOperationWithParams(4281, "isDirty", "boolean", false);
            AddOperationWithParams(4282, "isDeleted", "boolean", false);
            AddOperationWithParams(4283, "refresh", "void", false);
            AddOperationWithParams(4284, "_pk", "string?", false);
            AddOperationWithParams(4285, "save", "void", false);
            Sybase.Reflection.OperationMetaData FindWithQuery10_operation = AddOperationWithParams(4286, "findWithQuery", "ShoppingCart*", true);
            AddParameterForOperation(FindWithQuery10_operation, "query", "Sybase.Persistence.Query");
            Sybase.Reflection.OperationMetaData GetSize11_operation = AddOperationWithParams(4288, "getSize", "int", true);
            AddParameterForOperation(GetSize11_operation, "query", "Sybase.Persistence.Query");
            InitOperationMapFromOperations();
            SetName("ShoppingCart");
            SetTable("\"co_graybarmobility_1_0_shoppingcart\"");
            SetSynchronizationGroup("");
        }
    }
}