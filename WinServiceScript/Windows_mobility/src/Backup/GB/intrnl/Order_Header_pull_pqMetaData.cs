
namespace GB.intrnl
{
    public class Order_Header_pull_pqMetaData
        : Sybase.Reflection.EntityMetaData
    {
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public Order_Header_pull_pqMetaData()
        {
            _init();
        }
        
        protected void _init()
        {
            SetId(22);
            SetAttributes(new Sybase.Reflection.AttributeMetaDataList());
            SetOperations(new Sybase.Reflection.OperationMetaDataList());
            SetAttributeMap(new Sybase.Reflection.AttributeMap());
            SetOperationMap(new Sybase.Reflection.OperationMap());
            Sybase.Reflection.AttributeMetaData username_attribute = AddAttributeWithParams(0, "username", "string?", 300, false, false, true, false, false, false, "\"username\"");
            Sybase.Reflection.AttributeMetaData remoteId_attribute = AddAttributeWithParams(1, "remoteId", "string?", 300, false, false, true, false, false, false, "\"remoteId\"");
            Sybase.Reflection.AttributeMetaData sold_toParam_attribute = AddAttributeWithParams(2, "sold_toParam", "string?", 10, false, false, true, false, false, false, "\"sold_toParam\"");
            Sybase.Reflection.AttributeMetaData id_attribute = AddAttributeWithParams(3, "id", "long", -1, false, true, true, false, false, false, "\"id\"");
            InitAttributeMapFromAttributes();
            AddOperationWithParams(1953, "findAll", "Order_Header_pull_pq*", true);
            Sybase.Reflection.OperationMetaData FindSub1_operation = AddOperationWithParams(1955, "findSub", "Order_Header_pull_pq?", true);
            AddParameterForOperation(FindSub1_operation, "_username", "string?");
            AddParameterForOperation(FindSub1_operation, "sold_to", "string?");
            AddOperationWithParams(4564, "isNew", "boolean", false);
            AddOperationWithParams(4565, "isDirty", "boolean", false);
            AddOperationWithParams(4566, "isDeleted", "boolean", false);
            AddOperationWithParams(4567, "refresh", "void", false);
            AddOperationWithParams(4568, "_pk", "long?", false);
            AddOperationWithParams(4569, "save", "void", false);
            InitOperationMapFromOperations();
            SetName("Order_Header_pull_pq");
            SetTable("\"graybarmobility_1_0_order_header_pull_pq\"");
            SetSynchronizationGroup("unsubscribe");
        }
    }
}