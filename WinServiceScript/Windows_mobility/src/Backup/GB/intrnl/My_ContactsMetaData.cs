
namespace GB.intrnl
{
    public class My_ContactsMetaData
        : Sybase.Reflection.EntityMetaData
    {
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public My_ContactsMetaData()
        {
            _init();
        }
        
        protected void _init()
        {
            SetId(16);
            SetAttributes(new Sybase.Reflection.AttributeMetaDataList());
            SetOperations(new Sybase.Reflection.OperationMetaDataList());
            SetAttributeMap(new Sybase.Reflection.AttributeMap());
            SetOperationMap(new Sybase.Reflection.OperationMap());
            Sybase.Reflection.AttributeMetaData SOLD_TO_attribute = AddAttributeWithParams(0, "SOLD_TO", "string", 10, false, false, false, false, false, false, "\"a\"");
            Sybase.Reflection.AttributeMetaData PARTNER_TYPE_attribute = AddAttributeWithParams(1, "PARTNER_TYPE", "string", 2, false, false, false, false, false, false, "\"b\"");
            Sybase.Reflection.AttributeMetaData NAME_attribute = AddAttributeWithParams(2, "NAME", "string", 40, false, false, false, false, false, false, "\"c\"");
            Sybase.Reflection.AttributeMetaData TITLE_attribute = AddAttributeWithParams(3, "TITLE", "string", 40, false, false, false, false, false, false, "\"d\"");
            Sybase.Reflection.AttributeMetaData WORK_PHONE_attribute = AddAttributeWithParams(4, "WORK_PHONE", "string", 30, false, false, false, false, false, false, "\"e\"");
            Sybase.Reflection.AttributeMetaData EMAIL_attribute = AddAttributeWithParams(5, "EMAIL", "string", 241, false, false, false, false, false, false, "\"f\"");
            Sybase.Reflection.AttributeMetaData surrogateKey_attribute = AddAttributeWithParams(6, "surrogateKey", "long", -1, false, true, false, false, false, false, "\"g\"");
            InitAttributeMapFromAttributes();
            AddOperationWithParams(726, "findAll", "My_Contacts*", true);
            Sybase.Reflection.OperationMetaData FindBySOLD_TO1_operation = AddOperationWithParams(728, "findBySOLD_TO", "My_Contacts*", true);
            AddParameterForOperation(FindBySOLD_TO1_operation, "SOLD_TO", "string");
            Sybase.Reflection.OperationMetaData FindByPARTNER_TYPE2_operation = AddOperationWithParams(731, "findByPARTNER_TYPE", "My_Contacts*", true);
            AddParameterForOperation(FindByPARTNER_TYPE2_operation, "PARTNER_TYPE", "string");
            Sybase.Reflection.OperationMetaData FindByPrimaryKey3_operation = AddOperationWithParams(734, "findByPrimaryKey", "My_Contacts", true);
            AddParameterForOperation(FindByPrimaryKey3_operation, "SOLD_TO", "string");
            AddParameterForOperation(FindByPrimaryKey3_operation, "PARTNER_TYPE", "string");
            AddOperationWithParams(738, "getAllMyContacts", "Sybase.Persistence.QueryResultSet", true);
            AddOperationWithParams(4167, "isNew", "boolean", false);
            AddOperationWithParams(4168, "isDirty", "boolean", false);
            AddOperationWithParams(4169, "isDeleted", "boolean", false);
            AddOperationWithParams(4170, "refresh", "void", false);
            AddOperationWithParams(4171, "_pk", "long?", false);
            Sybase.Reflection.OperationMetaData FindWithQuery10_operation = AddOperationWithParams(4172, "findWithQuery", "My_Contacts*", true);
            AddParameterForOperation(FindWithQuery10_operation, "query", "Sybase.Persistence.Query");
            Sybase.Reflection.OperationMetaData GetSize11_operation = AddOperationWithParams(4174, "getSize", "int", true);
            AddParameterForOperation(GetSize11_operation, "query", "Sybase.Persistence.Query");
            InitOperationMapFromOperations();
            SetName("My_Contacts");
            SetTable("\"graybarmobility_1_0_my_contacts\"");
            SetSynchronizationGroup("ReferenceSyncGroup");
        }
    }
}