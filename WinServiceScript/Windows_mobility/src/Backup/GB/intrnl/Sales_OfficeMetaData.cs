
namespace GB.intrnl
{
    public class Sales_OfficeMetaData
        : Sybase.Reflection.EntityMetaData
    {
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public Sales_OfficeMetaData()
        {
            _init();
        }
        
        protected void _init()
        {
            SetId(30);
            SetAttributes(new Sybase.Reflection.AttributeMetaDataList());
            SetOperations(new Sybase.Reflection.OperationMetaDataList());
            SetAttributeMap(new Sybase.Reflection.AttributeMap());
            SetOperationMap(new Sybase.Reflection.OperationMap());
            Sybase.Reflection.AttributeMetaData SOLD_TO_attribute = AddAttributeWithParams(0, "SOLD_TO", "string", 10, false, false, false, false, false, false, "\"a\"");
            Sybase.Reflection.AttributeMetaData SALES_OFFICE_attribute = AddAttributeWithParams(1, "SALES_OFFICE", "string", 4, false, false, false, false, false, false, "\"b\"");
            Sybase.Reflection.AttributeMetaData NAME_attribute = AddAttributeWithParams(2, "NAME", "string", 40, false, false, false, false, false, false, "\"c\"");
            Sybase.Reflection.AttributeMetaData STREET1_attribute = AddAttributeWithParams(3, "STREET1", "string", 60, false, false, false, false, false, false, "\"d\"");
            Sybase.Reflection.AttributeMetaData STREET2_attribute = AddAttributeWithParams(4, "STREET2", "string", 60, false, false, false, false, false, false, "\"e\"");
            Sybase.Reflection.AttributeMetaData CITY_attribute = AddAttributeWithParams(5, "CITY", "string", 40, false, false, false, false, false, false, "\"f\"");
            Sybase.Reflection.AttributeMetaData STATE_attribute = AddAttributeWithParams(6, "STATE", "string", 3, false, false, false, false, false, false, "\"g\"");
            Sybase.Reflection.AttributeMetaData ZIPCODE_attribute = AddAttributeWithParams(7, "ZIPCODE", "string", 10, false, false, false, false, false, false, "\"h\"");
            Sybase.Reflection.AttributeMetaData COUNTRY_attribute = AddAttributeWithParams(8, "COUNTRY", "string", 3, false, false, false, false, false, false, "\"i\"");
            Sybase.Reflection.AttributeMetaData PHONE_attribute = AddAttributeWithParams(9, "PHONE", "string", 30, false, false, false, false, false, false, "\"j\"");
            Sybase.Reflection.AttributeMetaData surrogateKey_attribute = AddAttributeWithParams(10, "surrogateKey", "long", -1, false, true, false, false, false, false, "\"l\"");
            InitAttributeMapFromAttributes();
            AddOperationWithParams(1117, "findAll", "Sales_Office*", true);
            Sybase.Reflection.OperationMetaData FindByPrimaryKey1_operation = AddOperationWithParams(1119, "findByPrimaryKey", "Sales_Office", true);
            AddParameterForOperation(FindByPrimaryKey1_operation, "SOLD_TO", "string");
            AddOperationWithParams(1122, "getAllSalesOffices", "Sybase.Persistence.QueryResultSet", true);
            AddOperationWithParams(4260, "isNew", "boolean", false);
            AddOperationWithParams(4261, "isDirty", "boolean", false);
            AddOperationWithParams(4262, "isDeleted", "boolean", false);
            AddOperationWithParams(4263, "refresh", "void", false);
            AddOperationWithParams(4264, "_pk", "long?", false);
            Sybase.Reflection.OperationMetaData FindWithQuery8_operation = AddOperationWithParams(4265, "findWithQuery", "Sales_Office*", true);
            AddParameterForOperation(FindWithQuery8_operation, "query", "Sybase.Persistence.Query");
            Sybase.Reflection.OperationMetaData GetSize9_operation = AddOperationWithParams(4267, "getSize", "int", true);
            AddParameterForOperation(GetSize9_operation, "query", "Sybase.Persistence.Query");
            InitOperationMapFromOperations();
            SetName("Sales_Office");
            SetTable("\"graybarmobility_1_0_sales_office\"");
            SetSynchronizationGroup("ReferenceSyncGroup");
        }
    }
}