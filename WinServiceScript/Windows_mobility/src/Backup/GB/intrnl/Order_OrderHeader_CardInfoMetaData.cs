
namespace GB.intrnl
{
    public class Order_OrderHeader_CardInfoMetaData
        : Sybase.Reflection.ClassMetaData
    {
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public Order_OrderHeader_CardInfoMetaData()
        {
            _init();
        }
        
        protected void _init()
        {
            SetId(62);
            SetAttributes(new Sybase.Reflection.AttributeMetaDataList());
            SetOperations(new Sybase.Reflection.OperationMetaDataList());
            SetAttributeMap(new Sybase.Reflection.AttributeMap());
            SetOperationMap(new Sybase.Reflection.OperationMap());
            Sybase.Reflection.AttributeMetaData CardType_attribute = AddAttributeWithParams(0, "CardType", "string", 300, false, false, false, false, false, false);
            Sybase.Reflection.AttributeMetaData CardNumber_attribute = AddAttributeWithParams(1, "CardNumber", "string", 300, false, false, false, false, false, false);
            Sybase.Reflection.AttributeMetaData ExpirationDate_attribute = AddAttributeWithParams(2, "ExpirationDate", "string", 300, false, false, false, false, false, false);
            Sybase.Reflection.AttributeMetaData CCName_attribute = AddAttributeWithParams(3, "CCName", "string", 300, false, false, false, false, false, false);
            InitAttributeMapFromAttributes();
            SetName("Order_OrderHeader_CardInfo");
            
        }
        
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public  override bool IsEntity()
        {
            return false;
        }
        
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public  override bool IsService()
        {
            return false;
        }
    }
}