
namespace GB.intrnl
{
    public class ChangeLogKeyMetaData
        : Sybase.Reflection.ClassMetaData
    {
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public ChangeLogKeyMetaData()
        {
            _init();
        }
        
        protected void _init()
        {
            SetId(66);
            SetAttributes(new Sybase.Reflection.AttributeMetaDataList());
            SetOperations(new Sybase.Reflection.OperationMetaDataList());
            SetAttributeMap(new Sybase.Reflection.AttributeMap());
            SetOperationMap(new Sybase.Reflection.OperationMap());
            Sybase.Reflection.AttributeMetaData entityType_attribute = AddAttributeWithParams(0, "entityType", "int", -1, false, false, false, false, false, false);
            Sybase.Reflection.AttributeMetaData surrogateKey_attribute = AddAttributeWithParams(1, "surrogateKey", "long", -1, false, false, false, false, false, false);
            InitAttributeMapFromAttributes();
            SetName("ChangeLogKey");
            
        }
        
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public  override bool IsEntity()
        {
            return false;
        }
        
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public  override bool IsService()
        {
            return false;
        }
    }
}