
namespace GB.intrnl
{
    public class PreAuthorizeInputStructureMetaData
        : Sybase.Reflection.ClassMetaData
    {
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public PreAuthorizeInputStructureMetaData()
        {
            _init();
        }
        
        protected void _init()
        {
            SetId(59);
            SetAttributes(new Sybase.Reflection.AttributeMetaDataList());
            SetOperations(new Sybase.Reflection.OperationMetaDataList());
            SetAttributeMap(new Sybase.Reflection.AttributeMap());
            SetOperationMap(new Sybase.Reflection.OperationMap());
            Sybase.Reflection.AttributeMetaData CARD_TYPE_attribute = AddAttributeWithParams(0, "CARD_TYPE", "string", 4, false, false, false, false, false, false);
            Sybase.Reflection.AttributeMetaData CARD_NUMBER_attribute = AddAttributeWithParams(1, "CARD_NUMBER", "string", 25, false, false, false, false, false, false);
            Sybase.Reflection.AttributeMetaData EXPIRE_MONTH_attribute = AddAttributeWithParams(2, "EXPIRE_MONTH", "string", 2, false, false, false, false, false, false);
            Sybase.Reflection.AttributeMetaData EXPIRE_YEAR_attribute = AddAttributeWithParams(3, "EXPIRE_YEAR", "string", 4, false, false, false, false, false, false);
            Sybase.Reflection.AttributeMetaData NAME_ON_CARD_attribute = AddAttributeWithParams(4, "NAME_ON_CARD", "string", 40, false, false, false, false, false, false);
            Sybase.Reflection.AttributeMetaData VERIF_VALUE_attribute = AddAttributeWithParams(5, "VERIF_VALUE", "string", 6, false, false, false, false, false, false);
            Sybase.Reflection.AttributeMetaData AUTH_AMOUNT_attribute = AddAttributeWithParams(6, "AUTH_AMOUNT", "decimal", -1, false, false, false, false, false, false);
            Sybase.Reflection.AttributeMetaData AUTH_CURRENCY_attribute = AddAttributeWithParams(7, "AUTH_CURRENCY", "string", 5, false, false, false, false, false, false);
            InitAttributeMapFromAttributes();
            SetName("PreAuthorizeInputStructure");
            
        }
        
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public  override bool IsEntity()
        {
            return false;
        }
        
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public  override bool IsService()
        {
            return false;
        }
    }
}