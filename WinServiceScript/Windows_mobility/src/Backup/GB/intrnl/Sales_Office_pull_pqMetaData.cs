
namespace GB.intrnl
{
    public class Sales_Office_pull_pqMetaData
        : Sybase.Reflection.EntityMetaData
    {
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public Sales_Office_pull_pqMetaData()
        {
            _init();
        }
        
        protected void _init()
        {
            SetId(31);
            SetAttributes(new Sybase.Reflection.AttributeMetaDataList());
            SetOperations(new Sybase.Reflection.OperationMetaDataList());
            SetAttributeMap(new Sybase.Reflection.AttributeMap());
            SetOperationMap(new Sybase.Reflection.OperationMap());
            Sybase.Reflection.AttributeMetaData username_attribute = AddAttributeWithParams(0, "username", "string?", 300, false, false, true, false, false, false, "\"username\"");
            Sybase.Reflection.AttributeMetaData remoteId_attribute = AddAttributeWithParams(1, "remoteId", "string?", 300, false, false, true, false, false, false, "\"remoteId\"");
            Sybase.Reflection.AttributeMetaData sold_toParam_attribute = AddAttributeWithParams(2, "sold_toParam", "string?", 10, false, false, true, false, false, false, "\"sold_toParam\"");
            Sybase.Reflection.AttributeMetaData id_attribute = AddAttributeWithParams(3, "id", "long", -1, false, true, true, false, false, false, "\"id\"");
            InitAttributeMapFromAttributes();
            AddOperationWithParams(2031, "findAll", "Sales_Office_pull_pq*", true);
            Sybase.Reflection.OperationMetaData FindSub1_operation = AddOperationWithParams(2033, "findSub", "Sales_Office_pull_pq?", true);
            AddParameterForOperation(FindSub1_operation, "_username", "string?");
            AddParameterForOperation(FindSub1_operation, "sold_to", "string?");
            AddOperationWithParams(4592, "isNew", "boolean", false);
            AddOperationWithParams(4593, "isDirty", "boolean", false);
            AddOperationWithParams(4594, "isDeleted", "boolean", false);
            AddOperationWithParams(4595, "refresh", "void", false);
            AddOperationWithParams(4596, "_pk", "long?", false);
            AddOperationWithParams(4597, "save", "void", false);
            InitOperationMapFromOperations();
            SetName("Sales_Office_pull_pq");
            SetTable("\"graybarmobility_1_0_sales_office_pull_pq\"");
            SetSynchronizationGroup("unsubscribe");
        }
    }
}