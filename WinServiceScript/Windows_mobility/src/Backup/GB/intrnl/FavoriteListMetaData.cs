
namespace GB.intrnl
{
    public class FavoriteListMetaData
        : Sybase.Reflection.EntityMetaData
    {
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public FavoriteListMetaData()
        {
            _init();
        }
        
        protected void _init()
        {
            SetId(9);
            SetAttributes(new Sybase.Reflection.AttributeMetaDataList());
            SetOperations(new Sybase.Reflection.OperationMetaDataList());
            SetAttributeMap(new Sybase.Reflection.AttributeMap());
            SetOperationMap(new Sybase.Reflection.OperationMap());
            Sybase.Reflection.AttributeMetaData user_id_attribute = AddAttributeWithParams(0, "user_id", "string?", 100, false, false, false, false, false, false, "\"a\"");
            Sybase.Reflection.AttributeMetaData count_attribute = AddAttributeWithParams(1, "count", "int?", -1, false, false, false, false, false, false, "\"b\"");
            Sybase.Reflection.AttributeMetaData fav_list_id_attribute = AddAttributeWithParams(2, "fav_list_id", "string", 100, false, false, false, false, false, false, "\"c\"");
            Sybase.Reflection.AttributeMetaData favorite_name_attribute = AddAttributeWithParams(3, "favorite_name", "string?", 150, false, false, false, false, false, false, "\"d\"");
            Sybase.Reflection.AttributeMetaData pending_attribute = AddAttributeWithParams(20001, "pending", "boolean", -1, false, false, true, false, false, false, "\"pending\"");
            Sybase.Reflection.AttributeMetaData pendingChange_attribute = AddAttributeWithParams(20002, "pendingChange", "char", 1, false, false, true, false, false, false, "\"_pc\"");
            Sybase.Reflection.AttributeMetaData replayPending_attribute = AddAttributeWithParams(20005, "replayPending", "long", -1, false, false, true, false, false, false, "\"_rp\"");
            Sybase.Reflection.AttributeMetaData replayFailure_attribute = AddAttributeWithParams(20006, "replayFailure", "long", -1, false, false, true, false, false, false, "\"_rf\"");
            Sybase.Reflection.AttributeMetaData cvpOperation_attribute = AddAttributeWithParams(4, "cvpOperation", "bigString?", -1, false, false, true, false, false, false, "\"cvp_operation_header\"");
            Sybase.Reflection.AttributeMetaData cvpOperationLobs_attribute = AddAttributeWithParams(5, "cvpOperationLobs", "bigString?", -1, false, false, true, false, false, false, "\"cvp_operation_lobs\"");
            Sybase.Reflection.AttributeMetaData surrogateKey_attribute = AddAttributeWithParams(6, "surrogateKey", "long", -1, false, true, false, false, false, false, "\"e\"");
            Sybase.Reflection.AttributeMetaData replayCounter_attribute = AddAttributeWithParams(20004, "replayCounter", "long", -1, false, false, true, false, false, false, "\"_rc\"");
            Sybase.Reflection.AttributeMetaData disableSubmit_attribute = AddAttributeWithParams(20003, "disableSubmit", "boolean", -1, false, false, true, false, false, false, "\"_ds\"");
            Sybase.Reflection.AttributeMetaData cvpOperationLength_attribute = AddAttributeWithParams(7, "cvpOperationLength", "long", -1, false, false, true, false, false, false, "\"cvpOperation_length\"");
            Sybase.Reflection.AttributeMetaData cvpOperationLobsLength_attribute = AddAttributeWithParams(8, "cvpOperationLobsLength", "long", -1, false, false, true, false, false, false, "\"cvpOperationLobs_length\"");
            InitAttributeMapFromAttributes();
            AddOperationWithParams(585, "findAll", "FavoriteList*", true);
            Sybase.Reflection.OperationMetaData FindByPrimaryKey1_operation = AddOperationWithParams(587, "findByPrimaryKey", "FavoriteList", true);
            AddParameterForOperation(FindByPrimaryKey1_operation, "fav_list_id", "string");
            Sybase.Reflection.OperationMetaData GetByPrimaryKey2_operation = AddOperationWithParams(590, "getByPrimaryKey", "Sybase.Persistence.QueryResultSet", true);
            AddParameterForOperation(GetByPrimaryKey2_operation, "fav_list_id", "string");
            AddOperationWithParams(593, "getAllFavoriteLists", "Sybase.Persistence.QueryResultSet", true);
            AddOperationWithParams(596, "create", "void", false, 'C');
            AddOperationWithParams(607, "update", "void", false, 'U');
            AddOperationWithParams(619, "delete", "void", false, 'D');
            AddOperationWithParams(2662, "getPendingObjects", "FavoriteList*", true);
            Sybase.Reflection.OperationMetaData GetPendingObjects8_operation = AddOperationWithParams(2664, "getPendingObjects", "FavoriteList*", true);
            AddParameterForOperation(GetPendingObjects8_operation, "pendingChange", "char");
            AddOperationWithParams(2667, "getReplayPendingObjects", "FavoriteList*", true);
            AddOperationWithParams(4127, "isNew", "boolean", false);
            AddOperationWithParams(4128, "isDirty", "boolean", false);
            AddOperationWithParams(4129, "isDeleted", "boolean", false);
            AddOperationWithParams(4130, "refresh", "void", false);
            AddOperationWithParams(4131, "_pk", "long?", false);
            AddOperationWithParams(4132, "isPending", "boolean", false);
            AddOperationWithParams(4133, "isCreated", "boolean", false);
            AddOperationWithParams(4134, "isUpdated", "boolean", false);
            AddOperationWithParams(4135, "submitPending", "void", false);
            AddOperationWithParams(4136, "cancelPending", "void", false);
            AddOperationWithParams(4137, "submitPendingOperations", "void", true);
            AddOperationWithParams(4138, "cancelPendingOperations", "void", true);
            AddOperationWithParams(4139, "save", "void", false);
            Sybase.Reflection.OperationMetaData FindWithQuery23_operation = AddOperationWithParams(4140, "findWithQuery", "FavoriteList*", true);
            AddParameterForOperation(FindWithQuery23_operation, "query", "Sybase.Persistence.Query");
            Sybase.Reflection.OperationMetaData GetSize24_operation = AddOperationWithParams(4142, "getSize", "int", true);
            AddParameterForOperation(GetSize24_operation, "query", "Sybase.Persistence.Query");
            InitOperationMapFromOperations();
            SetName("FavoriteList");
            SetTable("\"graybarmobility_1_0_favoritelist\"");
            SetSynchronizationGroup("FavoriteSyncGroup");
        }
    }
}