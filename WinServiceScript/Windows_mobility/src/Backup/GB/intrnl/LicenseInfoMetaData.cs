
namespace GB.intrnl
{
    public class LicenseInfoMetaData
        : Sybase.Reflection.EntityMetaData
    {
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public LicenseInfoMetaData()
        {
            _init();
        }
        
        protected void _init()
        {
            SetId(12);
            SetAttributes(new Sybase.Reflection.AttributeMetaDataList());
            SetOperations(new Sybase.Reflection.OperationMetaDataList());
            SetAttributeMap(new Sybase.Reflection.AttributeMap());
            SetOperationMap(new Sybase.Reflection.OperationMap());
            Sybase.Reflection.AttributeMetaData lId_attribute = AddAttributeWithParams(0, "lId", "int", -1, false, false, false, false, false, false, "\"a\"");
            Sybase.Reflection.AttributeMetaData account_attribute = AddAttributeWithParams(1, "account", "string?", 50, false, false, false, false, false, false, "\"b\"");
            Sybase.Reflection.AttributeMetaData userId_attribute = AddAttributeWithParams(2, "userId", "string?", 100, false, false, false, false, false, false, "\"c\"");
            Sybase.Reflection.AttributeMetaData password_attribute = AddAttributeWithParams(3, "password", "string?", 100, false, false, false, false, false, false, "\"d\"");
            Sybase.Reflection.AttributeMetaData accountKey_attribute = AddAttributeWithParams(4, "accountKey", "string?", 100, false, false, false, false, false, false, "\"e\"");
            Sybase.Reflection.AttributeMetaData remarks_attribute = AddAttributeWithParams(5, "remarks", "string?", 500, false, false, false, false, false, false, "\"f\"");
            Sybase.Reflection.AttributeMetaData surrogateKey_attribute = AddAttributeWithParams(6, "surrogateKey", "long", -1, false, true, false, false, false, false, "\"g\"");
            InitAttributeMapFromAttributes();
            AddOperationWithParams(655, "findAll", "LicenseInfo*", true);
            Sybase.Reflection.OperationMetaData FindByPrimaryKey1_operation = AddOperationWithParams(657, "findByPrimaryKey", "LicenseInfo", true);
            AddParameterForOperation(FindByPrimaryKey1_operation, "lId", "int");
            AddOperationWithParams(660, "getAllLicenses", "Sybase.Persistence.QueryResultSet", true);
            AddOperationWithParams(4147, "isNew", "boolean", false);
            AddOperationWithParams(4148, "isDirty", "boolean", false);
            AddOperationWithParams(4149, "isDeleted", "boolean", false);
            AddOperationWithParams(4150, "refresh", "void", false);
            AddOperationWithParams(4151, "_pk", "long?", false);
            Sybase.Reflection.OperationMetaData FindWithQuery8_operation = AddOperationWithParams(4152, "findWithQuery", "LicenseInfo*", true);
            AddParameterForOperation(FindWithQuery8_operation, "query", "Sybase.Persistence.Query");
            Sybase.Reflection.OperationMetaData GetSize9_operation = AddOperationWithParams(4154, "getSize", "int", true);
            AddParameterForOperation(GetSize9_operation, "query", "Sybase.Persistence.Query");
            InitOperationMapFromOperations();
            SetName("LicenseInfo");
            SetTable("\"graybarmobility_1_0_licenseinfo\"");
            SetSynchronizationGroup("ReferenceSyncGroup");
        }
    }
}