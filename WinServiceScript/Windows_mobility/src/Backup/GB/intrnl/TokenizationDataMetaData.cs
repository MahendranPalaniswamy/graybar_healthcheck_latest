
namespace GB.intrnl
{
    public class TokenizationDataMetaData
        : Sybase.Reflection.EntityMetaData
    {
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public TokenizationDataMetaData()
        {
            _init();
        }
        
        protected void _init()
        {
            SetId(36);
            SetAttributes(new Sybase.Reflection.AttributeMetaDataList());
            SetOperations(new Sybase.Reflection.OperationMetaDataList());
            SetAttributeMap(new Sybase.Reflection.AttributeMap());
            SetOperationMap(new Sybase.Reflection.OperationMap());
            Sybase.Reflection.AttributeMetaData merchantGuid_attribute = AddAttributeWithParams(0, "merchantGuid", "string", 100, false, false, false, false, false, false, "\"a\"");
            Sybase.Reflection.AttributeMetaData signedPayload_attribute = AddAttributeWithParams(1, "signedPayload", "string", 1024, false, false, false, false, false, false, "\"b\"");
            Sybase.Reflection.AttributeMetaData paymetricURL_attribute = AddAttributeWithParams(2, "paymetricURL", "string", 1024, false, false, false, false, false, false, "\"c\"");
            Sybase.Reflection.AttributeMetaData surrogateKey_attribute = AddAttributeWithParams(3, "surrogateKey", "long", -1, false, true, false, false, false, false, "\"d\"");
            InitAttributeMapFromAttributes();
            AddOperationWithParams(1257, "findAll", "TokenizationData*", true);
            Sybase.Reflection.OperationMetaData FindByPrimaryKey1_operation = AddOperationWithParams(1259, "findByPrimaryKey", "TokenizationData", true);
            AddParameterForOperation(FindByPrimaryKey1_operation, "merchantGuid", "string");
            AddOperationWithParams(1262, "getTokenizationData", "Sybase.Persistence.QueryResultSet", true);
            AddOperationWithParams(4301, "isNew", "boolean", false);
            AddOperationWithParams(4302, "isDirty", "boolean", false);
            AddOperationWithParams(4303, "isDeleted", "boolean", false);
            AddOperationWithParams(4304, "refresh", "void", false);
            AddOperationWithParams(4305, "_pk", "long?", false);
            Sybase.Reflection.OperationMetaData FindWithQuery8_operation = AddOperationWithParams(4306, "findWithQuery", "TokenizationData*", true);
            AddParameterForOperation(FindWithQuery8_operation, "query", "Sybase.Persistence.Query");
            Sybase.Reflection.OperationMetaData GetSize9_operation = AddOperationWithParams(4308, "getSize", "int", true);
            AddParameterForOperation(GetSize9_operation, "query", "Sybase.Persistence.Query");
            InitOperationMapFromOperations();
            SetName("TokenizationData");
            SetTable("\"graybarmobility_1_0_tokenizationdata\"");
            SetSynchronizationGroup("ReferenceSyncGroup");
        }
    }
}