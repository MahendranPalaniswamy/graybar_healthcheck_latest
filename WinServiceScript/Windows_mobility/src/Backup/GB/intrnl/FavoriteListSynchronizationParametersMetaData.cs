
namespace GB.intrnl
{
    public class FavoriteListSynchronizationParametersMetaData
        : Sybase.Reflection.EntityMetaData
    {
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public FavoriteListSynchronizationParametersMetaData()
        {
            _init();
        }
        
        protected void _init()
        {
            SetId(46);
            SetAttributes(new Sybase.Reflection.AttributeMetaDataList());
            SetOperations(new Sybase.Reflection.OperationMetaDataList());
            SetAttributeMap(new Sybase.Reflection.AttributeMap());
            SetOperationMap(new Sybase.Reflection.OperationMap());
            Sybase.Reflection.AttributeMetaData user_id_attribute = AddAttributeWithParams(0, "user_id", "string?", 100, false, false, true, false, false, false, "\"user_id\"");
            Sybase.Reflection.AttributeMetaData user_idUserDefined_attribute = AddAttributeWithParams(1, "user_idUserDefined", "boolean", -1, false, false, true, false, false, false, "\"user_idUserDefined\"");
            Sybase.Reflection.AttributeMetaData size_sp_attribute = AddAttributeWithParams(2, "size_sp", "int", -1, false, false, true, false, false, false, "\"size_sp\"");
            Sybase.Reflection.AttributeMetaData user_sp_attribute = AddAttributeWithParams(3, "user_sp", "string", 300, false, true, true, false, false, false, "\"user_sp\"");
            InitAttributeMapFromAttributes();
            AddOperationWithParams(4515, "isNew", "boolean", false);
            AddOperationWithParams(4516, "isDirty", "boolean", false);
            AddOperationWithParams(4517, "isDeleted", "boolean", false);
            AddOperationWithParams(4518, "refresh", "void", false);
            AddOperationWithParams(4519, "_pk", "string?", false);
            AddOperationWithParams(4520, "save", "void", false);
            InitOperationMapFromOperations();
            SetName("FavoriteListSynchronizationParameters");
            SetTable("\"co_graybarmobility_1_0_favoritelistsp\"");
            SetSynchronizationGroup("");
        }
    }
}