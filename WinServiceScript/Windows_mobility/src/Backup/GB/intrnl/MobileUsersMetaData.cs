
namespace GB.intrnl
{
    public class MobileUsersMetaData
        : Sybase.Reflection.EntityMetaData
    {
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public MobileUsersMetaData()
        {
            _init();
        }
        
        protected void _init()
        {
            SetId(14);
            SetAttributes(new Sybase.Reflection.AttributeMetaDataList());
            SetOperations(new Sybase.Reflection.OperationMetaDataList());
            SetAttributeMap(new Sybase.Reflection.AttributeMap());
            SetOperationMap(new Sybase.Reflection.OperationMap());
            Sybase.Reflection.AttributeMetaData muId_attribute = AddAttributeWithParams(0, "muId", "int", -1, false, false, false, false, false, false, "\"a\"");
            Sybase.Reflection.AttributeMetaData active_attribute = AddAttributeWithParams(1, "active", "int?", -1, false, false, false, false, false, false, "\"b\"");
            Sybase.Reflection.AttributeMetaData userId_attribute = AddAttributeWithParams(2, "userId", "string?", 100, false, false, false, false, false, false, "\"c\"");
            Sybase.Reflection.AttributeMetaData soldTo_attribute = AddAttributeWithParams(3, "soldTo", "string?", 10, false, false, false, false, false, false, "\"d\"");
            Sybase.Reflection.AttributeMetaData firstName_attribute = AddAttributeWithParams(4, "firstName", "string?", 50, false, false, false, false, false, false, "\"e\"");
            Sybase.Reflection.AttributeMetaData lastName_attribute = AddAttributeWithParams(5, "lastName", "string?", 50, false, false, false, false, false, false, "\"f\"");
            Sybase.Reflection.AttributeMetaData salesOffice_attribute = AddAttributeWithParams(6, "salesOffice", "string?", 4, false, false, false, false, false, false, "\"g\"");
            Sybase.Reflection.AttributeMetaData jobTitle_attribute = AddAttributeWithParams(7, "jobTitle", "string?", 50, false, false, false, false, false, false, "\"h\"");
            Sybase.Reflection.AttributeMetaData userPhone_attribute = AddAttributeWithParams(8, "userPhone", "string?", 25, false, false, false, false, false, false, "\"i\"");
            Sybase.Reflection.AttributeMetaData companyName_attribute = AddAttributeWithParams(9, "companyName", "string?", 75, false, false, false, false, false, false, "\"j\"");
            Sybase.Reflection.AttributeMetaData companyPhone_attribute = AddAttributeWithParams(10, "companyPhone", "string?", 25, false, false, false, false, false, false, "\"l\"");
            Sybase.Reflection.AttributeMetaData dateCreated_attribute = AddAttributeWithParams(11, "dateCreated", "dateTime?", -1, false, false, false, false, false, false, "\"m\"");
            Sybase.Reflection.AttributeMetaData createdBy_attribute = AddAttributeWithParams(12, "createdBy", "string?", 100, false, false, false, false, false, false, "\"n\"");
            Sybase.Reflection.AttributeMetaData userType_attribute = AddAttributeWithParams(13, "userType", "string?", 20, false, false, false, false, false, false, "\"o\"");
            Sybase.Reflection.AttributeMetaData dateLastModified_attribute = AddAttributeWithParams(14, "dateLastModified", "dateTime?", -1, false, false, false, false, false, false, "\"p\"");
            Sybase.Reflection.AttributeMetaData lastModifiedBy_attribute = AddAttributeWithParams(15, "lastModifiedBy", "string?", 100, false, false, false, false, false, false, "\"q\"");
            Sybase.Reflection.AttributeMetaData surrogateKey_attribute = AddAttributeWithParams(16, "surrogateKey", "long", -1, false, true, false, false, false, false, "\"r\"");
            InitAttributeMapFromAttributes();
            AddOperationWithParams(694, "findAll", "MobileUsers*", true);
            Sybase.Reflection.OperationMetaData FindByPrimaryKey1_operation = AddOperationWithParams(696, "findByPrimaryKey", "MobileUsers", true);
            AddParameterForOperation(FindByPrimaryKey1_operation, "muId", "int");
            AddOperationWithParams(699, "getMobileUsers", "Sybase.Persistence.QueryResultSet", true);
            AddOperationWithParams(4157, "isNew", "boolean", false);
            AddOperationWithParams(4158, "isDirty", "boolean", false);
            AddOperationWithParams(4159, "isDeleted", "boolean", false);
            AddOperationWithParams(4160, "refresh", "void", false);
            AddOperationWithParams(4161, "_pk", "long?", false);
            Sybase.Reflection.OperationMetaData FindWithQuery8_operation = AddOperationWithParams(4162, "findWithQuery", "MobileUsers*", true);
            AddParameterForOperation(FindWithQuery8_operation, "query", "Sybase.Persistence.Query");
            Sybase.Reflection.OperationMetaData GetSize9_operation = AddOperationWithParams(4164, "getSize", "int", true);
            AddParameterForOperation(GetSize9_operation, "query", "Sybase.Persistence.Query");
            InitOperationMapFromOperations();
            SetName("MobileUsers");
            SetTable("\"graybarmobility_1_0_mobileusers\"");
            SetSynchronizationGroup("MobileUsersSyncGroup");
        }
    }
}