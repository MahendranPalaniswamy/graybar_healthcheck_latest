
namespace GB.intrnl
{
    public class Sold_To_InfoSynchronizationParametersMetaData
        : Sybase.Reflection.EntityMetaData
    {
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public Sold_To_InfoSynchronizationParametersMetaData()
        {
            _init();
        }
        
        protected void _init()
        {
            SetId(53);
            SetAttributes(new Sybase.Reflection.AttributeMetaDataList());
            SetOperations(new Sybase.Reflection.OperationMetaDataList());
            SetAttributeMap(new Sybase.Reflection.AttributeMap());
            SetOperationMap(new Sybase.Reflection.OperationMap());
            Sybase.Reflection.AttributeMetaData sold_to_attribute = AddAttributeWithParams(0, "sold_to", "string?", 10, false, false, true, false, false, false, "\"sold_to\"");
            Sybase.Reflection.AttributeMetaData sold_toUserDefined_attribute = AddAttributeWithParams(1, "sold_toUserDefined", "boolean", -1, false, false, true, false, false, false, "\"sold_toUserDefined\"");
            Sybase.Reflection.AttributeMetaData size_sp_attribute = AddAttributeWithParams(2, "size_sp", "int", -1, false, false, true, false, false, false, "\"size_sp\"");
            Sybase.Reflection.AttributeMetaData user_sp_attribute = AddAttributeWithParams(3, "user_sp", "string", 300, false, true, true, false, false, false, "\"user_sp\"");
            InitAttributeMapFromAttributes();
            AddOperationWithParams(4613, "isNew", "boolean", false);
            AddOperationWithParams(4614, "isDirty", "boolean", false);
            AddOperationWithParams(4615, "isDeleted", "boolean", false);
            AddOperationWithParams(4616, "refresh", "void", false);
            AddOperationWithParams(4617, "_pk", "string?", false);
            AddOperationWithParams(4618, "save", "void", false);
            InitOperationMapFromOperations();
            SetName("Sold_To_InfoSynchronizationParameters");
            SetTable("\"co_graybarmobility_1_0_sold_to_infosp\"");
            SetSynchronizationGroup("");
        }
    }
}