
namespace GB.intrnl
{
    public class UserSettingsMetaData
        : Sybase.Reflection.EntityMetaData
    {
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public UserSettingsMetaData()
        {
            _init();
        }
        
        protected void _init()
        {
            SetId(37);
            SetAttributes(new Sybase.Reflection.AttributeMetaDataList());
            SetOperations(new Sybase.Reflection.OperationMetaDataList());
            SetAttributeMap(new Sybase.Reflection.AttributeMap());
            SetOperationMap(new Sybase.Reflection.OperationMap());
            Sybase.Reflection.AttributeMetaData user_id_attribute = AddAttributeWithParams(0, "user_id", "string", 100, false, false, false, false, false, false, "\"a\"");
            Sybase.Reflection.AttributeMetaData default_contact_method_attribute = AddAttributeWithParams(1, "default_contact_method", "string?", 50, false, false, false, false, false, false, "\"b\"");
            Sybase.Reflection.AttributeMetaData default_payment_method_attribute = AddAttributeWithParams(2, "default_payment_method", "string?", 50, false, false, false, false, false, false, "\"c\"");
            Sybase.Reflection.AttributeMetaData default_pickup_plant_attribute = AddAttributeWithParams(3, "default_pickup_plant", "string?", 20, false, false, false, false, false, false, "\"d\"");
            Sybase.Reflection.AttributeMetaData default_shipping_addr_attribute = AddAttributeWithParams(4, "default_shipping_addr", "string?", 20, false, false, false, false, false, false, "\"e\"");
            Sybase.Reflection.AttributeMetaData pending_attribute = AddAttributeWithParams(20001, "pending", "boolean", -1, false, false, true, false, false, false, "\"pending\"");
            Sybase.Reflection.AttributeMetaData pendingChange_attribute = AddAttributeWithParams(20002, "pendingChange", "char", 1, false, false, true, false, false, false, "\"_pc\"");
            Sybase.Reflection.AttributeMetaData replayPending_attribute = AddAttributeWithParams(20005, "replayPending", "long", -1, false, false, true, false, false, false, "\"_rp\"");
            Sybase.Reflection.AttributeMetaData replayFailure_attribute = AddAttributeWithParams(20006, "replayFailure", "long", -1, false, false, true, false, false, false, "\"_rf\"");
            Sybase.Reflection.AttributeMetaData cvpOperation_attribute = AddAttributeWithParams(5, "cvpOperation", "bigString?", -1, false, false, true, false, false, false, "\"cvp_operation_header\"");
            Sybase.Reflection.AttributeMetaData cvpOperationLobs_attribute = AddAttributeWithParams(6, "cvpOperationLobs", "bigString?", -1, false, false, true, false, false, false, "\"cvp_operation_lobs\"");
            Sybase.Reflection.AttributeMetaData surrogateKey_attribute = AddAttributeWithParams(7, "surrogateKey", "long", -1, false, true, false, false, false, false, "\"f\"");
            Sybase.Reflection.AttributeMetaData replayCounter_attribute = AddAttributeWithParams(20004, "replayCounter", "long", -1, false, false, true, false, false, false, "\"_rc\"");
            Sybase.Reflection.AttributeMetaData disableSubmit_attribute = AddAttributeWithParams(20003, "disableSubmit", "boolean", -1, false, false, true, false, false, false, "\"_ds\"");
            Sybase.Reflection.AttributeMetaData cvpOperationLength_attribute = AddAttributeWithParams(8, "cvpOperationLength", "long", -1, false, false, true, false, false, false, "\"cvpOperation_length\"");
            Sybase.Reflection.AttributeMetaData cvpOperationLobsLength_attribute = AddAttributeWithParams(9, "cvpOperationLobsLength", "long", -1, false, false, true, false, false, false, "\"cvpOperationLobs_length\"");
            InitAttributeMapFromAttributes();
            AddOperationWithParams(1286, "findAll", "UserSettings*", true);
            Sybase.Reflection.OperationMetaData FindByPrimaryKey1_operation = AddOperationWithParams(1288, "findByPrimaryKey", "UserSettings", true);
            AddParameterForOperation(FindByPrimaryKey1_operation, "user_id", "string");
            AddOperationWithParams(1291, "getUserSettings", "Sybase.Persistence.QueryResultSet", true);
            AddOperationWithParams(1294, "create", "void", false, 'C');
            AddOperationWithParams(1306, "update", "void", false, 'U');
            AddOperationWithParams(1316, "delete", "void", false, 'D');
            AddOperationWithParams(3080, "getPendingObjects", "UserSettings*", true);
            Sybase.Reflection.OperationMetaData GetPendingObjects7_operation = AddOperationWithParams(3082, "getPendingObjects", "UserSettings*", true);
            AddParameterForOperation(GetPendingObjects7_operation, "pendingChange", "char");
            AddOperationWithParams(3085, "getReplayPendingObjects", "UserSettings*", true);
            AddOperationWithParams(4313, "isNew", "boolean", false);
            AddOperationWithParams(4314, "isDirty", "boolean", false);
            AddOperationWithParams(4315, "isDeleted", "boolean", false);
            AddOperationWithParams(4316, "refresh", "void", false);
            AddOperationWithParams(4317, "_pk", "long?", false);
            AddOperationWithParams(4318, "isPending", "boolean", false);
            AddOperationWithParams(4319, "isCreated", "boolean", false);
            AddOperationWithParams(4320, "isUpdated", "boolean", false);
            AddOperationWithParams(4321, "submitPending", "void", false);
            AddOperationWithParams(4322, "cancelPending", "void", false);
            AddOperationWithParams(4323, "submitPendingOperations", "void", true);
            AddOperationWithParams(4324, "cancelPendingOperations", "void", true);
            AddOperationWithParams(4325, "save", "void", false);
            Sybase.Reflection.OperationMetaData FindWithQuery22_operation = AddOperationWithParams(4326, "findWithQuery", "UserSettings*", true);
            AddParameterForOperation(FindWithQuery22_operation, "query", "Sybase.Persistence.Query");
            Sybase.Reflection.OperationMetaData GetSize23_operation = AddOperationWithParams(4328, "getSize", "int", true);
            AddParameterForOperation(GetSize23_operation, "query", "Sybase.Persistence.Query");
            InitOperationMapFromOperations();
            SetName("UserSettings");
            SetTable("\"graybarmobility_1_0_usersettings\"");
            SetSynchronizationGroup("ReferenceSyncGroup");
        }
    }
}