
namespace GB.intrnl
{
    public class PandAResultMetaData
        : Sybase.Reflection.EntityMetaData
    {
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public PandAResultMetaData()
        {
            _init();
        }
        
        protected void _init()
        {
            SetId(25);
            SetAttributes(new Sybase.Reflection.AttributeMetaDataList());
            SetOperations(new Sybase.Reflection.OperationMetaDataList());
            SetAttributeMap(new Sybase.Reflection.AttributeMap());
            SetOperationMap(new Sybase.Reflection.OperationMap());
            Sybase.Reflection.AttributeMetaData PRODUCT_ID_attribute = AddAttributeWithParams(0, "PRODUCT_ID", "string", 18, false, false, false, false, false, false, "\"a\"");
            Sybase.Reflection.AttributeMetaData UPC_CODE_attribute = AddAttributeWithParams(1, "UPC_CODE", "string", 18, false, false, false, false, false, false, "\"b\"");
            Sybase.Reflection.AttributeMetaData PRICE_attribute = AddAttributeWithParams(2, "PRICE", "decimal", -1, false, false, false, false, false, false, "\"c\"");
            Sybase.Reflection.AttributeMetaData PER_VALUE_attribute = AddAttributeWithParams(3, "PER_VALUE", "decimal", -1, false, false, false, false, false, false, "\"d\"");
            Sybase.Reflection.AttributeMetaData PICKUP_QTY_AVAILABLE_attribute = AddAttributeWithParams(4, "PICKUP_QTY_AVAILABLE", "decimal", -1, false, false, false, false, false, false, "\"e\"");
            Sybase.Reflection.AttributeMetaData SHIP_QTY_AVAILABLE_attribute = AddAttributeWithParams(5, "SHIP_QTY_AVAILABLE", "decimal", -1, false, false, false, false, false, false, "\"f\"");
            Sybase.Reflection.AttributeMetaData UOM_attribute = AddAttributeWithParams(6, "UOM", "string", 3, false, false, false, false, false, false, "\"g\"");
            Sybase.Reflection.AttributeMetaData VALID_TO_ORDER_attribute = AddAttributeWithParams(7, "VALID_TO_ORDER", "string", 1, false, false, false, false, false, false, "\"h\"");
            Sybase.Reflection.AttributeMetaData CATALOG_NUMBER_attribute = AddAttributeWithParams(8, "CATALOG_NUMBER", "string", 40, false, false, false, false, false, false, "\"i\"");
            Sybase.Reflection.AttributeMetaData DESCRIPTION_attribute = AddAttributeWithParams(9, "DESCRIPTION", "string", 40, false, false, false, false, false, false, "\"j\"");
            Sybase.Reflection.AttributeMetaData MANUFACTURER_NAME_attribute = AddAttributeWithParams(10, "MANUFACTURER_NAME", "string", 35, false, false, false, false, false, false, "\"l\"");
            Sybase.Reflection.AttributeMetaData MULTIPLES_attribute = AddAttributeWithParams(11, "MULTIPLES", "decimal", -1, false, false, false, false, false, false, "\"m\"");
            Sybase.Reflection.AttributeMetaData MULTIPLES_UOM_attribute = AddAttributeWithParams(12, "MULTIPLES_UOM", "string", 3, false, false, false, false, false, false, "\"n\"");
            Sybase.Reflection.AttributeMetaData surrogateKey_attribute = AddAttributeWithParams(13, "surrogateKey", "long", -1, false, true, false, false, false, false, "\"o\"");
            InitAttributeMapFromAttributes();
            AddOperationWithParams(972, "findAll", "PandAResult*", true);
            Sybase.Reflection.OperationMetaData FindByPrimaryKey1_operation = AddOperationWithParams(974, "findByPrimaryKey", "PandAResult", true);
            AddParameterForOperation(FindByPrimaryKey1_operation, "PRODUCT_ID", "string");
            AddOperationWithParams(977, "getPandAResult", "Sybase.Persistence.QueryResultSet", true);
            AddOperationWithParams(4218, "isNew", "boolean", false);
            AddOperationWithParams(4219, "isDirty", "boolean", false);
            AddOperationWithParams(4220, "isDeleted", "boolean", false);
            AddOperationWithParams(4221, "refresh", "void", false);
            AddOperationWithParams(4222, "_pk", "long?", false);
            Sybase.Reflection.OperationMetaData FindWithQuery8_operation = AddOperationWithParams(4223, "findWithQuery", "PandAResult*", true);
            AddParameterForOperation(FindWithQuery8_operation, "query", "Sybase.Persistence.Query");
            Sybase.Reflection.OperationMetaData GetSize9_operation = AddOperationWithParams(4225, "getSize", "int", true);
            AddParameterForOperation(GetSize9_operation, "query", "Sybase.Persistence.Query");
            InitOperationMapFromOperations();
            SetName("PandAResult");
            SetTable("\"graybarmobility_1_0_pandaresult\"");
            SetSynchronizationGroup("PandAOnDemandSyncGroup");
        }
    }
}