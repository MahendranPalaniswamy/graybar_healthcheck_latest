
namespace GB.intrnl
{
    public class CreditCardMetaData
        : Sybase.Reflection.EntityMetaData
    {
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public CreditCardMetaData()
        {
            _init();
        }
        
        protected void _init()
        {
            SetId(39);
            SetAttributes(new Sybase.Reflection.AttributeMetaDataList());
            SetOperations(new Sybase.Reflection.OperationMetaDataList());
            SetAttributeMap(new Sybase.Reflection.AttributeMap());
            SetOperationMap(new Sybase.Reflection.OperationMap());
            Sybase.Reflection.AttributeMetaData name_on_card_attribute = AddAttributeWithParams(0, "name_on_card", "string?", 50, false, false, false, false, false, false, "\"b\"");
            Sybase.Reflection.AttributeMetaData address1_attribute = AddAttributeWithParams(1, "address1", "string?", 50, false, false, false, false, false, false, "\"c\"");
            Sybase.Reflection.AttributeMetaData address2_attribute = AddAttributeWithParams(2, "address2", "string?", 50, false, false, false, false, false, false, "\"d\"");
            Sybase.Reflection.AttributeMetaData city_attribute = AddAttributeWithParams(3, "city", "string?", 20, false, false, false, false, false, false, "\"e\"");
            Sybase.Reflection.AttributeMetaData state_attribute = AddAttributeWithParams(4, "state", "string?", 2, false, false, false, false, false, false, "\"f\"");
            Sybase.Reflection.AttributeMetaData zipcode_attribute = AddAttributeWithParams(5, "zipcode", "string?", 20, false, false, false, false, false, false, "\"g\"");
            Sybase.Reflection.AttributeMetaData exp_month_attribute = AddAttributeWithParams(6, "exp_month", "string?", 2, false, false, false, false, false, false, "\"h\"");
            Sybase.Reflection.AttributeMetaData exp_year_attribute = AddAttributeWithParams(7, "exp_year", "string?", 4, false, false, false, false, false, false, "\"i\"");
            Sybase.Reflection.AttributeMetaData typecode_attribute = AddAttributeWithParams(8, "typecode", "string?", 10, false, false, false, false, false, false, "\"j\"");
            Sybase.Reflection.AttributeMetaData typename_attribute = AddAttributeWithParams(9, "typename", "string?", 20, false, false, false, false, false, false, "\"l\"");
            Sybase.Reflection.AttributeMetaData last4digit_attribute = AddAttributeWithParams(10, "last4digit", "string?", 10, false, false, false, false, false, false, "\"m\"");
            Sybase.Reflection.AttributeMetaData cvv_attribute = AddAttributeWithParams(11, "cvv", "string?", 5, false, false, false, false, false, false, "\"n\"");
            Sybase.Reflection.AttributeMetaData tokenkey_attribute = AddAttributeWithParams(12, "tokenkey", "string?", 20, false, false, false, false, false, false, "\"o\"");
            Sybase.Reflection.AttributeMetaData id_attribute = AddAttributeWithParams(13, "id", "string", 100, false, true, false, false, false, false, "\"a\"");
            InitAttributeMapFromAttributes();
            AddOperationWithParams(379, "findAll", "CreditCard*", true);
            Sybase.Reflection.OperationMetaData FindByPrimaryKey1_operation = AddOperationWithParams(381, "findByPrimaryKey", "CreditCard", true);
            AddParameterForOperation(FindByPrimaryKey1_operation, "id", "string");
            AddOperationWithParams(384, "getAllCreditCards", "Sybase.Persistence.QueryResultSet", true);
            Sybase.Reflection.OperationMetaData GetByPrimaryKey3_operation = AddOperationWithParams(386, "getByPrimaryKey", "Sybase.Persistence.QueryResultSet", true);
            AddParameterForOperation(GetByPrimaryKey3_operation, "id", "string");
            AddOperationWithParams(390, "create", "void", false, 'C');
            AddOperationWithParams(391, "update", "void", false, 'U');
            AddOperationWithParams(392, "delete", "void", false, 'D');
            AddOperationWithParams(4070, "isNew", "boolean", false);
            AddOperationWithParams(4071, "isDirty", "boolean", false);
            AddOperationWithParams(4072, "isDeleted", "boolean", false);
            AddOperationWithParams(4073, "refresh", "void", false);
            AddOperationWithParams(4074, "_pk", "string?", false);
            AddOperationWithParams(4075, "save", "void", false);
            Sybase.Reflection.OperationMetaData FindWithQuery13_operation = AddOperationWithParams(4076, "findWithQuery", "CreditCard*", true);
            AddParameterForOperation(FindWithQuery13_operation, "query", "Sybase.Persistence.Query");
            Sybase.Reflection.OperationMetaData GetSize14_operation = AddOperationWithParams(4078, "getSize", "int", true);
            AddParameterForOperation(GetSize14_operation, "query", "Sybase.Persistence.Query");
            InitOperationMapFromOperations();
            SetName("CreditCard");
            SetTable("\"co_graybarmobility_1_0_creditcard\"");
            SetSynchronizationGroup("");
        }
    }
}