
namespace GB.intrnl
{
    public class KeyPackageNameMetaData
        : Sybase.Reflection.ClassMetaData
    {
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public KeyPackageNameMetaData()
        {
            _init();
        }
        
        protected void _init()
        {
            SetId(91);
            SetAttributes(new Sybase.Reflection.AttributeMetaDataList());
            SetOperations(new Sybase.Reflection.OperationMetaDataList());
            SetAttributeMap(new Sybase.Reflection.AttributeMap());
            SetOperationMap(new Sybase.Reflection.OperationMap());
            Sybase.Reflection.AttributeMetaData key_name_attribute = AddAttributeWithParams(0, "key_name", "string", 255, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData package_name_attribute = AddAttributeWithParams(1, "package_name", "string", 100, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData user_name_attribute = AddAttributeWithParams(2, "user_name", "string", 255, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData domain_name_attribute = AddAttributeWithParams(3, "domain_name", "string", 100, false, false, true, false, false, false);
            InitAttributeMapFromAttributes();
            SetName("KeyPackageName");
            
        }
        
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public  override bool IsEntity()
        {
            return false;
        }
        
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public  override bool IsService()
        {
            return false;
        }
    }
}