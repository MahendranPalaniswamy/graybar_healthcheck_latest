
namespace GB.intrnl
{
    public class SessionPersonalizationMetaData
        : Sybase.Reflection.ClassMetaData
    {
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public SessionPersonalizationMetaData()
        {
            _init();
        }
        
        protected void _init()
        {
            SetId(93);
            SetAttributes(new Sybase.Reflection.AttributeMetaDataList());
            SetOperations(new Sybase.Reflection.OperationMetaDataList());
            SetAttributeMap(new Sybase.Reflection.AttributeMap());
            SetOperationMap(new Sybase.Reflection.OperationMap());
            Sybase.Reflection.AttributeMetaData key_name_attribute = AddAttributeWithParams(0, "key_name", "string", -1, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData value_attribute = AddAttributeWithParams(1, "value", "string?", -1, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData user_defined_attribute = AddAttributeWithParams(2, "user_defined", "boolean", -1, false, false, true, false, false, false);
            Sybase.Reflection.AttributeMetaData listSessionPK_attribute = AddAttributeWithParams(3, "listSessionPK", "GB.SessionPersonalization*", -1, true, false, true, false, false, false);
            InitAttributeMapFromAttributes();
            AddOperationWithParams(2276, "findAll", "SessionPersonalization*", true);
            InitOperationMapFromOperations();
            SetName("SessionPersonalization");
            
        }
        
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public  override bool IsEntity()
        {
            return false;
        }
        
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public  override bool IsService()
        {
            return false;
        }
    }
}