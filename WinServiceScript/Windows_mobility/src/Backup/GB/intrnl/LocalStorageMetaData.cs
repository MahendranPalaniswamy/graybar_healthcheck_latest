
namespace GB.intrnl
{
    public class LocalStorageMetaData
        : Sybase.Reflection.EntityMetaData
    {
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public LocalStorageMetaData()
        {
            _init();
        }
        
        protected void _init()
        {
            SetId(57);
            SetAttributes(new Sybase.Reflection.AttributeMetaDataList());
            SetOperations(new Sybase.Reflection.OperationMetaDataList());
            SetAttributeMap(new Sybase.Reflection.AttributeMap());
            SetOperationMap(new Sybase.Reflection.OperationMap());
            Sybase.Reflection.AttributeMetaData varValue_attribute = AddAttributeWithParams(0, "varValue", "string", 128, false, false, true, false, false, false, "\"var_value\"");
            Sybase.Reflection.AttributeMetaData varName_attribute = AddAttributeWithParams(1, "varName", "string", 32, false, true, true, false, false, false, "\"var_name\"");
            InitAttributeMapFromAttributes();
            AddOperationWithParams(4660, "isNew", "boolean", false);
            AddOperationWithParams(4661, "isDirty", "boolean", false);
            AddOperationWithParams(4662, "isDeleted", "boolean", false);
            AddOperationWithParams(4663, "refresh", "void", false);
            AddOperationWithParams(4664, "_pk", "string?", false);
            AddOperationWithParams(4665, "save", "void", false);
            InitOperationMapFromOperations();
            SetName("LocalStorage");
            SetTable("\"co_localstorage\"");
            SetSynchronizationGroup("");
        }
    }
}