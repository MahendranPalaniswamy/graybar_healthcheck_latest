
namespace GB.intrnl
{
    public class CustomAddressMetaData
        : Sybase.Reflection.EntityMetaData
    {
        /// <summary>
        /// Sybase internal use only.
        /// <summary>
        public CustomAddressMetaData()
        {
            _init();
        }
        
        protected void _init()
        {
            SetId(5);
            SetAttributes(new Sybase.Reflection.AttributeMetaDataList());
            SetOperations(new Sybase.Reflection.OperationMetaDataList());
            SetAttributeMap(new Sybase.Reflection.AttributeMap());
            SetOperationMap(new Sybase.Reflection.OperationMap());
            Sybase.Reflection.AttributeMetaData user_id_attribute = AddAttributeWithParams(0, "user_id", "string", 100, false, false, false, false, false, false, "\"a\"");
            Sybase.Reflection.AttributeMetaData care_of_attribute = AddAttributeWithParams(1, "care_of", "string?", 40, false, false, false, false, false, false, "\"b\"");
            Sybase.Reflection.AttributeMetaData city_attribute = AddAttributeWithParams(2, "city", "string?", 40, false, false, false, false, false, false, "\"c\"");
            Sybase.Reflection.AttributeMetaData country_attribute = AddAttributeWithParams(3, "country", "string?", 3, false, false, false, false, false, false, "\"d\"");
            Sybase.Reflection.AttributeMetaData name_attribute = AddAttributeWithParams(4, "name", "string?", 40, false, false, false, false, false, false, "\"e\"");
            Sybase.Reflection.AttributeMetaData name2_attribute = AddAttributeWithParams(5, "name2", "string?", 40, false, false, false, false, false, false, "\"f\"");
            Sybase.Reflection.AttributeMetaData state_attribute = AddAttributeWithParams(6, "state", "string?", 3, false, false, false, false, false, false, "\"g\"");
            Sybase.Reflection.AttributeMetaData street1_attribute = AddAttributeWithParams(7, "street1", "string?", 60, false, false, false, false, false, false, "\"h\"");
            Sybase.Reflection.AttributeMetaData street2_attribute = AddAttributeWithParams(8, "street2", "string?", 60, false, false, false, false, false, false, "\"i\"");
            Sybase.Reflection.AttributeMetaData zipcode_attribute = AddAttributeWithParams(9, "zipcode", "string?", 10, false, false, false, false, false, false, "\"j\"");
            Sybase.Reflection.AttributeMetaData pending_attribute = AddAttributeWithParams(20001, "pending", "boolean", -1, false, false, true, false, false, false, "\"pending\"");
            Sybase.Reflection.AttributeMetaData pendingChange_attribute = AddAttributeWithParams(20002, "pendingChange", "char", 1, false, false, true, false, false, false, "\"_pc\"");
            Sybase.Reflection.AttributeMetaData replayPending_attribute = AddAttributeWithParams(20005, "replayPending", "long", -1, false, false, true, false, false, false, "\"_rp\"");
            Sybase.Reflection.AttributeMetaData replayFailure_attribute = AddAttributeWithParams(20006, "replayFailure", "long", -1, false, false, true, false, false, false, "\"_rf\"");
            Sybase.Reflection.AttributeMetaData cvpOperation_attribute = AddAttributeWithParams(10, "cvpOperation", "bigString?", -1, false, false, true, false, false, false, "\"cvp_operation_header\"");
            Sybase.Reflection.AttributeMetaData cvpOperationLobs_attribute = AddAttributeWithParams(11, "cvpOperationLobs", "bigString?", -1, false, false, true, false, false, false, "\"cvp_operation_lobs\"");
            Sybase.Reflection.AttributeMetaData surrogateKey_attribute = AddAttributeWithParams(12, "surrogateKey", "long", -1, false, true, false, false, false, false, "\"l\"");
            Sybase.Reflection.AttributeMetaData replayCounter_attribute = AddAttributeWithParams(20004, "replayCounter", "long", -1, false, false, true, false, false, false, "\"_rc\"");
            Sybase.Reflection.AttributeMetaData disableSubmit_attribute = AddAttributeWithParams(20003, "disableSubmit", "boolean", -1, false, false, true, false, false, false, "\"_ds\"");
            Sybase.Reflection.AttributeMetaData cvpOperationLength_attribute = AddAttributeWithParams(13, "cvpOperationLength", "long", -1, false, false, true, false, false, false, "\"cvpOperation_length\"");
            Sybase.Reflection.AttributeMetaData cvpOperationLobsLength_attribute = AddAttributeWithParams(14, "cvpOperationLobsLength", "long", -1, false, false, true, false, false, false, "\"cvpOperationLobs_length\"");
            InitAttributeMapFromAttributes();
            AddOperationWithParams(412, "findAll", "CustomAddress*", true);
            Sybase.Reflection.OperationMetaData FindByPrimaryKey1_operation = AddOperationWithParams(414, "findByPrimaryKey", "CustomAddress", true);
            AddParameterForOperation(FindByPrimaryKey1_operation, "user_id", "string");
            AddOperationWithParams(417, "getCustomAddress", "Sybase.Persistence.QueryResultSet", true);
            AddOperationWithParams(420, "create", "void", false, 'C');
            AddOperationWithParams(437, "update", "void", false, 'U');
            AddOperationWithParams(452, "delete", "void", false, 'D');
            AddOperationWithParams(2570, "getPendingObjects", "CustomAddress*", true);
            Sybase.Reflection.OperationMetaData GetPendingObjects7_operation = AddOperationWithParams(2572, "getPendingObjects", "CustomAddress*", true);
            AddParameterForOperation(GetPendingObjects7_operation, "pendingChange", "char");
            AddOperationWithParams(2575, "getReplayPendingObjects", "CustomAddress*", true);
            AddOperationWithParams(4083, "isNew", "boolean", false);
            AddOperationWithParams(4084, "isDirty", "boolean", false);
            AddOperationWithParams(4085, "isDeleted", "boolean", false);
            AddOperationWithParams(4086, "refresh", "void", false);
            AddOperationWithParams(4087, "_pk", "long?", false);
            AddOperationWithParams(4088, "isPending", "boolean", false);
            AddOperationWithParams(4089, "isCreated", "boolean", false);
            AddOperationWithParams(4090, "isUpdated", "boolean", false);
            AddOperationWithParams(4091, "submitPending", "void", false);
            AddOperationWithParams(4092, "cancelPending", "void", false);
            AddOperationWithParams(4093, "submitPendingOperations", "void", true);
            AddOperationWithParams(4094, "cancelPendingOperations", "void", true);
            AddOperationWithParams(4095, "save", "void", false);
            Sybase.Reflection.OperationMetaData FindWithQuery22_operation = AddOperationWithParams(4096, "findWithQuery", "CustomAddress*", true);
            AddParameterForOperation(FindWithQuery22_operation, "query", "Sybase.Persistence.Query");
            Sybase.Reflection.OperationMetaData GetSize23_operation = AddOperationWithParams(4098, "getSize", "int", true);
            AddParameterForOperation(GetSize23_operation, "query", "Sybase.Persistence.Query");
            InitOperationMapFromOperations();
            SetName("CustomAddress");
            SetTable("\"graybarmobility_1_0_customaddress\"");
            SetSynchronizationGroup("ReferenceSyncGroup");
        }
    }
}