@echo off

SET arg1=%1

SET PROG="%~dp0SyncWindowsService.exe"
SET FIRSTPART=%WINDIR%"\Microsoft.NET\Framework\v"
SET SECONDPART="\InstallUtil.exe"
SET DOTNETVER=4.0.30319
  IF EXIST %FIRSTPART%%DOTNETVER%%SECONDPART% GOTO install
SET DOTNETVER=2.0.50727
  IF EXIST %FIRSTPART%%DOTNETVER%%SECONDPART% GOTO install
SET DOTNETVER=1.0.3705
  IF EXIST %FIRSTPART%%DOTNETVER%%SECONDPART% GOTO install
GOTO fail
:install
  ECHO Found .NET Framework version %DOTNETVER%
  IF /i %arg1%=='-I' GOTO displayinstallmessage
  IF /i %arg1%=='-i' GOTO displayinstallmessage
  IF /i %arg1%=='-U' GOTO displayuninstallmessage
  IF /i %arg1%=='-u' GOTO displayuninstallmessage
  
:displayinstallmessage 
ECHO Installing service %PROG%
%FIRSTPART%%DOTNETVER%%SECONDPART% %arg1% %PROG%
Exit /B 5

:displayuninstallmessage
ECHO Uninstalling service %PROG%
%FIRSTPART%%DOTNETVER%%SECONDPART% %arg1% %PROG%
Exit /B 5

IF /i %arg1%=='-U' GOTO endservice
IF /i %arg1%=='-u' GOTO endservice



:fail
echo FAILURE -- Could not find .NET Framework install
:param_error
echo USAGE: GrayBarSMPHealthCheckScript.bat [install type (-I or -U)] [application (.exe)]

:endservice
echo ***** Operation completed *****