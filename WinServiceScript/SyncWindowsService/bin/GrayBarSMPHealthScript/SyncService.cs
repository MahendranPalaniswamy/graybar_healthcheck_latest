﻿using System;
using System.ServiceProcess;
using System.Threading;
using WinServiceScript;

namespace SyncWindowsService
{
    public partial class SyncService : ServiceBase
    {
        private WinScript _winScript;
        private string _returnStatus;
        private Timer workTimer;    // System.Threading.Timer

        public SyncService()
        {
            InitializeComponent();
            _winScript = new WinScript();
            _winScript.InitializeEventLog();
        }

        protected override void OnStart(string[] args)
        {

            try
            {
                //Set the configuration details
                ConfigurationInstaller.ConfigurationManipulator(args);

                //Convert to milliseconds
                int interval = Convert.ToInt32(GrayBarAnonDBSettings.Interval) * 60 * 1000;

                //Call the sync process every 15 minutes
                workTimer = new Timer(new TimerCallback(SyncMBOs), null, 2000, interval);

            }
            catch (Exception ex)
            {
                _winScript.WriteErrorEntry(ex.StackTrace);
            }
        }

        private void SyncMBOs(object state)
        {
            _returnStatus = _winScript.SyncAll();
        }

        protected override void OnStop()
        {
            try
            {
                _winScript.RemoveSource();
            }
            catch (Exception ex)
            {
                _winScript.WriteErrorEntry(ex.StackTrace);
            }
            finally
            {
                workTimer.Dispose();
            }
        }

        protected override void OnPause()
        {
            try
            {
                _winScript.WriteEntry("Sync Service was Paused!!");
            }
            catch (Exception ex)
            {
                _winScript.WriteErrorEntry(ex.StackTrace);
            }
        }
    }
}
