@echo off

SET arg1=%1
SET arg2=%2
SET arg3=%3
SET arg4=%4
SET PROG="%~dp0SyncWindowsService.exe"
SET FIRSTPART=%WINDIR%"\Microsoft.NET\Framework\v"
SET SECONDPART="\InstallUtil.exe"
SET DOTNETVER=4.0.30319
  IF EXIST %FIRSTPART%%DOTNETVER%%SECONDPART% GOTO install
SET DOTNETVER=2.0.50727
  IF EXIST %FIRSTPART%%DOTNETVER%%SECONDPART% GOTO install
SET DOTNETVER=1.0.3705
  IF EXIST %FIRSTPART%%DOTNETVER%%SECONDPART% GOTO install
GOTO fail
:install
  ECHO Found .NET Framework version %DOTNETVER%
  IF /i %arg1%=='-I' GOTO displayinstallmessage
  IF /i %arg1%=='-i' GOTO displayinstallmessage
  IF /i %arg1%=='-U' GOTO displayuninstallmessage
  IF /i %arg1%=='-u' GOTO displayuninstallmessage
  
  :displayinstallmessage 
	ECHO Installing service %PROG%
	GOTO installCmd
  :displayuninstallmessage
	ECHO Uninstalling service %PROG%
	
:installCmd
  %FIRSTPART%%DOTNETVER%%SECONDPART% %arg1% %PROG%
  
   ECHO DONE!!
   
  IF EXIST /i %arg2% (
  GOTO next
  )
  :next
  IF EXIST /i %arg3% ( 
  GOTO nextnext
  )
  :nextnext
  IF EXIST /i %arg4% (
  ECHO Starting the service ....
  sc start GrayBarSMPHealthCheckScript %arg2% %arg3% %arg4%
  ECHO Service started .....
  )
    
  GOTO end
:fail
  echo FAILURE -- Could not find .NET Framework install
:param_error
  echo USAGE: GrayBarSMPHealthCheckScript.bat [install type (-I or -U)] [application (.exe)]
:end

 
  Pause