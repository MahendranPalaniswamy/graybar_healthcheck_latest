﻿using System.Configuration;

namespace SyncWindowsService
{
    partial class SyncServiceProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private string serviceName = string.Empty;
        private string displayName = string.Empty;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.syncServiceProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.syncServiceInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // syncServiceProcessInstaller
            // 
            this.syncServiceProcessInstaller.Password = null;
            this.syncServiceProcessInstaller.Username = null;
            // 
            // syncServiceInstaller
            // 
            this.syncServiceInstaller.Description = "This Services Syncs all the MBOs from the relay Server";

            if (ConfigurationManager.AppSettings["protocol"] == "https")
            {
                displayName = "Secured GrayBar SMP health check script";
                serviceName = "SecuredGrayBarSMPHealthCheck";
            }
            else
            {
                displayName = "GrayBar SMP health check script";
                serviceName = "GrayBarSMPHealthCheck";
            }

            this.syncServiceInstaller.DisplayName = displayName;
            this.syncServiceInstaller.ServiceName = serviceName;
            this.syncServiceInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            // 
            // SyncServiceProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.syncServiceProcessInstaller,
            this.syncServiceInstaller});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller syncServiceProcessInstaller;
        private System.ServiceProcess.ServiceInstaller syncServiceInstaller;
    }
}