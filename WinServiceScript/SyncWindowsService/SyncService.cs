﻿using System;
using System.Diagnostics;
using System.ServiceProcess;
using System.Threading;
using WinServiceScript;

namespace SyncWindowsService
{
    public partial class SyncService : ServiceBase
    {
        private WinScript _winScript;
        private string _returnStatus;

        public SyncService()
        {
            InitializeComponent();
            _winScript = new WinScript();
            _winScript.InitializeEventLog();
        }

        protected override void OnStart(string[] args)
        {
            base.OnStart(args);

            var t = new Thread(SyncMBOs);
            t.Start();
        }

        private void SyncMBOs()
        {
            int interval = GrayBarAnonDBSettings.Interval;
            _winScript = new WinScript();

            while (true)
            {
                try
                {
                    _returnStatus = _winScript.SyncAll();
                }
                catch (Exception)
                {
                    Thread.Sleep(interval * 60 * 1000);
                }
                Thread.Sleep(interval * 60 * 1000);
            }
        }

        protected override void OnStop()
        {
            base.OnStop();
        }

        protected override void OnPause()
        {
            try
            {
                _winScript = new WinScript();
                _winScript.WriteEntry("Sync Service was Paused!!");
            }
            catch (Exception ex)
            {
                _winScript = new WinScript();
                _winScript.WriteErrorEntry("Error occured while pause operation !");
            }
        }
    }
}
