﻿using System;
using System.Diagnostics;


namespace WinServiceScript
{
    public class WinScript : IWinScript
    {
        #region private variables

        GrayBarAnonymousDBConnectionManager _script;
        GrayBarMobilityDbConnectionManager _mobilityScript;
        EventLog _log;
        string _returnSyncStatus = string.Empty;

        #endregion

        public WinScript()
        {
            _script = new GrayBarAnonymousDBConnectionManager();
            _mobilityScript = new GrayBarMobilityDbConnectionManager();
        }


        #region Private methods

        public void InitializeEventLog()
        {
            _log = new EventLog();
            _log.Source = GrayBarAnonDBSettings.SYNC_SERVICE_LOG;
        }

        #endregion

        #region Public methods

        public void WriteEntry(string message)
        {
            _log = new EventLog();
            _log.Source = GrayBarAnonDBSettings.SYNC_SERVICE_LOG;
            _log.WriteEntry(message, EventLogEntryType.Information, 1);
        }

        public void WriteErrorEntry(string message)
        {
            _log = new EventLog();
            _log.Source = GrayBarAnonDBSettings.SYNC_SERVICE_LOG;
            _log.WriteEntry(message, EventLogEntryType.Error, 7);
        }


        public bool SyncAnonymousDB(bool value, string serverName)
        {
            bool result = true;
            string port = value == true ? "443" : "80";
            string successMessage = GrayBarAnonDBSettings.SUCCESS_MESSAGE_ANON_DB + "For the following server: "
                + serverName + " on port: " + port;
            try
            {
                //Create event logs
                InitializeEventLog();
                //First check with Port 443
                _script.InitializeApplication(value, serverName, Convert.ToInt32(port));

                var registerStatus = _script.Register();
                var loginStatus = _script.Login();
               
                var syncStatus = _script.Sync();

                _log.WriteEntry(successMessage, EventLogEntryType.SuccessAudit, 1);

                _returnSyncStatus = GrayBarAnonDBSettings.SYNC_SUCCESS;
            }
            catch (Exception ex)
            {
                _log.WriteEntry("Health script failed to connect to the following server: " + serverName + " on port "+ port 
                    + ex.Message, EventLogEntryType.Error, 7);

                result = false;
                _script.ResetApplication();
                throw;
            }

            return result;
        }


        public bool SyncMobilityDB(bool useSSL, string serverName)
        {
            bool status = true;
            string port = useSSL == true ? "443" : "80";
            string successMessage = GrayBarAnonDBSettings.SUCCESS_MESSAGE_MOBILITY_DB + "For the following server: "
                + serverName + " on port: " + port;
            try
            {
                _mobilityScript.InitializeApplication(useSSL, serverName);
                    _mobilityScript.Login();

                _mobilityScript.Sync();

                //Synchronize the groups
                _mobilityScript.SyncGroup(GrayBarAnonDBSettings.MOBILE_USERS_SYNC_GROUP);

                //Synchronizing they order group
                _mobilityScript.SyncGroup(GrayBarAnonDBSettings.SYNC_GRP_ORDER);

                
                var uregister = _script.UnRegister();
                _log.WriteEntry(successMessage, EventLogEntryType.SuccessAudit, 1);

                _returnSyncStatus = GrayBarAnonDBSettings.SYNC_SUCCESS;
            }
            catch (Exception ex)
            {
                _log.WriteEntry("Health script failed to connect to the following server: " + serverName
                        + " at port "+ port + ex.Message, EventLogEntryType.Error, 7);

                _mobilityScript.ResetApplication();
                status = false;
                throw;
            }

            return status;
        }

        /// <summary>
        /// Syncs the Anonymous DB Registers the application and logs in to the LDAP Server
        /// Syncs the Order group from Mobility DB.
        /// </summary>
        public string SyncAll()
        {
            try
            {
                //Sync alias server then sync relay
                string aliasServerName = GrayBarAnonDBSettings.Alias;
                SyncAnonymousDB(true, aliasServerName);
                SyncMobilityDB(true, aliasServerName);

                //Sync alias server then sync relay
                SyncAnonymousDB(false, GrayBarAnonDBSettings.RelayServerAddress);
                SyncMobilityDB(false, GrayBarAnonDBSettings.RelayServerAddress);
            }
            catch (Exception ex)
            {
                throw;
            }
            return _returnSyncStatus;
        }

        #endregion

    }
}
