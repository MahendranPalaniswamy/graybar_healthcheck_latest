﻿using com.sybase.afx.util;
using Sybase.Persistence;
using GB;
using Sybase.Collections;

namespace WinServiceScript
{
   public class MobilityCallbackHandler : DefaultCallbackHandler
    {
        public override SynchronizationAction
        OnSynchronize(Sybase.Collections.GenericList<ISynchronizationGroup> groups,
        SynchronizationContext context)
        {
            if (context.Status ==
            SynchronizationStatus.FINISHING || context.Status ==
            SynchronizationStatus.ASYNC_REPLAY_UPLOADED)
            {

            }
            return SynchronizationAction.CONTINUE;
        }
    }
}
