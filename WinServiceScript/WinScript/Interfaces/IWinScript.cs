﻿namespace WinServiceScript
{
    public interface IWinScript
    {
        
        bool SyncAnonymousDB(bool useSSL, string serverName);

        bool SyncMobilityDB(bool useSSL, string serverName);

        string SyncAll();

        void WriteEntry(string message);

    }
}