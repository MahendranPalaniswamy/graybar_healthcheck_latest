﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinServiceScript.Interfaces
{
   public interface ISyncMobilityDBScript
    {
       
        bool Login();

        bool Sync();

        bool SyncGroup(string groupName);

        void InitializeApplication(bool secured, string serverName);

        void ResetApplication();
    }
}
