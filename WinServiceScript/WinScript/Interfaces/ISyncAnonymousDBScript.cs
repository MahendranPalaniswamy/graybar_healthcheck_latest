﻿namespace WinServiceScript.Interfaces
{
    public interface ISyncAnonymousDBScript
    {
        bool Register();

        bool Login();

        bool UnRegister();

        bool Sync();

        void InitializeApplication(bool secured, string serverName, int port);

        void ResetApplication();
    }
}
