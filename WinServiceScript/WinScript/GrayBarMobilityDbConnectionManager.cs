﻿using GB;
using Sybase.Mobile;
using Sybase.Persistence;
using System;
using System.Collections.Generic;
using System.Configuration;
using WinServiceScript.Interfaces;

namespace WinServiceScript
{
    public class GrayBarMobilityDbConnectionManager : ISyncMobilityDBScript
    {
        #region Private variables

        private Application _app;
        private LoginCredentials _loginCredentials;
        private ConnectionProperties _conProperties;
        private List<MobileUsers> _mobileUsers;

        #endregion

        #region Public methods


        /// <summary>
        /// Initialize the application
        /// </summary>
        public void InitializeApplication(bool secured, string serverName)
        {
            _app = Application.GetInstance();
            _app.ApplicationIdentifier = "GraybarMobility";

            //Based on the dbType value being passed switch the handlers
            GraybarMobilityDB.RegisterCallbackHandler(new MobilityCallbackHandler());

            //Set the login credentials for connection properties
            if (secured)
            {
                SetSecuredLoginCredentials(_loginCredentials, _conProperties, serverName);
                SetSecuredSynchronizationProfile(serverName);
            }
            else
            {
                SetLoginCredentials(_loginCredentials, _conProperties, serverName, 80);
                SetSynchronizationProfile(serverName);
            }

            //Set the application
            GraybarMobilityDB.SetApplication(_app);
        }

        public void ResetApplication()
        {
            _app.ResetApplication();
        }

        /// <summary>
        /// Start the connection
        /// </summary>
        /// <returns>returns true if successful else unregisters</returns>
        public bool Login()
        {
            try
            {
                _app.StartConnection(GrayBarAnonDBSettings.TIMEOUT);
            }
            catch (Exception ex)
            {
                _app.UnregisterApplication(GrayBarAnonDBSettings.TIMEOUT);
                throw;
            }
            return true;
        }

        public bool Sync()
        {
            GraybarMobilityDB.DisableChangeLog();

            try
            {
                GraybarMobilityDB.Synchronize();
            }
            catch (Exception ex)
            {
                _app.UnregisterApplication(GrayBarAnonDBSettings.TIMEOUT);
                throw;
            }
            /*
             sequence: MobileUserSyncGroup, syncSoldToInfo, syncOrderData, syncFavorites, syncReferenceData,
             */

            GraybarMobilityDB.EnableChangeLog();

            return true;
        }


        public bool SyncGroup(string groupName)
        {
            _mobileUsers = MobileUsers.FindAll();

            try
            {
                //MobileUserSyncGroup
                if (groupName == GrayBarAnonDBSettings.MOBILE_USERS_SYNC_GROUP)
                {
                    MobileUsersSubscription mobileUserSubscription = new MobileUsersSubscription();
                    mobileUserSubscription.User_id = GrayBarAnonDBSettings.LDAPUserName;
                    MobileUsers.AddSubscription(mobileUserSubscription);

                    //Synchronize mobile users sync group
                    GraybarMobilityDB.Synchronize(GrayBarAnonDBSettings.MOBILE_USERS_SYNC_GROUP);
                }

                // SoldToInfo
                else if (groupName == GrayBarAnonDBSettings.SYNC_GRP_SOLD_TO_INFO && _mobileUsers.Count > 0)
                {

                    Sold_To_InfoSubscription spInfoSubscription = new Sold_To_InfoSubscription();
                    //Get sold to subscription

                    spInfoSubscription.Sold_to = _mobileUsers[0].SoldTo;
                    //Add subscription
                    Sold_To_Info.AddSubscription(spInfoSubscription);
                    GraybarMobilityDB.Synchronize(GrayBarAnonDBSettings.SYNC_GRP_SOLD_TO_INFO);

                } // OrderData
                else if (groupName == GrayBarAnonDBSettings.SYNC_GRP_ORDER && _mobileUsers.Count > 0)
                {
                    //Order_HeaderSubscription oHeaderSubscription = new Order_HeaderSubscription();
                    Prev_Ordered_ItemsSubscription prevOrderedSubscription = new Prev_Ordered_ItemsSubscription();

                    prevOrderedSubscription.Sold_to = _mobileUsers[0].SoldTo;
                    //Order_Header.AddSubscription(prevOrderedSubscription);
                    Prev_Ordered_Items.AddSubscription(prevOrderedSubscription);
                    GraybarMobilityDB.Synchronize(GrayBarAnonDBSettings.SYNC_GRP_ORDER);

                } // SYNC_GRP_FAVORITE
                else if (groupName == GrayBarAnonDBSettings.SYNC_GRP_FAVORITE)
                {
                    var favGroup = GraybarMobilityDB.GetSynchronizationGroup(GrayBarAnonDBSettings.SYNC_GRP_FAVORITE);

                    FavoriteItemSubscription favItemSubscription = new FavoriteItemSubscription();
                    favItemSubscription.User_id = GrayBarAnonDBSettings.LDAPUserName;
                    FavoriteItem.AddSubscription(favItemSubscription);
                    GraybarMobilityDB.Synchronize(GrayBarAnonDBSettings.SYNC_GRP_FAVORITE);
                }
                else if (groupName == GrayBarAnonDBSettings.SYNC_GRP_REF_DATA)
                {
                    ContactMethodSubscription ctMethodSubscription = new ContactMethodSubscription();
                    ctMethodSubscription.User_id = GrayBarAnonDBSettings.LDAPUserName;
                    ContactMethod.AddSubscription(ctMethodSubscription);
                    GraybarMobilityDB.Synchronize(GrayBarAnonDBSettings.SYNC_GRP_REF_DATA);
                }
            }
            catch (Exception ex)
            {
                _app.UnregisterApplication(GrayBarAnonDBSettings.TIMEOUT);
                throw;
            }
            return true;
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Set login credentials for unsecured port
        /// </summary>
        /// <param name="loginCredentials"></param>
        /// <param name="conProperties"></param>
        private void SetLoginCredentials(LoginCredentials loginCredentials, ConnectionProperties conProperties, 
            string serverName, int port)
        {
            _conProperties = _app.ConnectionProperties;
            _conProperties.ServerName = serverName;
            _conProperties.PortNumber = port;
            _conProperties.FarmId = GrayBarAnonDBSettings.FarmId;
            _conProperties.UrlSuffix = "";
            _conProperties.NetworkProtocol = ConnectionProperties.NETWORK_PROTOCOL_HTTP;
            _conProperties.SecurityConfiguration = GrayBarAnonDBSettings.SEC_CONFIG_LDAP;

            _loginCredentials = new LoginCredentials(GrayBarAnonDBSettings.LDAPUserName, GrayBarAnonDBSettings.LDAPPassword);
            _conProperties.LoginCredentials = _loginCredentials;

            ConnectionProfile cp = GraybarMobilityDB.GetConnectionProfile();
            cp.DomainName = "default";
        }

        /// <summary>
        /// Sets the login credentials and connection properties before calling the service
        /// </summary>
        /// <param name="loginCredentials"></param>
        /// <param name="conProperties"></param>
        private void SetSecuredLoginCredentials(LoginCredentials loginCredentials, ConnectionProperties conProperties, string serverName)
        {
            _conProperties = _app.ConnectionProperties;
            _conProperties.ServerName = serverName;
            _conProperties.PortNumber = 443;
            _conProperties.FarmId = GrayBarAnonDBSettings.FarmId;
            _conProperties.UrlSuffix = "";
            _conProperties.NetworkProtocol = ConnectionProperties.NETWORK_PROTOCOL_HTTPS;
            _conProperties.SecurityConfiguration = GrayBarAnonDBSettings.SEC_CONFIG_LDAP;

            _loginCredentials = new LoginCredentials(GrayBarAnonDBSettings.LDAPUserName, GrayBarAnonDBSettings.LDAPPassword);
            _conProperties.LoginCredentials = _loginCredentials;

            ConnectionProfile cp = GraybarMobilityDB.GetConnectionProfile();
            cp.DomainName = "default";
        }

        /// <summary>
        /// Sets the synchronization profile
        /// </summary>
        private void SetSecuredSynchronizationProfile(string serverName)
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + GrayBarAnonDBSettings.CertFileName;

            GraybarMobilityDB.GetSynchronizationProfile().DomainName = GrayBarAnonDBSettings.DomainName;
            GraybarMobilityDB.GetSynchronizationProfile().NetworkProtocol = ConnectionProperties.NETWORK_PROTOCOL_HTTPS;
            GraybarMobilityDB.GetSynchronizationProfile().ServerName = serverName;
            GraybarMobilityDB.GetSynchronizationProfile().NetworkStreamParams = @"trusted_certificates=" + path + ";compression=none;url_suffix=/ias_relay_server/client/rs_client.dll/FARMRBS";
            GraybarMobilityDB.GetSynchronizationProfile().PortNumber = 443;
            GraybarMobilityDB.GetSynchronizationProfile().UserName = GrayBarAnonDBSettings.LDAPUserName;
            GraybarMobilityDB.GetSynchronizationProfile().Password = GrayBarAnonDBSettings.LDAPPassword;
        }

        private void SetSynchronizationProfile(string serverName)
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + GrayBarAnonDBSettings.CertFileName;

            GraybarMobilityDB.GetSynchronizationProfile().DomainName = GrayBarAnonDBSettings.DomainName;
            GraybarMobilityDB.GetSynchronizationProfile().NetworkProtocol = ConnectionProperties.NETWORK_PROTOCOL_HTTP;
            GraybarMobilityDB.GetSynchronizationProfile().ServerName = serverName;
            GraybarMobilityDB.GetSynchronizationProfile().NetworkStreamParams = @"trusted_certificates=;compression=none;url_suffix=/ias_relay_server/client/rs_client.dll/FARMRBS";
            GraybarMobilityDB.GetSynchronizationProfile().PortNumber = 80;
            GraybarMobilityDB.GetSynchronizationProfile().UserName = GrayBarAnonDBSettings.LDAPUserName;
            GraybarMobilityDB.GetSynchronizationProfile().Password = GrayBarAnonDBSettings.LDAPPassword;
        }

        #endregion
    }
}
