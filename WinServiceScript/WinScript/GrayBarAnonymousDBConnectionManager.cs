﻿using System;
using Sybase.Mobile;
using Sybase.Persistence;
using GB;
using WinServiceScript.Interfaces;

namespace WinServiceScript
{
    public class GrayBarAnonymousDBConnectionManager : ISyncAnonymousDBScript
    {
        #region Private variables

        public Application _app;
        private LoginCredentials _loginCredentials;
        private ConnectionProperties _conProperties;

        #endregion


        #region public methods

        /// <summary>
        /// Logins the application to start the sync process
        /// </summary>
        /// <returns>bool value </returns>
        public bool Login()
        {
            try
            {
                _app.StartConnection(GrayBarAnonDBSettings.TIMEOUT);
            }
            catch (Exception ex)
            {
                _app.UnregisterApplication(GrayBarAnonDBSettings.TIMEOUT);
                throw;
            }
            return true;
        }

        /// <summary>
        /// Registers the application with SMP server
        /// </summary>
        /// <returns>bool</returns>
        public bool Register()
        {
            if (_app.RegistrationStatus != RegistrationStatus.REGISTERED && _app.RegistrationStatus != RegistrationStatus.REGISTERING)
            {
                try
                {
                    _app.RegisterApplication(GrayBarAnonDBSettings.TIMEOUT);
                }
                catch (Exception ex)
                {
                    _app.ResetApplication();
                    throw;
                }
            }
            return true;
        }

        public void ResetApplication()
        {
            _app.ResetApplication();
        }

        /// <summary>
        /// Unregisters the application from SMP server
        /// </summary>
        /// <returns>bool value</returns>
        public bool UnRegister()
        {
            try
            {
                _app.UnregisterApplication(GrayBarAnonDBSettings.TIMEOUT);
            }
            catch (Exception ex)
            {
                _app.ResetApplication();
                throw;
            }
            return true;
        }

        /// <summary>
        /// Syncs the MBOs with the server
        /// </summary>
        /// <returns>bool</returns>
        public bool Sync()
        {
            GraybarMobilityAnonymousDB.DisableChangeLog();
            try
            {
                GraybarMobilityAnonymousDB.Synchronize("default");
            }
            catch (Exception ex)
            {
                //In case of exception uregister the application
                _app.UnregisterApplication(GrayBarAnonDBSettings.TIMEOUT);
                GraybarMobilityAnonymousDB.EnableChangeLog();
                throw;
            }
            GraybarMobilityAnonymousDB.EnableChangeLog();

            return true;
        }

        /// <summary>
        /// Initializes the connection properties etc of the application
        /// </summary>
        public void InitializeApplication(bool secured, string serverName, int port) {
            _app = Application.GetInstance();
            _app.ApplicationIdentifier = "GraybarMobility";

            //Based on the dbType value being passed switch the handlers
            GraybarMobilityAnonymousDB.RegisterCallbackHandler(new CallbackHandler());

            //Set the login credentials for connection properties
            if (secured) {
                //Set the login credentials
                SetSecuredLoginCredentials(_loginCredentials, _conProperties, serverName);

                //Set the synchronization profile
                SetSecuredSynchronizationProfile(serverName);
            }
            else {
                //Set the login credentials
                SetLoginCredentials(_loginCredentials, _conProperties, serverName);

                //Set the synchronization profile
                SetSynchronizationProfile(serverName, port);
            }
            //Set the application
            GraybarMobilityAnonymousDB.SetApplication(_app);
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Set login credentials for unsecured port
        /// </summary>
        /// <param name="loginCredentials"></param>
        /// <param name="conProperties"></param>
        private void SetLoginCredentials(LoginCredentials loginCredentials, ConnectionProperties conProperties, string serverName) {
            _conProperties = _app.ConnectionProperties;
            _conProperties.ServerName = serverName;
            _conProperties.PortNumber = 80;
            _conProperties.FarmId = "FARMMBS";
            _conProperties.UrlSuffix = "";
            _conProperties.NetworkProtocol = ConnectionProperties.NETWORK_PROTOCOL_HTTP;

            _conProperties.SecurityConfiguration = "anonymous";
            //User name: anonymous, password anonymous
            _loginCredentials = new LoginCredentials(GrayBarAnonDBSettings.UserName, 
                GrayBarAnonDBSettings.Password);
            _conProperties.LoginCredentials = _loginCredentials;

            ConnectionProfile cp = GraybarMobilityDB.GetConnectionProfile();
            cp.DomainName = "default";
        }

        private void SetSynchronizationProfile(string serverName, int port) {
            GraybarMobilityDB.GetSynchronizationProfile().DomainName = GrayBarAnonDBSettings.DomainName;
            GraybarMobilityDB.GetSynchronizationProfile().NetworkProtocol = ConnectionProperties.NETWORK_PROTOCOL_HTTP;
            GraybarMobilityDB.GetSynchronizationProfile().ServerName = serverName;
            GraybarMobilityDB.GetSynchronizationProfile().PortNumber = port;
            GraybarMobilityDB.GetSynchronizationProfile().NetworkStreamParams = @"trusted_certificates=;compression=none;url_suffix=/ias_relay_server/client/rs_client.dll/FARMRBS";
            GraybarMobilityDB.GetSynchronizationProfile().UserName = GrayBarAnonDBSettings.UserName;
            GraybarMobilityDB.GetSynchronizationProfile().Password = GrayBarAnonDBSettings.Password;
        }

        /// <summary>
        /// Sets the login credentials and connection properties before calling the service
        /// </summary>
        /// <param name="loginCredentials"></param>
        /// <param name="conProperties"></param>
        private void SetSecuredLoginCredentials(LoginCredentials loginCredentials, ConnectionProperties conProperties, 
            string serverName) {
            _conProperties = _app.ConnectionProperties;
            _conProperties.ServerName = serverName;
            _conProperties.PortNumber = 443;
            _conProperties.FarmId = GrayBarAnonDBSettings.FarmId;
            _conProperties.UrlSuffix = "";
            _conProperties.NetworkProtocol = ConnectionProperties.NETWORK_PROTOCOL_HTTPS;
            _conProperties.SecurityConfiguration = "anonymous";
            _loginCredentials = new LoginCredentials(GrayBarAnonDBSettings.UserName, GrayBarAnonDBSettings.Password);
            _conProperties.LoginCredentials = _loginCredentials;

            ConnectionProfile conProfile = GraybarMobilityAnonymousDB.GetConnectionProfile();
            conProfile.DomainName = GrayBarAnonDBSettings.DomainName;
        }

        /// <summary>
        /// Sets the synchronization profile
        /// </summary>
        private void SetSecuredSynchronizationProfile(string serverName) {
            string path = AppDomain.CurrentDomain.BaseDirectory + GrayBarAnonDBSettings.CertFileName;

            GraybarMobilityAnonymousDB.GetSynchronizationProfile().DomainName = GrayBarAnonDBSettings.DomainName;
            GraybarMobilityAnonymousDB.GetSynchronizationProfile().NetworkProtocol = ConnectionProperties.NETWORK_PROTOCOL_HTTPS;
            GraybarMobilityAnonymousDB.GetSynchronizationProfile().ServerName = serverName;
            GraybarMobilityAnonymousDB.GetSynchronizationProfile().NetworkStreamParams = @"trusted_certificates=" + path + ";compression=none;url_suffix=/ias_relay_server/client/rs_client.dll/FARMRBS";
            GraybarMobilityAnonymousDB.GetSynchronizationProfile().PortNumber = 443;
            GraybarMobilityAnonymousDB.GetSynchronizationProfile().UserName = GrayBarAnonDBSettings.UserName;
            GraybarMobilityAnonymousDB.GetSynchronizationProfile().Password = GrayBarAnonDBSettings.Password;
        }

        #endregion
    }
}