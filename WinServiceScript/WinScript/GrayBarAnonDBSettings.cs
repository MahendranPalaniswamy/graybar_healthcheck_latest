﻿using System;
using System.Configuration;

namespace WinServiceScript
{
    /// <summary>
    /// This class contains relevant properties which need to be assigned to register, login, sync and unregister
    /// the application
    /// </summary>
    public static class GrayBarAnonDBSettings
    {

        public const int TIMEOUT = 1200;
        public const string SEC_CONFIG_LDAP = "LDAPConnection";

        public const string MOBILE_USERS_SYNC_GROUP = "MobileUsersSyncGroup";
        public const string SYNC_GRP_DEFAULT = "default";
        public const string SYNC_GRP_REF_DATA = "ReferenceSyncGroup";
        public const string SYNC_GRP_PANDA_ONDEMAND = "PandAOnDemandSyncGroup";
        public const string SYNC_GRP_PRE_AUTH = "PreAuthOnDemandSyncGroup";
        public const string SYNC_GRP_CUST_ADDR = "CustomAddressSyncGroup";
        public const string SYNC_GRP_FAVORITE = "FavoriteSyncGroup";
        public const string SYNC_GRP_SOLD_TO_INFO = "SoldToInfoSyncGroup";
        public const string SYNC_GRP_ORDER = "OrderRelatedSyncGroup";
        public const string SYNC_GRP_MOBILE_USERS = "MobileUsersSyncGroup";

        public const string SYNC_SERVICE_LOG = "SMPHealthServiceLog";
        public const string SYNC_SERVICE_LOG_NAME = "SMP Health Service Log";
        public const string SYNC_SUCCESS = "Success";
        public const string SUCCESS_MESSAGE_MOBILITY_DB = "Synchronized Mobility DB(Orders) successfully !";
        public const string SUCCESS_MESSAGE_ANON_DB = "Synchronized Anonymous DB successfully !";
        public const string ERROR_MESSAGE = "Error occurred while connecting to the relay server to synchronize Anonymous DB !";
        public const string ERROR_MESSAGE_MOBILITYDB = "Error occured while connecting to the relay server to synchronize Mobility DB (Orders) successfully !";

        public static string UserName
        {
            get
            {
                return ConfigurationManager.AppSettings["userName"];
            }
        }

        public static string ServiceName {

            get
            {
                string protocol = ConfigurationManager.AppSettings["protocol"];
                return (protocol == "https") ? "GrayBar SMP health check script(https)"
                    : "GrayBar SMP health check script(http)";
            }
        }
        
        public static string Protocol
        {
            get
            {
                return ConfigurationManager.AppSettings["protocol"];
            }
        }

        //public static int PORT
        //{
        //    get
        //    {
        //        string protocol = ConfigurationManager.AppSettings["protocol"];
        //        return (protocol == "https") ? 443 : 80;
        //    }
        //}

        public static string CertFileName
        {
            get
            {
                return ConfigurationManager.AppSettings["certName"];
            }
            set
            {
                CertFileName = value;
            }
        }

        public static string Password
        {
            get
            {
                return ConfigurationManager.AppSettings["password"];
            }
        }

        public static string RelayServerAddress
        {
            get
            {
                return ConfigurationManager.AppSettings["relayServer"];
            }
            set
            {
                RelayServerAddress = value;
            }
        }

        public static string Alias
        {
            get
            {
                return ConfigurationManager.AppSettings["alias"];
            }
            set
            {
                Alias = value;
            }
        }

        public static string FarmId
        {
            get
            {
                return ConfigurationManager.AppSettings["farmId"];
            }
        }

        public static string DomainName
        {
            get
            {
                return "default";
            }
        }

        public static string LDAPUserName
        {
            get
            {
                return ConfigurationManager.AppSettings["ldapUserName"];
            }
            set
            {
                LDAPUserName = value;
            }
        }

        public static string LDAPPassword
        {
            get
            {

                return ConfigurationManager.AppSettings["ldapPassword"];
            }
            set
            {
                LDAPPassword = value;
            }
        }

        public static int Interval
        {
            get
            {
                return Convert.ToInt32(ConfigurationManager.AppSettings["interval"]);
            }
        }
    }
}
